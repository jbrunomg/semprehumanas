<?php
//die('AQUI');
require_once './loader.php'; 
@session_start();
if ($_SESSION['LOGADOEMPRESA'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'empresa/empresaLogin/');
    exit;
}

$pos = strpos($_SESSION['EMPRESA']['DADOSESTAGIO'][0]->recrut_end_esta, 'Nº');

if ($pos === false) {
   $end = $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->recrut_end_esta;
   $num = '';
}else{
    $result = explode("Nº", $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->recrut_end_esta);
    $end = $result[0];
    $num = $result[1]; 
}


$setorAtividade = explode(" / ", $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->contrestempr_atividade);
$setor = $setorAtividade[0];
$atividade = $setorAtividade[1]; 

$beneficios = explode(" - ", $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->contrestempr_beneficios);
$valor_transporte = $beneficios[0];
$valor_refei = $beneficios[1]; 
$valor_outro = $beneficios[2]; 


?>

<?php 
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl()."/thumb.php?w=200&src=images/".$sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';
?>

<style>
  h1 {
    font-family: arial, sans-serif;
    font-size: 15pt;

  }

  hr {
    border-color: orangered;
  }

  select.input-lg {
    height: 46px;
    line-height: 20px;
}
.input-lg {
    height: 0px;
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
}
</style>

<body class="js">

	<!-- Preloader -->
	 <!-- <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div> -->
	<!-- End Preloader -->
	
<?php require_once './menu.php'; ?>
      
<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($sobre->modulo3_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($sobre->modulo3_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
					<ul>
						<li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
						<li class="active"><a href="empresas/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->   
    
<section id="estudante" class="features section" style="padding-top: 70px;">

<form method="post" action="empresa/estEditExe/">
		<div class="container">
			<H1>Empresa</H1>
			<hr>
			<div class="container">

				<?php if (isset($_GET['erro'])): ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h6><i class="icon fa fa-check">  Aconteceu um erro contate o Suporte!</i></h6>                 
					</div>
				<?php endif; ?>

                <?php if (isset($_GET['errocpf'])): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h6><i class="icon fa fa-check"> CPF já cadastrado, favor verificar se o CPF foi digitado corretamente!</i></h6>                 
                    </div>
                <?php endif; ?>

				<br>
			</div>

			<div class="container">
				<div class="row">
                    <div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputPassword1">Empresa</label>
							<input type="text" class="form-control" id="empresa_sempemrazao" name="empresa_sempemrazao" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->sempemrazao ?>" disabled>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
                            <label for="input">Usar endereço da empresa?</label>
                            <select id="usar" class="form-control">
                                <option value="sim">Sim</option>
                                <option value="nao">Não</option>
                            </select>
						</div>
					</div>
				</div>
			</div>
            <div class="container">
				<div class="row">
					<div class="col-lg-4">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Endereço</label>
						<input type="text" class="form-control" id="empresa_sempemlogra" name="empresa_sempemlogra" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->sempemlogra ?>" placeholder="">
						</div>
					
					</div>
					<div class="col-lg-1">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Nº</label>
						<input type="text" class="form-control" id="empresa_sempemnumer" name="empresa_sempemnumer" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->sempemnumer ?>" placeholder="">
						</div>
					
					</div>
                    <div class="col-lg-3">

                        <div class="form-group">
                        <label for="exampleInputEmail1">Bairro</label>
                        <input type="text" class="form-control" id="empresa_sempembairr" name="empresa_sempembairr" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->sempembairr ?>" placeholder="">
                        </div>
                        
                    </div>
                    <div class="col-lg-3">
                    
                        <div class="form-group">
                        <label for="exampleInputEmail1">Cidade</label>
                        <input type="text" class="form-control" id="empresa_sempemcidad" name="empresa_sempemcidad" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->sempemcidad ?>" placeholder="">
                        </div>
                    
                    </div>
                    <div class="col-lg-1">

                        <div class="form-group">
                        <label for="exampleInputEmail1">UF</label>
                        <input type="text" class="form-control" id="empresa_sempemuf" name="empresa_sempemuf"" placeholder="">
                        </div>
                    
                    </div>
				</div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                    
                        <div class="form-group">
                        <label for="exampleInputEmail1">CEP</label>
                        <input type="text" class="form-control" id="empresa_sempemcep" name="empresa_sempemcep" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->sempemcep ?>" placeholder="">
                        </div>
                    
                    </div>

                   
                </div>
            </div>

            <br>
            <h1>Dados Vagas</h1>

			<hr>

            <div class="container">
				<div class="row">
                    <div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputPassword1">Setor Estágio *</label>]
                            <input type="hidden" id="idEdicao" name="idEdicao" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->contrestempr_id ?>" />
							<input type="text" class="form-control" id="contrestempr_sertor" value="<?php echo $setor ?>" name="contrestempr_sertor" required>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
                            <label for="input">Estágio Obrigatório?</label>
                            <select id="contrestempr_estagioobg" name="contrestempr_estagioobg" class="form-control">
                                <option value="1" <?php if($_SESSION['EMPRESA']['DADOSESTAGIO'][0]->contrestempr_estagioobg == '1'){ echo "SELECTED";}?>>Sim</option>
                                <option value="0" <?php if($_SESSION['EMPRESA']['DADOSESTAGIO'][0]->contrestempr_estagioobg == '0'){ echo "SELECTED";}?>>Não</option>
                            </select>
						</div>
					</div>
				</div>
			</div>  
            <div class="container">
				<div class="row">
                    <div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputPassword1">Atividade a serem desenvolvidas no estágio</label>
							<input type="text" class="form-control" id="contrestempr_atividade" value="<?php echo $atividade ?>" name="contrestempr_atividade">
						</div>
					</div>
				</div>
			</div> 

            <div class="container">
				<div class="row">
                    <div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputPassword1">Carga Horária</label>
							<input type="text" class="form-control"  value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->contrestempr_cargahoraria ?>" id="contrestempr_cargahoraria" name="contrestempr_cargahoraria">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
                            <label for="input">Horário</label>
                            <input type="text" class="form-control" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->contrestempr_horario ?>" id="contrestempr_horario" name="contrestempr_horario">
						</div>
					</div>
				</div>
			</div> 

            <div class="container">
				<div class="row">
                    <div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputPassword1">Duração do Estágio</label>
							<input type="text" class="form-control"  value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->contrestempr_duracao_estagio ?>" id="contrestempr_duracao_estagio" name="contrestempr_duracao_estagio">
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
                            <label for="input">Incío</label>
                            <input type="date" class="form-control" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->contrestempr_data_ini ?>" id="contrestempr_data_ini" name="contrestempr_data_ini">
						</div>
					</div>
                    <div class="col-lg-3">
						<div class="form-group">
                            <label for="input">até</label>
                            <input type="date" class="form-control" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->contrestempr_data_term ?>" id="contrestempr_data_term" name="contrestempr_data_term">
						</div>
					</div>
				</div>
			</div> 

            <br>
            <h1>Benefícios</h1>

			<hr>

			<div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Bolsa</strong></h5>
                          <label class="checkbox-inline">
                            <input type="checkbox" class="styled" name="bolsa" value="1">  
                            Checked
                          </label>
                          <input type="text"  class="form-control input-xs" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->contrestempr_bolsa_Auxilio ?>"  name="valor_auxilio" id="valor_auxilio">
                        </div>
                    </div>
               
                    <div class="col-lg-3">
                        <div class="panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Vale transporte</strong></h5>
                          <label class="checkbox-inline">
                            <input type="checkbox" class="styled" name="vale" value="1">   
                            Checked
                          </label>
                          <input type="text"  class="form-control input-xs" value="<?php echo $valor_transporte ?>"  name="valor_transporte" id="valor_transporte">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Refeição</strong></h5>
                          <label class="checkbox-inline">
                            <input type="checkbox" class="styled" name="refeicao" value="1">                 
                            Checked
                          </label>
                          <input type="text"  class="form-control input-xs" value="<?php echo $valor_refei ?>"  name="valor_refei" id="valor_refei">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Outros</strong></h5>
                          <label class="checkbox-inline">
                            <input type="checkbox" class="styled" name="outros" value="1">                 
                            Checked
                          </label>
                          <input type="text"  class="form-control input-xs" value="<?php echo $valor_outro ?>"  name="valor_outro" id="valor_outro">
                        </div>
                    </div>

                </div>
			</div>

			<br>
            <h1>Dados do Estagio Supervisionado</h1>

            <hr>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">CPF *</label>
                            <input type="text" class="form-control" maxlength="11" id="estudante_cpf" name="estudante_cpf" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualcpf ?>"  placeholder="" required>
                            <input type="hidden" id="estudante_palualcodig"  name="estudante_palualcodig" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome *</label>
                            <input type="text" class="form-control" id="estudante_nome" name="estudante_nome" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualnome ?>" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">E-Mail *</label>
                            <input type="email" class="form-control" id="estudante_salualemail" name="estudante_salualemail" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualemail ?>"  placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Celular *</label>
                            <input type="text" class="form-control" data-inputmask="'mask': ['()-9999-9999 [x99999]', '+099 99 99 9999[9]-9999'], 'showTooltip': false" id="estudante_salualtel01" name="estudante_salualtel01" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualtel01 ?>" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Rg *</label>
                           <input type="text" class="form-control"  id="estudante_rg" name="estudante_rg" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualrg ?>"  placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Orgão Expedição *</label>
                           <input type="text" class="form-control" id="estudante_rg_org_expd" name="estudante_rg_org_expd" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualrg_org_expd ?>"  placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="sel1">Estado Civil *</label>
                            <select class="form-control" id="estudante_salualestci" name="estudante_salualestci">
                            <option value='Solteiro' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualestci =='Solteiro'?'selected':'';?>>Solteiro(a)</option>
                            <option value='Casado' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualestci =='Casado'?'selected':'';?>>Casado(a)</option>
                            <option value='Separado' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualestci =='Separado'?'selected':'';?>>Separado(a)</option>
                            <option value='Divorciado' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualestci =='Divorciado'?'selected':'';?>>Divorciado(a)</option>
                            <option value='Viuvo' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualestci =='Viuvo'?'selected':'';?>>Viuvo(a)</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Data Nascimento *</label>
                           <input type="date" class="form-control" id="estudante_talualniver" name="estudante_talualniver" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->talualniver ?>"  placeholder="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                           <label for="exampleInputEmail1">CEP *</label>
                           <input type="text" class="form-control" id="estudante_salualcep" name="estudante_salualcep" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualcep ?>" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Endereço *</label>
                           <input type="text" class="form-control" id="estudante_saluallogra" name="estudante_saluallogra" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->saluallogra ?>" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nº *</label>
                            <input type="text" class="form-control" id="estudante_salualnumer" name="estudante_salualnumer" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualnumer ?>" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Bairro *</label>
                            <input type="text" class="form-control" id="estudante_salualbairr" name="estudante_salualbairr" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualbairr ?>" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Cidade *</label>
                           <input type="text" class="form-control" id="estudante_salualcidad" name="estudante_salualcidad" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualcidad ?>" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                           <label for="exampleInputEmail1">UF *</label>
                           <input type="text" class="form-control" id="estudante_salualestad" name="estudante_salualestad" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualestad ?>" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Nome do Pai *</label>
                           <input type="text" class="form-control" id="estudante_salualpai" name="estudante_salualpai" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualpai ?>" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome da Mãe *</label>
                            <input type="text" class="form-control" id="estudante_salualmae" name="estudante_salualmae" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualmae ?>" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="sel1">Instituição de Ensino *</label>
                            <select class="form-control" id="estudante_ialualinsen" name="estudante_ialualinsen">
                            <?php foreach ($_SESSION['EMPRESA']['INSTITUICAO'] as $inst) { $select = ''; ?>
                                <?php
                                    if ($inst->pensencodig == $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->ialualinsen) {
                                    $select = "selected";

                                } ?>
                                <option value="<?php echo $inst->pensencodig; ?>"<?php echo $select ?>><?php echo $inst->sensennome; ?></option>
                            <?php  } ?>  
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="sel1">Curso *</label>
                            <select class="form-control" id="estudante_ialualcurso" name="estudante_ialualcurso">
                            <?php foreach ($_SESSION['EMPRESA']['CURSOS'] as $curso) { $select = ''; ?>
                                <?php
                                    if ($curso->pcurcucodig == $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->ialualcurso) {
                                    $select = "selected";

                                } ?>
                                <option value="<?php echo $curso->pcurcucodig; ?>"<?php echo $select ?>><?php echo $curso->scurcunome; ?></option>
                            <?php  } ?>    
                            </select>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="sel1">Período/Série *</label>
                            <select class="form-control" id="estudante_salualperio" name="estudante_salualperio">
                                <option value='1º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='1º'?'selected':'';?>>1º</option> 
                                <option value='2º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='2º'?'selected':'';?>>2º</option> 
                                <option value='3º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='3º'?'selected':'';?>>3º</option> 
                                <option value='4º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='4º'?'selected':'';?>>4º</option> 
                                <option value='5º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='5º'?'selected':'';?>>5º</option> 
                                <option value='6º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='3º'?'selected':'';?>>3º</option> 
                                <option value='7º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='7º'?'selected':'';?>>7º</option> 
                                <option value='8º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='8º'?'selected':'';?>>8º</option> 
                                <option value='9º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='9º'?'selected':'';?>>9º</option> 
                                <option value='10º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='10º'?'selected':'';?>>10º</option> 
                                <option value='11º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='11º'?'selected':'';?>>11º</option> 
                                <option value='12º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='12º'?'selected':'';?>>12º</option> 
                                <option value='13º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='13º'?'selected':'';?>>13º</option> 
                                <option value='14º' <?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualperio =='14º'?'selected':'';?>>14º</option>         
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">          
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nº: Matrícula *</label>
                            <input type="text" class="form-control" id="estudante_salualmatricula" name="estudante_salualmatricula" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualmatricula ?>" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">          
                        <div class="form-group">
                            <label for="exampleInputEmail1">Termino Curso *</label>
                            <input type="date" class="form-control" id="estudante_salualtermino" name="estudante_salualtermino" value="<?php echo $_SESSION['EMPRESA']['DADOSESTAGIO'][0]->salualtermino ?>" placeholder="">
                        </div>
                    </div>
                </div>
            </div>

			<br>

            <div class="container">
                <div class="row">
                    <div class="col-lg-12">          
                        <p style="font-weight: 700;">Obs.: Para facilitar o processo de contratação, por gentileza preencher TODOS os dados solicitados.</p>
                    </div>
                </div>
            </div>

            <br>

			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<button type="submit" class="btn btn-warning btn-lg">Salvar</button>
					</div>
				</div>
			</div>
    	</div>
	</form>



</section>


   
         



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>	
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
    <!-- Custom  -->
	<script type="text/javascript" src="js\jquery.maskedinput.min.js"></script>

	<script>
	$('li#services').addClass('current');
	</script>
    <script>

    $('#usar').change(function() {
        preencherDadosEmpresa();
    });

    function preencherDadosEmpresa() {

        if ($('#usar').val() == 'sim') {
            $('#empresa_sempemlogra').val('<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemlogra ?>');
            $('#empresa_sempemnumer').val('<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemnumer ?>');
            $('#empresa_sempembairr').val('<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempembairr ?>');
            $('#empresa_sempemcep').val('<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemcep ?>');
            $('#empresa_sempemcidad').val('<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemcidad ?>');
            $('#empresa_sempemuf').val('<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemuf ?>');
        } else {
            $('#empresa_sempemlogra').val('');
            $('#empresa_sempemnumer').val('');
            $('#empresa_sempembairr').val('');
            $('#empresa_sempemcep').val('');
            $('#empresa_sempemcidad').val('');
            $('#empresa_sempemuf').val('');
        }
    }

    $('#empresa_sempemcep').blur(function(){

        var cep = $('#empresa_sempemcep').val();
        cep = cep.replace('-','');

        if($.trim(cep) != ""){


        $.getScript("https://www.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
            // o getScript dá um eval no script, então é só ler!
            //Se o resultado for igual a 1 
            
            if(resultadoCEP["resultado"]){
                
                if (resultadoCEP['resultado'] == '1' || resultadoCEP['resultado'] == '2'){
                    
                    $("#empresa_sempemlogra").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
                    $("#empresa_sempembairr").val(unescape(resultadoCEP["bairro"]));
                    $("#empresa_sempemuf").val(unescape(resultadoCEP["uf"]));
                    $("#empresa_sempemcidad").val(unescape(resultadoCEP["cidade"]));

                }else{ 

                    $("#empresa_sempemlogra").val('');
                    $("#empresa_sempembairr").val('');
                    $("#empresa_sempemuf").val('');
                    $("#empresa_sempemcidad").val('');

                }
                
            }
                        
        });
        
        }

    });

    $(function ($) {
            $("#estudante_salualtel01").mask("(99) 9999-9999?9");
            $("#estudante_salualtel02").mask("(99) 9999-9999?9");
            $("#estudante_salualtel03").mask("(99) 9999-9999?9");
            $("#estudante_salualcep").mask("99999-999");
            $("#estudante_cpf").mask("999.999.999-99");
            
    });

    //   $('#estudante_salualtermino').datepicker({
    //         format: "mm/yyyy",
    //         language: "pt-BR"
    //     });


    $('#estudante_salualcep').blur(function(){

        var cep = $('#estudante_salualcep').val();
        cep = cep.replace('-','');

        if($.trim(cep) != ""){


            $.getScript("https://www.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
            // o getScript dá um eval no script, então é só ler!
            //Se o resultado for igual a 1 
                
                if(resultadoCEP["resultado"]){
                    
                    if (resultadoCEP['resultado'] == '1' || resultadoCEP['resultado'] == '2'){
                    
                        $("#estudante_saluallogra").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
                        $("#estudante_salualbairr").val(unescape(resultadoCEP["bairro"]));
                        $("#estudante_salualestad").val(unescape(resultadoCEP["uf"]));
                        $("#estudante_salualcidad").val(unescape(resultadoCEP["cidade"]));

                    }else{ 

                        $("#estudante_saluallogra").val('');
                        $("#estudante_salualbairr").val('');
                        $("#estudante_salualestad").val('');
                        $("#estudante_salualcidad").val('');

                    }
                
                }
                            
            });
            
        }
    
    });

    $('#estudante_cpf').blur(function(){

        let cpf = $('#estudante_cpf').val().match(/\d/g).join("");

        $.ajax({
            url: 'empresa/consultaAluno/',
            method: "POST",
            data: { cpf : cpf }, 
            dataType: "json",
            success: function(data) {
                $("#estudante_palualcodig").val(data.palualcodig);
                $("#estudante_nome").val(data.salualnome);
                $("#estudante_salualemail").val(data.salualemail);
                $("#estudante_salualtel01").val(data.salualtel01);
                $("#estudante_rg").val(data.salualrg);
                $("#estudante_rg_org_expd").val(data.salualrg_org_expd);
                $("#estudante_talualniver").val(data.talualniver.split('/').reverse().join('-'));
                $("#estudante_salualestci").val(data.salualestci);
                $("#estudante_salualcep").val(data.salualcep);
                $("#estudante_saluallogra").val(data.saluallogra);
                $("#estudante_salualbairr").val(data.salualbairr.replace("+", " "));
                $("#estudante_salualestad").val(data.salualestad);
                $("#estudante_salualcidad").val(data.salualcidad);
                $("#estudante_salualnumer").val(data.salualnumer);
                $("#estudante_salualpai").val(data.salualpai);
                $("#estudante_salualmae").val(data.salualmae);
                $("#estudante_ialualcurso").val(data.ialualcurso);
                $("#estudante_ialualinsen").val(data.ialualinsen);
                $("#estudante_salualtermino").val(data.salualtermino.split('/').reverse().join('-'));
                $("#estudante_salualmatricula").val(data.salualmatricula);
                $("#estudante_salualperio").val(data.salualperio);

                

                

            }
            
        });

    });

    
        

    </script>
</body>
</html>          