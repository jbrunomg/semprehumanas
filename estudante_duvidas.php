<?php
require_once './loader.php'; ?>
<?php
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl() . "/thumb.php?w=200&src=images/" . $sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';
?>

<body class="js">

  <!-- Preloader -->
  <!-- <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
    <div class="loader-inner ball-scale-ripple-multiple vh-center">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div> -->
  <!-- End Preloader -->

  <?php require_once './menu.php'; ?>

  <!-- Start Breadcrumbs -->
  <section class="breadcrumbs" <?php if (!empty($sobre->modulo3_imagem)) {
                                  echo "style='background: url(thumb.php?w=1280&zc=0&src=images/" . stripslashes($sobre->modulo3_imagem) . ");'";
                                } ?>>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
          <ul>
            <li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
            <li class="active"><a href="estudantes/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!--/ End Breadcrumbs -->



  <div class="row sidebar-page">

    <!-- Page Content -->
    <div class="col-md-9 page-content">

      <!-- Toggle -->
      <div class="panel-group">

        <!-- Start Toggle 1 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1" class="collapsed" aria-expanded="false">
                <i class="fa fa-angle-up control-icon"></i>
                <i class="fa fa-desktop"></i> O que é Estágio?
              </a>
            </h4>
          </div>
          <div id="collapse-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">É a oportunidade para que os estudantes coloquem em prática os conhecimentos adquiridos em sala de aula, de maneira que possam vivenciar no dia a dia a teoria, absorvendo melhor os conhecimentos, podendo refletir e confirmar sobre a sua escolha. O estágio curricular, seja ele obrigatório ou não obrigatório, tem a função de propiciar ao estagiário o aprendizado social, profissional e cultural, tendo como resultado uma reflexão real e futurista dos novos cenários sócio-econômicos. O estágio não é um emprego. Ele é um complemento do aprendizado dos cursos de nível médio, técnicos ou superiores, regido pela lei nº11.788 de 25/09/2008.</div>
          </div>
        </div>
        <!-- End Toggle 1 -->

        <!-- Start Toggle 2 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-2" class="collapsed" aria-expanded="false">
                <i class="fa fa-angle-down control-icon"></i>
                <i class="fa fa-gift"></i> O que é Agente de Integração?
              </a>
            </h4>
          </div>
          <div id="collapse-2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">É a instituição que promove a aproximação entre a Instituição de Ensino, a Unidade Concedente e o estudante, com a finalidade de identificar e captar oportunidade de estágio.</div>
          </div>
        </div>
        <!-- End Toggle 2 -->

        <!-- Start Toggle 3 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-3" class="" aria-expanded="true">
                <i class="fa fa-angle-down control-icon"></i> Quem pode ser estagiário?
              </a>
            </h4>
          </div>
          <div id="collapse-3" class="panel-collapse collapse in" aria-expanded="true" style="">
            <div class="panel-body">Alunos regularmente matriculados que freqüentem efetivamente cursos vinculados à estrutura do Ensino Público ou Particular, nos níveis Superior, Profissionalizante do ensino médio e supletivo.</div>
          </div>
        </div>
        <!-- End Toggle 3 -->

        <!-- Start Toggle 4 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-4" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Como faço para me cadastrar?
              </a>
            </h4>
          </div>
          <div id="collapse-4" class="panel-collapse collapse">
            <div class="panel-body">Comparecendo pessoalmente ao SempreHumana ou através do site:<strong class="accent-color"><a href="/contato.php"> CLIQUE AQUI </a></strong> .</div>
          </div>
        </div>
        <!-- End Toggle 4 -->

        <!-- Start Toggle 5 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-5" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Qual a jornada diária de estágio? Existe uma jornada mínima ou máxima?
              </a>
            </h4>
          </div>
          <div id="collapse-5" class="panel-collapse collapse">
            <div class="panel-body">Mínima, não. Máxima: Nível Médio: 04:00 horas diárias, Nível Técnico: 06:00 horas diárias e Nível Superior: 08:00 horas diárias</div>
          </div>
        </div>
        <!-- End Toggle 5 -->

        <!-- Start Toggle 6 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-6" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Legislação e Regulamentação do Estágio
              </a>
            </h4>
          </div>
          <div id="collapse-6" class="panel-collapse collapse">
            <div class="panel-body">Para todo estágio é assinado um convênio entre instituições de ensino e o SEMPRE HUMANA, como também um TCE – Temo de Compromisso de Estágio, entre estudante, escola, empresa e Agente de Integração (SEMPRE HUMANA), de acordo com a Lei 6494 de 07.12.77 e seu Decreto de Regulamentação nº 87497/82 de 18.08.82.</div>
          </div>
        </div>
        <!-- End Toggle 6 -->

        <!-- Start Toggle 7 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-7" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Qual a documentação necessária para efetivar um contrato de estágio?
              </a>
            </h4>
          </div>
          <div id="collapse-7" class="panel-collapse collapse">
            <div class="panel-body">Cópia de Identidade e CPF, Comprovante de Vínculo com a Instituição de Ensino e 1 (uma) foto 3X4.</div>
          </div>
        </div>
        <!-- End Toggle 7 -->

        <!-- Start Toggle 8 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-8" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Carteira Profissional
              </a>
            </h4>
          </div>
          <div id="collapse-8" class="panel-collapse collapse">
            <div class="panel-body">A Lei 6494/77 e o Decreto 87497/82 não tratam da obrigatoriedade de anotar o estágio de estudantes na “Carteira de Trabalho e Previdência Social”. Assim, entende-se que é facultado à Empresa anotar, na parte de “Anotações Gerais” da “Carteira de Trabalho e Previdência Social” do estudante.</div>
          </div>
        </div>
        <!-- End Toggle 8 -->

        <!-- Start Toggle 9 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-9" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Estágio não cria vínculo empregatício
              </a>
            </h4>
          </div>
          <div id="collapse-9" class="panel-collapse collapse">
            <div class="panel-body">O Termo de Compromisso de Estágio – TCE, vínculado ao instrumento jurídico (Acordo de Cooperação), constitui um dos componentes exigíveis, pela autoridade competente, sobre a inexistência de vínculo empregatício.</div>
          </div>
        </div>
        <!-- End Toggle 9 -->

        <!-- Start Toggle 10 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-10" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Estágio não cria vínculo empregatício
              </a>
            </h4>
          </div>
          <div id="collapse-10" class="panel-collapse collapse">
            <div class="panel-body">Sim. O estagiário não faz jus a aviso prévio em caso de rescisão contratual. Na prática, no entanto, tem se adotado o critério de comunicar a dispensa por escrito, com antecedência de 5 dias, válido para ambas as partes.</div>
          </div>
        </div>
        <!-- End Toggle 10 -->

        <!-- Start Toggle 11 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-11" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Quem providencia o seguro contra acidentes pessoais?
              </a>
            </h4>
          </div>
          <div id="collapse-11" class="panel-collapse collapse">
            <div class="pane8l-body">Caberá ao SempreHumana providenciar e pagar o prêmio mensal do Seguro Contra Acidentes Pessoais (cobertura de morte ou invalidez), bem como administrar a Apólice mensalmente, no tocante a inclusão e exclusão de Estagiários, sem qualquer ônus para a Instituição de Ensino, para o estudante ou para empresa.</div>
          </div>
        </div>
        <!-- End Toggle 11 -->

        <!-- Start Toggle 12 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-12" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Qual a duração máxima do estágio na empresa?
              </a>
            </h4>
          </div>
          <div id="collapse-12" class="panel-collapse collapse">
            <div class="panel-body">Dois(02)anos no máximo, exeto no caso do estagiário ser portador de deficiência.</div>
          </div>
        </div>
        <!-- End Toggle 12 -->



      </div>
      <!-- End Toggle -->

      <!-- Divider -->
      <div class="hr5" style=" margin-top:45px; margin-bottom:40px;"></div>



    </div>
    <!-- End Page Content-->

    <!--Sidebar-->
    <div class="col-md-3 sidebar right-sidebar">


      <!-- Popular Posts widget -->
      <div class="widget widget-popular-posts">
        <!--  <h4>Popular Post <span class="head-line"></span></h4> -->

        <!-- Classic Heading -->
        <h4 class="classic-title"><span>Atividades</span></h4>

        <div class="row">

          <!-- Start Service Icon 1 -->
          <div class="col-md-12 service-box service-icon-left-more">
            <div class="service-icon">
              <i class="fa fa-users icon-medium"></i>
            </div>
            <div class="service-content">
              <h4 class="accent-color">Estudantes</h4>
              <p>Cadastre aqui o seu currículo e tenha acesso as oportunidades de estágio.</p>
            </div>
          </div>
          <br>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-12 service-box service-icon-left-more">
            <div class="service-icon">
              <i class="fa fa-briefcase  icon-medium"></i>
            </div>
            <div class="service-content">
              <h4 class="accent-color">Empresas</h4>
              <p>Cadastre aqui suas vagas a tornando-as mais conhecidas aos estudantes.</p>
            </div>
          </div>
          <br>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-12 service-box service-icon-left-more">
            <div class="service-icon">
              <i class="fa fa-bank icon-medium"></i>
            </div>
            <div class="service-content">
              <h4 class="accent-color">Instituições de ensino</h4>
              <p>Cadastre aqui sua instituição, informando-se, diariamente, das vagas de estágios existentes.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

          <!--  </div> -->

        </div>

      </div>
      <!--End sidebar-->

    </div>
  </div>




  <?php require_once './footer.php'; ?>
  <!-- Jquery -->
  <script type="text/javascript" src="js\jquery.min.js"></script>
  <!-- Colors -->
  <script type="text/javascript" src="js\colors.js"></script>
  <!-- Modernizr JS -->
  <script type="text/javascript" src="js\modernizr.min.js"></script>
  <!-- Appear Js -->
  <script type="text/javascript" src="js\jquery.appear.js"></script>
  <!-- Scrool Up -->
  <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
  <!-- Typed Js -->
  <script type="text/javascript" src="js\typed.min.js"></script>
  <!-- Slick Nav -->
  <script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
  <!-- Onepage Nav -->
  <script type="text/javascript" src="js\jquery.nav.js"></script>
  <!-- Yt Player -->
  <script type="text/javascript" src="js\ytplayer.min.js"></script>
  <!-- Magnific Popup -->
  <script type="text/javascript" src="js\magnific-popup.min.js"></script>
  <!-- Wow JS -->
  <script type="text/javascript" src="js\wow.min.js"></script>
  <!-- Counter JS -->
  <script type="text/javascript" src="js\waypoints.min.js"></script>
  <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
  <!-- Isotop JS -->
  <script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
  <!-- Masonry JS -->
  <script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="js\slick.min.js"></script>
  <!-- Bootstrap JS -->
  <script type="text/javascript" src="js\bootstrap.min.js"></script>
  <!-- Activate JS -->
  <script type="text/javascript" src="js\active.js"></script>
  <!-- Custom  -->

  <script>
    $('li#services').addClass('current');
  </script>
</body>

</html>