<?php
//die('AQUI');
require_once './loader.php'; ?>
<?php 
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl()."/thumb.php?w=200&src=images/".$sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';
?>

<style>



  h1 {
		font-family: arial, sans-serif;
		font-size: 15pt;

	}

	hr {
		border-color: orangered;
	}


</style>
<meta name="viewport" content="width=device-width, initial-scale=1">

<body class="js">

	<!-- Preloader -->
	 <!-- <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div> -->
	<!-- End Preloader -->
	
<?php require_once './menu.php'; ?>
      
<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($sobre->modulo3_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($sobre->modulo3_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
					<ul>
						<li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
						<li class="active"><a href="estudantes/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->   
    
<section id="estudante" class="features section" style="padding-top: 70px;">


<div class="container">
  <div class="row">
    <div class="col-lg-8">
  <!-- Start Toggle 1 -->
  <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1" class="collapsed" aria-expanded="false">
                <i class="fa fa-angle-up control-icon"></i>
                <i class="fa fa-desktop"></i> Eu posso rescindir o contrato de estágio?
              </a>
            </h4>
          </div>
          <div id="collapse-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">Sim pode rescindir, não há obrigatoriedade em cumprir prazo de contrato de estágio das partes: a empresa e o estagiário. Sugere que avise a empresa com antecedência de no mínimo 05 dias.</div>
          </div>
        </div>
        <!-- End Toggle 1 -->

        <!-- Start Toggle 2 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-2" class="collapsed" aria-expanded="false">
                <i class="fa fa-angle-down control-icon"></i>
                <i class="fa fa-gift"></i> Eu tenho direito ao recesso no estágio?
              </a>
            </h4>
          </div>
          <div id="collapse-2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">É assegurado ao estagiário, sempre que o estágio tenha duração igual ou superior a 1 (um) ano, período de recesso de 30 (trinta) dias, a ser gozado em comum acordo com a empresa, se possível preferencialmente durante as férias escolares e obrigatoriamente no tempo vigente do contrato. O recesso poderá ser remunerado, se estágio não obrigatório, e proporcional ao tempo de permanência na empresa.</div>
          </div>
        </div>
        <!-- End Toggle 2 -->

        <!-- Start Toggle 3 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-3" class="" aria-expanded="true">
                <i class="fa fa-angle-down control-icon"></i> O pagamento de bolsa auxílio e benefícios aos estagiários é obrigatório?
              </a>
            </h4>
          </div>
          <div id="collapse-3" class="panel-collapse collapse in" aria-expanded="true">
            <div class="panel-body">O estagiário poderá receber a bolsa -auxílio sendo obrigatória a sua concessão, bem como a do auxílio-transporte, na hipótese de estágio não obrigatório.</div>
          </div>
        </div>
        <!-- End Toggle 3 -->

        <!-- Start Toggle 4 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-4" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> O estagiário tem direito a 13º salário? 
              </a>
            </h4>
          </div>
          <div id="collapse-4" class="panel-collapse collapse">
            <div class="panel-body">Diante da legislação de estágio não, ficando a critério da empresa oferecer como gratificação.</div>
          </div>
        </div>
        <!-- End Toggle 4 -->

        <!-- Start Toggle 5 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-5" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Se eu danificar algum equipamento ou qualquer objeto na empresa, o que faço?
              </a>
            </h4>
          </div>
          <div id="collapse-5" class="panel-collapse collapse">
            <div class="panel-body">Procurar primeiramente ter todo zelo e cuidado com os pertences da empresa, porém se ocasionalmente acontecer, procurar conhecer as normas da empresa para saber como resolver e tratar amigavelmente a solução do problema.</div>
          </div>
        </div>
        <!-- End Toggle 5 -->

        <!-- Start Toggle 6 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-6" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Para transformar meu estágio em obrigatório, o que devo fazer?
              </a>
            </h4>
          </div>
          <div id="collapse-6" class="panel-collapse collapse">
            <div class="panel-body">Primeiramente verificar possibilidade de matricular na disciplina correspondente ao estágio obrigatório, depois verificar a aceitação da empresa, planejando a carga-horária e dando ao conhecimento os critérios exigidos da Instituição Ensino. A empresa precisa autorizar ao agente de integração fazer a mudança de modalidade de estágio para o curricular obrigatório através do termo aditivo. </div>
          </div>
        </div>
        <!-- End Toggle 6 -->

        <!-- Start Toggle 7 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-7" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Eu posso faltar o estágio em período de prova?
              </a>
            </h4>
          </div>
          <div id="collapse-7" class="panel-collapse collapse">
            <div class="panel-body"> Em período de avaliação escolar, apresentando um documento. da IES, ter a dispensa da metade da carga horária diária de estágio para fins de estudos.  Tudo é acordado com a empresa. As faltas só serão abonadas, se caso doença, e apresentando atestado médico para curto período de licença médica e aceitação do abono a critério da empresa.</div>
          </div>
        </div>
        <!-- End Toggle 7 -->

        <!-- Start Toggle 8 -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse-8" class="collapsed">
                <i class="fa fa-angle-down control-icon"></i> Estou grávida, tenho direito a licença maternidade? 
              </a>
            </h4>
          </div>
          <div id="collapse-8" class="panel-collapse collapse">
            <div class="panel-body">A estagiária poderá ser desligada da empresa a critério, podendo ser recontratada mediante sua disposição a volta as atividades do estágio, mas sem garantias.</div>
          </div>
        </div>
        <!-- End Toggle 8 -->       



    </div>
    <div class="col-lg-4">
      <h1>Informações</h1>

      <?php
        $servico = new Servico();
        $servico->getServicos();     
      ?> 
      <!-- Início Serviço --> 
      <?php foreach ($servico->db->data as $s): ?>
      <div class="col-md-12 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
        <a href="<?= stripslashes($s->servico_link) ?>">
          <div class="single-features">
            <i class="<?= stripslashes($s->servico_icon) ?>"></i>
              <h4><?= stripslashes($s->servico_nome) ?></h4>
                <p><?= stripslashes($s->servico_descricao) ?></p>
          </div>
          </br>
  <!--    <div class="service-content">             
                <p>Cadastre aqui o seu currículo e tenha acesso as oportunidades de estágio.</p>
              </div> -->
        </a>
      </div>

      </br>
      </br>

      <?php endforeach; ?> 


				</div>
    </div>
  </div>
</div>



    


</section>


   
         



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>	
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
      <!-- Custom  -->
    
  <script>
   $('li#services').addClass('current');
  </script> 
  
</body>
</html>          