<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

spl_autoload_register(function ($className) {
    $classpath = array(
        'model/', 'helpers/', 'plugin/email/', //frontend
        '../model/', '../helpers/', '../plugin/email/' //backend
    );
    $classFile = ucfirst($className) . ".php";
    foreach ($classpath as $path) {
        if (is_readable("$path$classFile")) {
            require "$path$classFile";
            // die("$path$classFile");
            break;
        }
    }
});

function getUserIP() {
    $client = filter_var($_SERVER['HTTP_CLIENT_IP'] ?? '', FILTER_VALIDATE_IP);

    $forward = filter_var($_SERVER['HTTP_X_FORWARDED_FOR'] ?? '', FILTER_VALIDATE_IP);

    $remote = filter_var($_SERVER['REMOTE_ADDR'] ?? '', FILTER_VALIDATE_IP);

    if ($client !== false) {
        return $client;
    } elseif ($forward !== false) {
        return $forward;
    } elseif ($remote !== false) {
        return $remote;
    } else {
        return '0.0.0.0'; 
    }
}

function geolocationEstudante($address)
	{

		$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');

		$output= json_decode($geocode);

		//v($output);

		if($output->status <> 'OVER_QUERY_LIMIT'){ 

            if($output->status <> 'ZERO_RESULTS'){

                    //v($output);die();
                
                $lat = $output->results[0]->geometry->location->lat;
                $long = $output->results[0]->geometry->location->lng;

                $dados['latitude']   = $lat;
                $dados['longitude']  = $long;

                return $dados;

            }else{
                return false;
            }


        } else{
            echo 'You have exceeded your daily request quota for this API';
        }

	}

    function enviarEmail($nome, $fone){

        include_once "../includes/vendor/autoload.php";
        require_once '../model/Smtpr.php';

        $mail = new PHPMailer(true);

        try{

            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            $mail->WordWrap = 80;
            $mail->IsHTML(true);

        }catch(Exception $e){
            throw new Exception('Error');
        }


        if (isset($nome) && !empty($nome)) {

            $smtp = new Smtpr();
            $smtp->getSmtp();

            $contato = new Contato();
            $contato->getContato();

        
            $mail->Port = $smtp->smtp_port;
            $mail->Host = $smtp->smtp_host;
            $mail->Username = $smtp->smtp_username;
            //$mail->From = $smtp->smtp_username;
            $mail->Password = $smtp->smtp_password;
        
        
            //$mail->FromName = $smtp->smtp_fromname;
            $mail->Subject = utf8_decode("Novo Cadastro Empresa");
        
        
            $mail->setFrom($smtp->smtp_username, $smtp->smtp_fromname);
        
            if($smtp->smtp_bcc != "" || $smtp->smtp_bcc != null)
                $mail->addBCC($smtp->smtp_bcc);
        
            $mail->addAddress($smtp->smtp_username);
        
            $hour=date('H');
            $data = date('d/m/Y H:i',mktime($hour+1));
            $nomeEmail = stripslashes($nome);
            $email = stripslashes($contato->contato_email);
            $assunto = utf8_decode(stripslashes("Novo Cadastro Empresa"));
            $mensagem = utf8_decode(stripslashes("Legal! Temos uma nova empresa cadastrada de Nome: ".$nome." Contato: ".$fone.""));
            //$userip = getUserIP();
        
        
            $mail->addReplyTo($email);
            $body = "<b>Data da Mensagem: </b> $data <br />";
            $body .= "<b>Nome:</b> $nomeEmail <br />";
            $body .= "<b>E-mail:</b> $email <br />";
            $body .= "<b>Assunto:</b> $assunto <br />";
            $body .= "<b>Mensagem: </b>$mensagem <br /><br />";
           // $body .= "<b>IP: </b>$userip<br />";
            $mail->Body = nl2br($body);

            if($mail->send()){
                return true;
            } else {
                return false;
            }
        
            return false;
        }

    }



$site = new Site();
$site->getMeta();

$modulo_aparencia = new ModuloAparencia();
$modulo_aparencia->getModuloAparencia();

$modulo1 = new Modulo1();
$modulo1->getModulo1();

$topo = new Modulo1();
$topo->getModulo1();

$modulo2 = new Modulo2();
$modulo2->getModulo2();

$menu = new Modulo2();
$menu->getModulo2();

$modulo3 = new Modulo3();
$modulo3->getModulo3();

$sobre = new Modulo3();
$sobre->getModulo3();

$modulo4 = new Modulo4();
$modulo4->getModulo4();

$modulo5 = new Modulo5();
$modulo5->getModulo5();

$modulo6 = new Modulo6();
$modulo6->getModulo6();

$modulo7 = new Modulo7();
$modulo7->getModulo7();

//$projeto = new Portfolio();
//$projeto->getPortfolios();

$modulo8 = new Modulo8();
$modulo8->getModulo8();

$modulo9 = new Modulo9();
$modulo9->getModulo9();

$contato = new Contato();
$contato->getContato();

$modulo10 = new Modulo10();
$modulo10->getModulo10();

$blog = new Modulo10();
$blog->getModulo10();

$modulo11 = new Modulo11();
$modulo11->getModulo11();

$rodape = new Modulo11();
$rodape->getModulo11();

$modulo12 = new Modulo12();
$modulo12->getModulo12();

$video = new Modulo12();
$video->getModulo12();

$modulo13 = new Modulo13();
$modulo13->getModulo13();

$modulo14 = new Modulo14();
$modulo14->getModulo14();

$modulo15 = new Modulo15();
$modulo15->getModulo15();

$modulo16 = new Modulo16();
$modulo16->getModulo16();

$modulo17 = new Modulo17();
$modulo17->getModulo17();

$servico = new Servico();
$servico->getServicos();

$categoria_blog = new Area();
$categoria_blog->getAreas();


//if($modulo_aparecia->modulo_aparecia_idioma != 'pt') {
setcookie('googtrans', '/pt/'.stripslashes($modulo_aparencia->modulo_aparencia_idioma));
//}