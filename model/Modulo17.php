<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class Modulo17 extends Controle {

    public $db;
    public $modulo17_id;
    public $modulo17_nome;
    public $modulo17_subtitulo;
    public $modulo17_button;
    public $modulo17_status;
    public $modulo17_paginacao;
    public $modulo17_comment;
    public $modulo17_imagem;
    public $result;

    public function __construct() {
        parent::__construct();
        require_once 'Registry.php';
        $registry = Registry::getInstance();
        if( $registry->get('db') == false ) {
            $registry->set('db', new DB);
        }
        $this->db = $registry->get('db');           
    }

    public function atualizar() {
        if ($this->modulo17_imagem != "") {
            $this->update("modulo17", "modulo17_nome  = '$this->modulo17_nome', modulo17_subtitulo = '$this->modulo17_subtitulo',"
                    . " modulo17_button = '$this->modulo17_button',"
                    . " modulo17_status = '$this->modulo17_status', modulo17_imagem = '$this->modulo17_imagem', modulo17_paginacao = '$this->modulo17_paginacao', modulo17_comment = '$this->modulo17_comment'", 
                    "modulo17_id = '$this->modulo17_id'");
        } else {
            $this->update("modulo17", "modulo17_nome  = '$this->modulo17_nome', modulo17_subtitulo = '$this->modulo17_subtitulo',"
                    . " modulo17_button = '$this->modulo17_button',"
                    . " modulo17_status = '$this->modulo17_status', modulo17_paginacao = '$this->modulo17_paginacao', modulo17_comment = '$this->modulo17_comment'", 
                    "modulo17_id = '$this->modulo17_id'");
        }
    }

    public function removerArquivo() {
        $this->deleteArquivo("modulo17", "modulo17_id = '$this->modulo17_id'", "modulo17_imagem", "../images/");
    }

    public function enviar() {
        $this->upload("../images/", "modulo17_imagem", "");
    }

    public function getModulo17() {
        $this->select("modulo17", "", "*", "", "WHERE modulo17_id = 1", "");
    }

}
