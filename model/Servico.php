<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class Servico extends Controle {

    public $db;
    public $servico_id;
    public $servico_nome;
    public $servico_icon;
    public $servico_link;
    public $servico_imagem;
    public $servico_descricao;
    public $result;

    public function __construct() {
        parent::__construct();
        require_once 'Registry.php';
        $registry = Registry::getInstance();
        if( $registry->get('db') == false ) {
            $registry->set('db', new DB);
        }
        $this->db = $registry->get('db');           
    }

    public function gravar() {
        $this->insert("servico", " servico_nome, servico_icon, servico_link, servico_descricao, servico_imagem", " '$this->servico_nome','$this->servico_icon','$this->servico_link','$this->servico_descricao','$this->servico_imagem'");
    }

    public function atualizar() {
        $query = "servico_nome  = '$this->servico_nome', servico_icon = '$this->servico_icon', servico_link = '$this->servico_link', servico_descricao = '$this->servico_descricao'"; 
        if (isset($_FILES['servico_imagem']['name']) && !empty($_FILES['servico_imagem']['name'])) {
            $query .= ", servico_imagem = '$this->servico_imagem'";
        }
        $this->update("servico", "$query", "servico_id = '$this->servico_id'");
    }

    public function remover() {
        $this->delete("servico", "servico_id = '$this->servico_id'");
    }

    public function getServico() {
        $this->select("servico", "", "*", "", "WHERE servico_id = $this->servico_id", "");
    }

    public function getServicos() {
        $this->select("servico");
    }

    public function getJason() {
        $this->getJSON("servico", "servico_id = '$this->servico_id'");
    }

    public function enviar() {
        $this->upload("../images/servico/", "servico_imagem", "");
    }

    public function removerArquivo() {
        $this->deleteArquivo("servico", "servico_id = '$this->servico_id'", "servico_imagem", "../images/servico/");
    }



}
