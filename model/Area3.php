<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class Area3 extends Controle {

    public $db;
    public $area3_id;
    public $area3_nome;
    public $area3_parent;
    public $area3_level;
    public $result;

    public function __construct() {
        parent::__construct();
    }

    public function incluir() {
        $this->insert("area3", "area3_nome, area3_parent, area3_level", "'$this->area3_nome','$this->area3_parent','$this->area3_level'");
    }

    public function atualizar() {
        $this->update("area3", "area3_nome = '$this->area3_nome',area3_parent = '$this->area3_parent', area3_level = '$this->area3_level'", "area3_id = $this->area3_id");
    }

    public function deletar() {
        require_once '../loader.php';
        $p = new Paginas();
        $p->paginas_area3 = $this->area3_id;
        //$p->video_id = $this->area3_id;
        //$p->getBy();
        $p->db->query("SELECT * FROM area3 
                        INNER JOIN paginas ON (paginas_area3 = area3_id) 
                        WHERE area3_id = $this->area3_id")->fetchAll();
        if(isset($p->db->data[0])){
            foreach ($p->db->data as $rem) {
                $p->paginas_id = $rem->paginas_id;
                $p->removerArquivo();
                $p->remover();
            }
        }
        $this->delete("area3", "area3_id =  $this->area3_id");
    }

    public function getAreas3() {
        $this->db->query("SELECT * FROM area3  ORDER BY area3_pos ASC")->fetchAll();
        //$this->select("area2", "", "*", "", " order by area3_pos ASC", "");
    }
   
    public function getArea3($id) {
        $this->select("area3", "", "*", "", "INNER JOIN paginas ON (paginas_area3 = area3_id) WHERE paginas_area3 = $this->area3_id", "");
    }
    
     public function getArea3_() {
        $this->select("area3", "", "*", "", "WHERE area3_id = $this->area3_id", "");
    }
    
    public function getParentArea3($id) {
        $this->select("area3", "area3_id = '$this->area3_id'");
    }
    
    public function getCatSub() {   
    
    $this->db->query("SELECT x.area3_id as catid, x.area3_nome AS categoria, y.area3_id as subcatid, y.area3_nome as categoria_parent FROM area3 as x LEFT JOIN area3 as y ON x.area3_parent = y.area3_id ORDER BY categoria_parent,categoria ")->fetchAll();

     if (isset($this->db->data[0])) {
            return ($this->db->data);
        }
        
    } 
    
    public function getAreasPai() {
        $this->db->query("SELECT * FROM area3 WHERE area3_parent = '0' ORDER BY area3_pos ASC")->fetchAll();
    }  
     
    
     public function getAreasSub() {
        $this->db->query("SELECT * FROM area3 WHERE area3_parent = $this->area3_id ORDER BY area3_nome ASC")->fetchAll();
    }

    
    public function getPaginasRelacionadas($id,$n) {
        $this->select("area3", "", "*", "", "INNER JOIN paginas ON (paginas_area3 = area3_id) WHERE paginas_area3 = $id AND paginas_id <> $n", "");
    }

    public function getJason() {
        $this->getJSON("area3", "area3_id = '$this->area3_id'");
    }

    public function updatePos() {        
        $item = $_POST['item'];
        if(!empty($item )){
            $this->Posicao($item, "area3", "area3_pos", "area3_id");
        }
    }

}
