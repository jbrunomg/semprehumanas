<?php  
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include_once "../vendor/autoload.php";
require_once 'Smtpr.php';

$smtp = new Smtpr();
$smtp->getSmtp();

$mail = new PHPMailer(true);

try{

    //$mail->SMTPDebug = SMTP::DEBUG_SERVER; 

    $mail->isSMTP();                                          
    $mail->SMTPAuth   = true;                               
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
    $mail->Port = $smtp->smtp_port;
    $mail->Host = $smtp->smtp_host;

    $mail->Username = $smtp->smtp_username;
    $mail->Password = $smtp->smtp_password;
    $mail->setFrom($smtp->smtp_username, $smtp->smtp_fromname);
    $mail->Subject = utf8_decode("Contato Via Site");

    $mail->setFrom($smtp->smtp_username, $smtp->smtp_fromname);

    if($smtp->smtp_bcc != "" || $smtp->smtp_bcc != null)
        $mail->addBCC($smtp->smtp_bcc);

    $mail->addAddress('pbcacheats@gmail.com');

    $mail->isHTML(true);   
    $hour=date('H');
    $data = date('d/m/Y H:i',mktime($hour+1));
    $nome = stripslashes($_POST['nome']);
    $email = stripslashes($_POST['email']);
    $assunto = utf8_decode(stripslashes($_POST['assunto']));
    $mensagem = utf8_decode(stripslashes($_POST['mensagem']));  

    $mail->addReplyTo($email);

    $body = "<b>Data da Mensagem: </b> $data <br />";
    $body .= "<b>Nome:</b> $nome <br />";
    $body .= "<b>E-mail:</b> $email <br />";
    $body .= "<b>Assunto:</b> $assunto <br />";
    $body .= "<b>Mensagem: </b>$mensagem <br /><br />";

    $mail->Body = nl2br($body);

    if($mail->send()){
        echo "<p class='alert alert-success' id='msg_alert'> <strong>Obrigado !</strong> Sua Mensagem foi entregue.</p>";
    } else {
        echo "<p class='alert alert-danger' id='msg_alert'> Erro ao enviar  Mensagem<br>{$mail->ErrorInfo}</p>";
        exit();
    }

}catch(Exception $e){
    echo "<p class='alert alert-danger' id='msg_alert'> Erro ao enviar  Mensagem<br>{$mail->ErrorInfo}</p>";

}

exit();


?>