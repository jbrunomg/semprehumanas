<?php

require_once 'Controle.php';

/**
 * Descrição de Usuario
 * Classe gerenciar usuários
 * @author phpstaff.com.br
 */
class Equipe extends Controle {

    public $bd;
    public $equipe_id;
    public $equipe_nome;
    public $equipe_subtitulo;
    public $equipe_link1;
    public $equipe_link2;
    public $equipe_link3;
    public $equipe_link4;
    public $equipe_descricao;
    public $equipe_imagem;
    public $result;

    public function __construct() {
        parent::__construct();
                $this->db = new DB;  
    }

    public function incluir() {
        $this->insert("equipe", "equipe_nome, equipe_subtitulo, equipe_descricao,  equipe_imagem, equipe_link1, equipe_link2, equipe_link3,equipe_link4", 
                "'$this->equipe_nome','$this->equipe_subtitulo', '$this->equipe_descricao', '$this->equipe_imagem','$this->equipe_link1','$this->equipe_link2','$this->equipe_link3','$this->equipe_link4'");
    }    

    public function atualizar() {
        if ($this->equipe_imagem != "") {
            $this->update("equipe", "equipe_nome = '$this->equipe_nome', equipe_subtitulo = '$this->equipe_subtitulo', equipe_link1 = '$this->equipe_link1',"
                    . "equipe_link2 = '$this->equipe_link2', equipe_link3 = '$this->equipe_link3',equipe_link4 = '$this->equipe_link4' ,equipe_descricao = '$this->equipe_descricao',"
                    . "equipe_imagem = '$this->equipe_imagem'", "equipe_id = '$this->equipe_id'");
        } else {
            $this->update("equipe", "equipe_nome = '$this->equipe_nome', equipe_subtitulo = '$this->equipe_subtitulo', equipe_link1 = '$this->equipe_link1',"
                    . "equipe_link2 = '$this->equipe_link2', equipe_link3 = '$this->equipe_link3',equipe_link4 = '$this->equipe_link4', equipe_descricao = '$this->equipe_descricao'", "equipe_id = '$this->equipe_id'");
        }
    }

    public function remover() {
        $this->delete("equipe", "equipe_id = '$this->equipe_id'");
    }

    public function removerArquivo() {
        $this->deleteArquivo("equipe", "equipe_id = '$this->equipe_id'", "equipe_imagem", "../images/team/");
    }

    public function enviar() {
        $this->upload("../images/team/", "equipe_imagem", "");
    }

    public function getEquipe() {
        $this->select("equipe", "", "*", "", "WHERE equipe_id = $this->equipe_id", "");
    }

    public function getEquipes() {
        $this->select("equipe");
    }

    public function JSON() {
        $this->getJSON("equipe", "equipe_id = '$this->equipe_id'");
    }

}
