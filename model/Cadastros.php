<?php

require_once 'Controle.php';

class Cadastros extends Controle {

    public $db;
    public $cadastro_id;
    public $cadastro_email;
    public $cadastro_data;
    public $cadastro_status;

    public function __construct() {
        parent::__construct();
        $this->data = date('Y-m-d');
    }
    
    public function incluir() {
        $this->insert("cadastros", "cadastro_email,  cadastro_data", "'$this->cadastro_email','$this->data'");
    }
    
    public function atualizar() {
        $query = "cadastro_email = '$this->cadastro_email', cadastro_data = '$this->cadastro_data'";
      
        $this->update("cadastros", "$query", "cadastro_id = '$this->cadastro_id'");
    }

    public function getCadastro($cadastro_id) {
        $this->select("cadastros", "", "*", "", " WHERE cadastro_id = $cadastro_id", "");
    }
    
    public function getCadastroEmail() {
        $this->select("cadastros", "", "*", "", " WHERE cadastro_email = '".$this->cadastro_email."'", "");
        
    }

    public function getCadastros() {
        $this->select("cadastros", "", "*", "", "order by cadastro_id DESC", "");
    }

    public function remover() {
        $this->delete("cadastros", "cadastro_id = $this->cadastro_id");
    }

    public function mudarStatus($cadastro_id, $cadastro_status) {
        $this->Moderar("cadastros", "cadastro_status", "cadastro_id", "$cadastro_id", "$cadastro_status");
    }

    public function Contar($id) {
        $this->getCount("cadastros", "");
    }
    
    public function JSON() {
        $this->getJSON("cadastros", "cadastro_id = '$this->cadastro_id'");
    }

}