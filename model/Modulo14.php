<?php

require_once 'Controle.php';

/**
 * Descrição de Usuario
 * Classe gerenciar usuários
 * @author phpstaff.com.br
 */
class Modulo14 extends Controle {

    public $db;
    public $modulo14_id;
    public $modulo14_icon1;
    public $modulo14_text1;
    public $modulo14_descricao1;
    public $modulo14_icon2;
    public $modulo14_text2;
    public $modulo14_descricao2;
    public $modulo14_icon3;
    public $modulo14_text3;
    public $modulo14_descricao3;
    public $modulo14_icon4;
    public $modulo14_text4;
    public $modulo14_descricao4;
    public $modulo14_imagem;
    public $modulo14_status;
    public $result;

    public function __construct() {
        parent::__construct();
    }

    public function atualizar() {
        if ($this->modulo14_imagem != "") {
            $this->update("modulo14", "modulo14_icon1 = '$this->modulo14_icon1' ,modulo14_text1 = '$this->modulo14_text1' ,modulo14_descricao1 = '$this->modulo14_descricao1', modulo14_icon2 = '$this->modulo14_icon2' , modulo14_text2 = '$this->modulo14_text2',"
                    . "modulo14_descricao2  = '$this->modulo14_descricao2', modulo14_icon3 = '$this->modulo14_icon3' , modulo14_text3  = '$this->modulo14_text3', modulo14_descricao3  = '$this->modulo14_descricao3', modulo14_icon4 = '$this->modulo14_icon4' , modulo14_text4  = '$this->modulo14_text4',"
                    . " modulo14_descricao4  = '$this->modulo14_descricao4', modulo14_imagem  = '$this->modulo14_imagem',"
                    . "modulo14_status  = '$this->modulo14_status'", "modulo14_id = '$this->modulo14_id'");
        } else {
            $this->update("modulo14", "modulo14_icon1 = '$this->modulo14_icon1' , modulo14_text1 = '$this->modulo14_text1' ,modulo14_descricao1 = '$this->modulo14_descricao1', modulo14_icon2 = '$this->modulo14_icon2' , modulo14_text2 = '$this->modulo14_text2',"
                    . "modulo14_descricao2  = '$this->modulo14_descricao2', modulo14_icon3 = '$this->modulo14_icon3' , modulo14_text3  = '$this->modulo14_text3', modulo14_descricao3  = '$this->modulo14_descricao3', modulo14_icon4 = '$this->modulo14_icon4' , modulo14_text4  = '$this->modulo14_text4',"
                    . " modulo14_descricao4  = '$this->modulo14_descricao4', modulo14_status  = '$this->modulo14_status'", "modulo14_id = '$this->modulo14_id'");
        }
    }

    public function removerArquivo() {
        $this->deleteArquivo("modulo14", "modulo14_id = '$this->modulo14_id'", "modulo14_imagem", "../images/");
    }

    public function enviar() {
        $this->upload("../images/", "modulo14_imagem", "");
    }

    public function getModulo14() {
        $this->select("modulo14", "", "*", "", "WHERE modulo14_id = 1", "");
    }

}
