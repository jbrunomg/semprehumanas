<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class Paginas extends Controle {

    public $db;
    public $paginas_id;
    public $paginas_nome;
    public $paginas_descricao;
    public $paginas_imagem;
    public $paginas_area3;
    public $result;

    public function __construct() {
        parent::__construct();         
    }

    public function incluir() {
        $this->insert("paginas", "paginas_nome, paginas_imagem, paginas_descricao, paginas_area3", "'$this->paginas_nome',"
                . "  '$this->paginas_imagem','$this->paginas_descricao', '$this->paginas_area3'");
    }

    public function atualizar() {
        if ($this->paginas_imagem != "") {
            $this->update("paginas", "paginas_nome = '$this->paginas_nome', paginas_imagem = '$this->paginas_imagem', paginas_descricao  = '$this->paginas_descricao', paginas_area3 = '$this->paginas_area3'"
                    . ",paginas_imagem = '$this->paginas_imagem'", "paginas_id = '$this->paginas_id'");
        } else {
            $this->update("paginas", "paginas_nome = '$this->paginas_nome', paginas_descricao  = '$this->paginas_descricao', paginas_area3 = '$this->paginas_area3'", "paginas_id = '$this->paginas_id'");
        }
    }

    public function remover() {
        $sql = "delete from paginas where paginas_id = $this->paginas_id";
        $this->db->query("$sql");        
    }

    public function removerArquivo() {
        $this->deleteArquivo("paginas", "paginas_id = '$this->paginas_id'", "paginas_imagem", "../images/paginas/");
    }

    public function enviar() {
        $this->upload("../images/paginas/", "paginas_imagem", "");
    }
    

    public function GetMyPaginas() {
        $this->select("paginas", "", "*", "", "INNER JOIN area3 ON(paginas_area3 = area3_id)LEFT JOIN fotos ON(fotos_paginas = paginas_id) WHERE paginas_id = $this->paginas_id", "");
    }


    public function getByPaginas() {
        $this->select("area3", "", "*", "", "INNER JOIN paginas ON (paginas_area3 = area3_id) LEFT JOIN fotos ON(fotos_paginas = paginas_id) WHERE paginas_id = $this->paginas_id", "");
    }
    
      public function getCategorias() {
        $this->select("paginas", "", "*", "", "INNER JOIN area3 ON (paginas_area3 = area3_id) WHERE paginas_area3 = $this->paginas_id ORDER BY paginas_nome DESC", "");
    }
    
     public function getPaginasRight() {
        $paginas_id = intval($_GET['id']);
       if ($paginas_id != '') {
        $onde = "WHERE paginas_id != '".$paginas_id."' AND paginas_imagem != ''";
        } else {
        $onde = "WHERE paginas_imagem != ''";
        }
       $this->select("paginas", "", "*", "", "INNER JOIN area3 ON (paginas_area3 = area3_id) ".$onde." ORDER BY paginas_id DESC LIMIT 0,5", "");
    
    }

    public function getPaginasAll() {
        $this->select("paginas", "", "*", "paginas_id DESC", "INNER JOIN area3 ON (paginas_area3 = area3_id)", "");
    }
     public function getPaginasHome() {
        $this->select("paginas", "", "*", "", "INNER JOIN area3 ON (paginas_area3 = area3_id) WHERE paginas_imagem != '' ORDER BY paginas_id DESC LIMIT 0,12", "");
    }
    
      public function getPaginasArea3() {
        $this->select("paginas", "", "*", "paginas_id DESC", "INNER JOIN area3 ON (paginas_area3 = area3_id) WHERE paginas_area3 = $this->paginas_area3 AND paginas_id != $this->paginas_id", "");
    }


    public function getUltimas() {
        $this->select("paginas", "", "*", "", "WHERE paginas_imagem != '' ORDER BY paginas_id DESC LIMIT 0,6", "");
    }


    public function getFotos($id) {
        $this->select("fotos", "", "*", "", "where fotos_paginas = $id", "");
    }
    
     public function getSearch() {
        $busca = stripslashes($_POST['busca']);
        $this->db->query("SELECT * FROM paginas INNER JOIN area3 ON (paginas_area3 = area3_id) WHERE paginas_nome like '%".$busca."%' or paginas_descricao LIKE '%".$busca."%'")->fetchAll(); //OR 
        //$this->db->data;
        if (isset($this->db->data[0])) {
            return ($this->db->data);
        } else {
            echo '<div class="single-news">
								<div class="news-head">
									<div class="news-content"><h5>Desculpe: nenhum resultado para <strong>"'.$busca.'"</strong><h5><br /></div></div></div>';
        }
    }

    public function JSON() {
        $this->getJSON("paginas", "paginas_id = '$this->paginas_id'");
    }

}