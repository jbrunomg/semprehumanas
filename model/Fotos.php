<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class Fotos extends Controle {

    public $db;
    public $fotos_id;
    public $fotos_url;
    public $fotos_pos;
    public $fotos_paginas;
    public $result;

    public function __construct() {
        parent::__construct();
    }

    public function incluir() {
        $this->insert("fotos", "fotos_url, fotos_paginas", "'$this->fotos_url', '$this->fotos_paginas'");
    }

    public function getFotosAll() {
        $this->select("fotos", "", "*", "", " INNER JOIN paginas on (fotos_paginas = paginas_id) where fotos_paginas = $this->fotos_paginas ORDER BY fotos_pos ASC", "");
    }

  
    public function remover() {
        $this->delete("fotos", "fotos_id = '$this->fotos_id'");
    }

    public function removerImagem() {
        $this->deleteArquivo("fotos", "fotos_id = '$this->fotos_id'", "fotos_url", "../images/paginas/");
    }

}