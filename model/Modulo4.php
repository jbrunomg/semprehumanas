<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class Modulo4 extends Controle {

    public $db;
    public $modulo4_id;
    public $modulo4_top;
    public $modulo4_side;
    public $modulo4_side1;
    public $modulo4_bottom;
    public $modulo4_status;
    public $result;

    public function __construct() {
        parent::__construct();
        require_once 'Registry.php';
        $registry = Registry::getInstance();
        if( $registry->get('db') == false ) {
            $registry->set('db', new DB);
        }
        $this->db = $registry->get('db');           
    }

    public function atualizar() {
        $query = "modulo4_top = '$this->modulo4_top', modulo4_side = '$this->modulo4_side', modulo4_side1 = '$this->modulo4_side1', modulo4_bottom = '$this->modulo4_bottom', modulo4_status = '$this->modulo4_status'";
        $this->update("modulo4", "$query", "modulo4_id = '$this->modulo4_id'");
    }

    public function getModulo4() {
        $this->select("modulo4", "", "*", "", "WHERE modulo4_id = 1", "");
    }

}
