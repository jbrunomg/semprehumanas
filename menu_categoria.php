<div class="single-sidebar category">
	<h2><?= stripslashes($modulo2->modulo2_nome3) ?> <span>Categorias</span></h2>
	 <ul>
		<?php $categoria_blog->getMenu() ?>
        <?php if (isset($categoria_blog->db->data[0])): ?>
            <?php foreach ($categoria_blog->db->data as $c): ?>
              <li><a href="categoria/blog/<?= Filter::slug2($c->area_nome) ?>/<?= $c->area_id ?>/"><?= $c->area_nome ?></a></li>
            <?php endforeach; ?>
        <?php endif; ?>
            <li><a href="blog/">Todas as Categorias</a></li>			
	 </ul>
</div>