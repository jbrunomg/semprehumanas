<?php
//die('AQUI');
require_once './loader.php';
@session_start();
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl() . "/thumb.php?w=200&src=images/" . $sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';


?>


<style>
  h1 {
    font-family: arial, sans-serif;
    font-size: 15pt;

  }

  hr {
    border-color: orangered;
  }
</style>
<link href="./admin/assets/css/datepicker.css" rel="stylesheet">
<body class="js">

  <!-- Preloader -->
  <!-- <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
    <div class="loader-inner ball-scale-ripple-multiple vh-center">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div> -->
  <!-- End Preloader -->

  <?php require_once './menu.php'; ?>


  <!-- Start Breadcrumbs -->
  <section class="breadcrumbs" <?php if (!empty($sobre->modulo3_imagem)) {
                                  echo "style='background: url(thumb.php?w=1280&zc=0&src=images/" . stripslashes($sobre->modulo3_imagem) . ");'";
                                } ?>>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
          <ul>
            <li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
            <li class="active"><a href="estudantes/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!--/ End Breadcrumbs -->


  <section id="estudante" class="features section" style="padding-top: 70px;">
   <form method="post" action="estudante/estudanteCadastro/">
    <div class="container">
      <H1>Cadastro de Estudante</H1>
      <hr>
      <div class="container">

      <?php if (isset($_GET['erro'])): ?>
          <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h6><i class="icon fa fa-check">  Aconteceu um erro contate o Suporte!</i></h6>                 
          </div>
      <?php endif; ?>

      <?php if (isset($_GET['errocpf'])): ?>
          <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h6><i class="icon fa fa-check"> CPF já cadastrado, favor verificar se o CPF foi digitado corretamente!</i></h6>                 
          </div>
      <?php endif; ?>

      <?php if (isset($_GET['errocpfvazio'])): ?>
          <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h6><i class="icon fa fa-check"> CPF incorreto, favor verificar se o CPF foi digitado corretamente!</i></h6>                 
          </div>
      <?php endif; ?>

      <?php if (isset($_GET['erroemail'])): ?>
          <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h6><i class="icon fa fa-check"> Ocorreu um error no Email informado, Entrar em contato com Suporte!</i></h6>                 
          </div>
      <?php endif; ?>


        <!-- <div class="row">
          <div class="col-lg-4">
          </div>
          <div class="col-lg-4">
            
              <div class="form-group">
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Você ainda não enviou o seu curriculo?</label>
                <input type="file" id="exampleInputFile">
                <p class="help-block">Enviar arquivos apenas em .pdf ou .word</p>
                <label for="exampleInputFile">Anexo-CV</label>
              </div>
              <table class="table">
                ...
              </table>
            
          </div>
          <div class="col-lg-4">
          </div>
        </div> -->
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            
              <div class="form-group">
                <label for="exampleInputEmail1">Nome:</label>
                <input required type="text" class="form-control" id="estudante_nome" name="estudante_nome" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualnome ?>" placeholder="">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">CPF:</label>
                <input required type="text" class="form-control" maxlength="11" id="estudante_cpf" name="estudante_cpf" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualcpf ?>" placeholder="">
              </div>
            
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            
              <div class="form-group">
                <label for="exampleInputEmail1">Rg:</label>
                <input required type="text" class="form-control"  id="estudante_rg" name="estudante_rg" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualrg ?>" placeholder="">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Naturalidade:</label>
                <input type="text" class="form-control"  id="estudante_salualnatur" name="estudante_salualnatur" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualnatur ?>" placeholder="">
              </div>
            
          </div>
          <div class="col-lg-6">
            
              <div class="form-group">
                <label for="exampleInputEmail1">Orgão Expedição:</label>
                <input type="text" class="form-control" id="estudante_rg_org_expd" name="estudante_rg_org_expd" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualrg_org_expd ?>" placeholder="">
              </div>
              <div class="form-group">
                <label for="sel1">Estado Civil:</label>
                <select class="form-control" id="estudante_salualestci" name="estudante_salualestci">
                  <option value='Solteiro' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualestci =='Solteiro'?'selected':'';?>>Solteiro(a)</option>
                  <option value='Casado' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualestci =='Casado'?'selected':'';?>>Casado(a)</option>
                  <option value='Separado' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualestci =='Separado'?'selected':'';?>>Separado(a)</option>
                  <option value='Divorciado' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualestci =='Divorciado'?'selected':'';?>>Divorciado(a)</option>
                  <option value='Viuvo' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualestci =='Viuvo'?'selected':'';?>>Viuvo(a)</option>
                </select>
              </div>
            
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            
              <div class="form-group">
                <label for="exampleInputEmail1">Data Nascimento:</label>
                <input required type="date" class="form-control" id="estudante_talualniver" name="estudante_talualniver" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->talualniver ?>" placeholder="">
              </div>
            
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <p><strong> Gênero: </strong></p>
            <label class="radio-inline">
              <input type="radio" id="estudante_ealualsexo" name="estudante_ealualsexo" value="0" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualsexo == 0) ? 'checked' : ''; ?>>  Masculino
            </label>
            <label class="radio-inline">
              <input type="radio" id="estudante_ealualsexo" name="estudante_ealualsexo" value="1" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualsexo == 1) ? 'checked' : ''; ?>>  Feminino
            </label>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <p><strong>Habilitação: </strong></p>
            <label class="radio-inline">
              <input type="radio" id="estudante_ealualhabil" name="estudante_ealualhabil" value="0" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualhabil == 0) ? 'checked' : ''; ?>> CNH-sim
            </label>
            <label class="radio-inline">
              <input type="radio" id="estudante_ealualhabil" name="estudante_ealualhabil" value="1" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualhabil == 1) ? 'checked' : ''; ?>> CNH-não
            </label>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <p><strong>Carro Próprio: </strong></p>
            <label class="radio-inline">
              <input type="radio" id="estudante_ealualvepro" name="estudante_ealualvepro" value="0" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualvepro == 0) ? 'checked' : ''; ?>> Sim
            </label>
            <label class="radio-inline">
              <input type="radio" id="estudante_ealualvepro" name="estudante_ealualvepro" value="1" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualvepro == 1) ? 'checked' : ''; ?>> Não
            </label>
          </div>
        </div>
      </div>

      <br>

      <h1>Dados Afiliação</h1>

      <hr>

      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            
              <div class="form-group">
                <label for="exampleInputEmail1">Pai:</label>
                <input required type="text" class="form-control" id="estudante_salualpai" name="estudante_salualpai" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualpai ?>" placeholder="">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Mãe:</label>
                <input required type="text" class="form-control" id="estudante_salualmae" name="estudante_salualmae" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualmae ?>" placeholder="">
              </div>
            
          </div>

          <div class="col-lg-6">
            
              <div class="form-group">
                <label for="exampleInputEmail1">Profissão:</label>
                <input type="text" class="form-control" id="estudante_salualpprof" name="estudante_salualpprof" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualpprof ?>" placeholder="">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Profissão:</label>
                <input type="text" class="form-control" id="estudante_salualmprof" name="estudante_salualmprof" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualmprof ?>" placeholder="">
              </div>
            
          </div>
        </div>

        <br>

        <h1>Dados Logradouro</h1>

        <hr>

        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              
                <div class="form-group">
                  <label for="exampleInputEmail1">CEP:</label>
                  <input type="text" class="form-control" id="estudante_salualcep" name="estudante_salualcep" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualcep ?>" placeholder="">
                </div>
              
            </div>
            <div class="col-lg-4">
              
                <div class="form-group">
                  <label for="exampleInputEmail1">Endereço:</label>
                  <input required type="text" class="form-control" id="estudante_saluallogra" name="estudante_saluallogra" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->saluallogra ?>" placeholder="">
                </div>
              
            </div>
            <div class="col-lg-4">
              
                <div class="form-group">
                  <label for="exampleInputEmail1">Nº:</label>
                  <input type="text" class="form-control" id="estudante_salualnumer" name="estudante_salualnumer" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualnumer ?>" placeholder="">
                </div>
              
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              
                <div class="form-group">
                  <label for="exampleInputEmail1">Complemento:</label>
                  <input type="text" class="form-control" id="estudante_salualcompl" name="estudante_salualcompl" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualcompl ?>" placeholder="">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">UF:</label>
                  <input required type="text" class="form-control" id="estudante_salualestad" name="estudante_salualestad" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualestad ?>" placeholder="">
                </div>
              
            </div>
            <div class="col-lg-6">
              
                <div class="form-group">
                  <label for="exampleInputEmail1">Bairro:</label>
                  <input required type="text" class="form-control" id="estudante_salualbairr" name="estudante_salualbairr" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualbairr ?>" placeholder="">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Cidade:</label>
                  <input required type="text" class="form-control" id="estudante_salualcidad" name="estudante_salualcidad" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualcidad ?>" placeholder="">
                </div>
              
            </div>
          </div>
        </div>

        <br>

        <h1>Dados Contato</h1>
        <hr>
        <div class="container">
          <div class="row">
            <div class="col-lg-6">

              
                <div class="form-group">
                  <label for="exampleInputEmail1">Telefone:</label>
                  <input type="text" class="form-control" data-inputmask="'mask': ['()-9999-9999 [x99999]', '+099 99 99 9999[9]-9999'], 'showTooltip': false" id="estudante_salualtel01" name="estudante_salualtel01" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualtel01 ?>" placeholder="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Celular:</label>
                  <input type="text" class="form-control" data-inputmask="'mask': ['()-9999-9999 [x99999]', '+099 99 99 9999[9]-9999'], 'showTooltip': false" id="estudante_salualtel02" name="estudante_salualtel02" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualtel02 ?>" placeholder="">
                </div>
              
            </div>
            <div class="col-lg-6">

              <div class="form-group">
                <label for="exampleInputPassword1">E-Mail:</label>
                <input required type="email" class="form-control" id="estudante_salualemail" name="estudante_salualemail" value="<?php echo isset($_SESSION['ESTUDANTE']['EMAILCADASTRO']) ? $_SESSION['ESTUDANTE']['EMAILCADASTRO'] : $_SESSION['ESTUDANTE']['DADOS'][0]->salualemail ;?>" placeholder="">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Celular II:</label>
                <input type="text" class="form-control" data-inputmask="'mask': ['()-9999-9999 [x99999]', '+099 99 99 9999[9]-9999'], 'showTooltip': false" id="estudante_salualtel03" name="estudante_salualtel03" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualtel03 ?>" placeholder="">
              </div>
            </div>
          </div>
        </div>

        <br>

        <h1>Dados Acadêmicos</h1>
        <hr>
        <div class="container">
          <div class="row">
            <div class="col-lg-6">

              <div class="form-group">
                <label for="sel1">Curso:</label>
                <select required class="form-control" id="estudante_ialualcurso" name="estudante_ialualcurso">
                <?php foreach ($_SESSION['CURSOS'] as $curso) { $select = ''; ?>
                      <?php
                        if ($curso->pcurcucodig == $_SESSION['ESTUDANTE']['DADOS'][0]->ialualcurso) {
                        $select = "selected";

                      } ?>
                      <option value="<?php echo $curso->pcurcucodig; ?>"<?php echo $select ?>><?php echo $curso->scurcunome; ?></option>
                    <?php  } ?>  
                </select>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="sel1">Nível:</label>
                <select required class="form-control" id="estudante_ialualcurso_nivel" name="estudante_ialualcurso_nivel">
                  <option value='Médio' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->ialualcurso_nivel =='Médio'?'selected':'';?>>Médio</option>
                  <option value='Técnico' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->ialualcurso_nivel =='Técnico'?'selected':'';?>>Técnico</option>
                  <option value='Superior' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->ialualcurso_nivel =='Superior'?'selected':'';?>>Superior</option>
                  <option value='Pós Graduação' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->ialualcurso_nivel =='Pós Graduação'?'selected':'';?>>Pós Graduação</option>            
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                <label for="sel1">Instituição de Ensino:</label>
                <select required class="form-control" id="estudante_ialualinsen" name="estudante_ialualinsen">
                    <?php foreach ($_SESSION['INSTITUICAO'] as $inst) { $select = ''; ?>
                      <?php
                        if ($inst->pensencodig == $_SESSION['ESTUDANTE']['DADOS'][0]->ialualinsen) {
                        $select = "selected";

                      } ?>
                      <option value="<?php echo $inst->pensencodig; ?>"<?php echo $select ?>><?php echo $inst->sensennome; ?></option>
                    <?php  } ?>  
                </select>
              </div>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <div class="form-group">
                <label for="sel1">Período/Série:</label>
                <select required class="form-control" id="estudante_salualperio" name="estudante_salualperio">
                  <option value='1º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='1º'?'selected':'';?>>1º</option> 
                  <option value='2º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='2º'?'selected':'';?>>2º</option> 
                  <option value='3º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='3º'?'selected':'';?>>3º</option> 
                  <option value='4º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='4º'?'selected':'';?>>4º</option> 
                  <option value='5º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='5º'?'selected':'';?>>5º</option> 
                  <option value='6º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='3º'?'selected':'';?>>3º</option> 
                  <option value='7º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='7º'?'selected':'';?>>7º</option> 
                  <option value='8º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='8º'?'selected':'';?>>8º</option> 
                  <option value='9º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='9º'?'selected':'';?>>9º</option> 
                  <option value='10º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='10º'?'selected':'';?>>10º</option> 
                  <option value='11º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='11º'?'selected':'';?>>11º</option> 
                  <option value='12º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='12º'?'selected':'';?>>12º</option> 
                  <option value='13º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='13º'?'selected':'';?>>13º</option> 
                  <option value='14º' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualperio =='14º'?'selected':'';?>>14º</option>         
                </select>
              </div>

            </div>
            <div class="col-lg-4">
              <div class="form-group">
                <label for="sel1">Turno:</label>
                <select class="form-control" id="estudante_salualturno" name="estudante_salualturno">
                  <option value='Manhã' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualturno =='Manhã'?'selected':'';?>>Manhã</option>
                  <option value='Tarde' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualturno =='Tarde'?'selected':'';?>>Tarde</option>
                  <option value='Noite' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualturno =='Noite'?'selected':'';?>>Noite</option>
                  <option value='Manhã/Tarde' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualturno =='Manhã/Tarde'?'selected':'';?>>Manhã/Tarde</option>
                  <option value='Manhã/Noite' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualturno =='Manhã/Noite'?'selected':'';?>>Manhã/Noite</option>
                  <option value='Tarde/Noite' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualturno =='Tarde/Noite'?'selected':'';?>>Tarde/Noite</option>
                  <option value='Integral' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualturno =='Integral'?'selected':'';?>>Integral</option>
                  <option value='EAD' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualturno =='EAD'?'selected':'';?>>EAD</option>                  
                </select>
              </div>
            </div>

            <div class="col-lg-4">
              
                <div class="form-group">
                  <label for="exampleInputEmail1">Termino Curso:</label>
                  <input type="text" class="form-control" id="estudante_salualtermino" name="estudante_salualtermino" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualtermino ?>" placeholder="">
                </div>
              
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="sel1">Disponibilidade para Trabalho:</label>
                <select required class="form-control" id="estudante_salualdispo" name="estudante_salualdispo">
                  <option value='Manhã' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualdispo =='Manhã'?'selected':'';?>>Manhã</option>
                  <option value='Tarde' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualdispo =='Tarde'?'selected':'';?>>Tarde</option>
                  <option value='Noite' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualdispo =='Noite'?'selected':'';?>>Noite</option>
                  <option value='Manhã/Tarde' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualdispo =='Manhã/Tarde'?'selected':'';?>>Manhã/Tarde</option>
                  <option value='Manhã/Noite' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualdispo =='Manhã/Noite'?'selected':'';?>>Manhã/Noite</option>
                  <option value='Tarde/Noite' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualdispo =='Tarde/Noite'?'selected':'';?>>Tarde/Noite</option>
                  <option value='Integral' <?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualdispo =='Integral'?'selected':'';?>>Integral</option>
                </select>
              </div>
            </div>
            <div class="col-lg-6">
              
                <div class="form-group">
                  <label for="exampleInputEmail1">Nº: Matrícula:</label>
                  <input type="text" class="form-control" id="estudante_salualmatricula" name="estudante_salualmatricula" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualmatricula ?>" placeholder="">
                </div>
              
            </div>
          </div>
        </div>

        <br>

        <h1>Conhecimento Informática</h1>
        <hr>
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <p><strong>Conhecimento em Informatica:</strong></p>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-3">
              <label class="checkbox-inline"><input type="checkbox" id="estudante_ealualwindo" name="estudante_ealualwindo" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualwindo == 1) ? 'checked' : ''; ?>>Windows</label>
              <label class="checkbox-inline"><input type="checkbox" id="estudante_ealualword" name="estudante_ealualword" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualword == 1) ? 'checked' : ''; ?>>Word</label>
            </div>
            <div class="col-lg-3">
              <label class="checkbox-inline"><input type="checkbox" id="estudante_ealualexel" name="estudante_ealualexel" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualexel == 1) ? 'checked' : ''; ?>>Excel</label>
              <label class="checkbox-inline"><input type="checkbox" id="estudante_ealualpower" name="estudante_ealualpower" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualpower == 1) ? 'checked' : ''; ?>>Power Point</label>
            </div>
            <div class="col-lg-3">
              <label class="checkbox-inline"><input type="checkbox" id="estudante_ealuallinux" name="estudante_ealuallinux" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealuallinux == 1) ? 'checked' : ''; ?>>Linux</label>
              <label class="checkbox-inline"><input type="checkbox" id="estudante_ealualinter" name="estudante_ealualinter" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualinter == 1) ? 'checked' : ''; ?>>Internet</label>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <p><strong>Idioma:</strong></p>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-3">
              <label class="checkbox-inline"><input type="checkbox" id="estudante_ealualingle" name="estudante_ealualingle" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualingle == 1) ? 'checked' : ''; ?>>Inglês</label>
              <label class="checkbox-inline"><input type="checkbox" id="estudante_ealualespan" name="estudante_ealualespan" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualespan == 1) ? 'checked' : ''; ?>>Espanhol</label>
            </div>
            <div class="col-lg-3">
              <label class="checkbox-inline"><input type="checkbox" id="estudante_ealualfranc" name="estudante_ealualfranc" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualfranc == 1) ? 'checked' : ''; ?>>Frânces</label>
              <label class="checkbox-inline"><input type="checkbox" id="estudante_ealualalema" name="estudante_ealualalema" <?php echo ($_SESSION['ESTUDANTE']['DADOS'][0]->ealualalema == 1) ? 'checked' : ''; ?>>Alemão</label>
            </div>
            <div class="col-lg-3">
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <p><strong>Outros Idiomas:</strong></p>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                <label for="comment"></label>
                <textarea class="form-control" rows="5"  id="estudante_salualoutro" name="estudante_salualoutro" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualoutro ?>"></textarea>
              </div>
            </div>
          </div>
        </div>

        <br>

        <h1>Informações Profissionais</h1>
        <hr>

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                <label for="comment">Experiências Profissionais:</label>
                <textarea class="form-control" rows="5" id="estudante_salualexper" name="estudante_salualexper" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualexper ?>"></textarea>
              </div>
              <div class="form-group">
                <label for="comment">Observações:</label>
                <textarea class="form-control" rows="5" id="estudante_salualobser" name="estudante_salualobser" value="<?php echo $_SESSION['ESTUDANTE']['DADOS'][0]->salualobser ?>"></textarea>
              </div>

              <br>

              <button type="submit" class="btn btn-warning btn-lg">Enviar</button>
            </div>
          </div>
        </div>
      </div>
    </form>

  </section>







  <?php require_once './footer.php'; ?>
  <!-- Jquery -->
  <script type="text/javascript" src="js\jquery.min.js"></script>
  <!-- Colors -->
  <script type="text/javascript" src="js\colors.js"></script>
  <!-- Modernizr JS -->
  <script type="text/javascript" src="js\modernizr.min.js"></script>
  <!-- Appear Js -->
  <script type="text/javascript" src="js\jquery.appear.js"></script>
  <!-- Scrool Up -->
  <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
  <!-- Typed Js -->
  <script type="text/javascript" src="js\typed.min.js"></script>
  <!-- Slick Nav -->
  <script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
  <!-- Onepage Nav -->
  <script type="text/javascript" src="js\jquery.nav.js"></script>
  <!-- Yt Player -->
  <script type="text/javascript" src="js\ytplayer.min.js"></script>
  <!-- Magnific Popup -->
  <script type="text/javascript" src="js\magnific-popup.min.js"></script>
  <!-- Wow JS -->
  <script type="text/javascript" src="js\wow.min.js"></script>
  <!-- Counter JS -->
  <script type="text/javascript" src="js\waypoints.min.js"></script>
  <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
  <!-- Isotop JS -->
  <script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
  <!-- Masonry JS -->
  <script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="js\slick.min.js"></script>
  <!-- Bootstrap JS -->
  <script type="text/javascript" src="js\bootstrap.min.js"></script>
  <!-- Activate JS -->
  <script type="text/javascript" src="js\active.js"></script>
  <!-- Custom  -->
  <script type="text/javascript" src="js\jquery.maskedinput.min.js"></script>
  <script type="text/javascript" src="js\bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="js\bootstrap-datepicker.pt-BR.js"></script>

  <script>
    $('li#services').addClass('current');
  </script>
  <script>

      $(function ($) {
            $("#estudante_salualtel01").mask("(99) 9999-9999?9");
            $("#estudante_salualtel02").mask("(99) 9999-9999?9");
            $("#estudante_salualtel03").mask("(99) 9999-9999?9");
            $("#estudante_salualcep").mask("99999-999");
            $("#estudante_cpf").mask("999.999.999-99");
            
      });

      $('#estudante_salualtermino').datepicker({
            format: "mm/yyyy",
            language: "pt-BR"
        });


      $('#estudante_salualcep').blur(function(){
   
          var cep = $('#estudante_salualcep').val();
          cep = cep.replace('-','');

          if($.trim(cep) != ""){


            $.getScript("https://www.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
              // o getScript dá um eval no script, então é só ler!
              //Se o resultado for igual a 1 
                
                if(resultadoCEP["resultado"]){
                    
                    if (resultadoCEP['resultado'] == '1' || resultadoCEP['resultado'] == '2'){
                      
                        $("#estudante_saluallogra").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
                        $("#estudante_salualbairr").val(unescape(resultadoCEP["bairro"]));
                        $("#estudante_salualestad").val(unescape(resultadoCEP["uf"]));
                        $("#estudante_salualcidad").val(unescape(resultadoCEP["cidade"]));

                    }else{ 

                        $("#estudante_saluallogra").val('');
                        $("#estudante_salualbairr").val('');
                        $("#estudante_salualestad").val('');
                        $("#estudante_salualcidad").val('');

                    }
                  
                }
                            
            });
            
          }
      
      });

      
          

  </script>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</body>

</html>