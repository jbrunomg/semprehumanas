<?php
require_once '../loader.php';
@session_start();
require_once '../database/DB.php';
$db = new DB();
if (isset($_SESSION['ESTUDANTE']['ID'])) {

    $id = $_SESSION['ESTUDANTE']['ID'];
    $db->listarVagas = "SELECT 
    (SELECT 
        GROUP_CONCAT(scurcunome) 
        FROM
        tbcurcursos 
        WHERE FIND_IN_SET(
            pcurcucodig,
            SUBSTRING(
            REPLACE(sestvacurso, ';', TRIM(', ')),
            2,
            LENGTH(sestvacurso) - 2
            )
        )) AS sestvacurso,
        `sestvaperio`,
        `sestvamodul`,
        `sestvaserie`,
        `sestvacarho`,
        CONCAT(sestvabairr, '/', tb_cidades.`nome`) AS lugar,
        CONCAT(
        IF(eestvabolsa = 1, '', 'Bolsa '),    
        IF(eestva_valorauxilio IS NULL, '', CAST(eestva_valorauxilio AS CHAR CHARACTER SET utf8)),
        IF( eestvavale = 1, '',' + Vale Transporte'),
        IF( eestvarefei = 1,'',' + Refeição'),
        IF( eestvaoutros = 1,'',' + Outros')
        ) AS beneficios,
        `sestvaativi`,
        `observacaovaga`,
        `pestvacodig`,
        `falualcodig`,
        paluvagcodig
        FROM
            `tbestvagas` 
            LEFT JOIN `tbalunovagas` 
            ON `tbestvagas`.`pestvacodig` = `tbalunovagas`.`festvacodig`
            
            LEFT JOIN `tb_cidades` 
            ON  tbestvagas.`sestvacidad` = tb_cidades.`id`  
            
        WHERE `tbestvagas`.vaga_visivel = 1 AND  `testvaexpir` >= CURDATE() AND `tbalunovagas`.`falualcodig` = ".$id. "
        
        UNION
        
        SELECT 
            (SELECT 
            GROUP_CONCAT(scurcunome) 
            FROM
            tbcurcursos 
            WHERE FIND_IN_SET(
                pcurcucodig,
                SUBSTRING(
                REPLACE(sestvacurso, ';', TRIM(', ')),
                2,
                LENGTH(sestvacurso) - 2
                )
            )) AS sestvacurso,
            `sestvaperio`,
            `sestvamodul`,
            `sestvaserie`,
            `sestvacarho`,
            CONCAT(sestvabairr, '/', tb_cidades.`nome`) AS lugar,
            CONCAT(
            IF(eestvabolsa = 1, '', 'Bolsa '),
                IF(eestva_valorauxilio IS NULL, '', CAST(eestva_valorauxilio AS CHAR CHARACTER SET utf8)),
            IF( eestvavale = 1, '',' + Vale Transporte'),
            IF( eestvarefei = 1,'',' + Refeição'),
            IF( eestvaoutros = 1,'',' + Outros')
            ) AS beneficios,
            `sestvaativi`,
            `observacaovaga`,
            `pestvacodig`,
            `falualcodig`,
            paluvagcodig
        FROM
            `tbestvagas` 
            LEFT JOIN `tbalunovagas` 
            ON `tbestvagas`.`pestvacodig` = `tbalunovagas`.`festvacodig`
            LEFT JOIN `tb_cidades` 
            ON  tbestvagas.`sestvacidad` = tb_cidades.`id`    
        WHERE `tbestvagas`.vaga_visivel = 1 AND `testvaexpir` >= CURDATE() AND pestvacodig NOT IN (
        SELECT 
            `pestvacodig`
        FROM
            `tbestvagas` 
            LEFT JOIN `tbalunovagas` 
            ON `tbestvagas`.`pestvacodig` = `tbalunovagas`.`festvacodig`
        WHERE `testvaexpir` >= CURDATE() AND `tbalunovagas`.`falualcodig` =".$id. "  
        ) GROUP BY pestvacodig
        ORDER BY `pestvacodig` DESC ";
        


    $db->totalvagas = "SELECT count(*) as qtd_vagas FROM tbestvagas WHERE testvaexpir >= CURDATE() AND vaga_visivel = 1";

    $_SESSION['ESTUDANTE']['listarVagas'] = $db->query("$db->listarVagas")->fetchAll();
    $_SESSION['ESTUDANTE']['totalvagas'] = $db->query("$db->totalvagas")->fetchAll(); 

  //  echo'<pre>'; var_dump($_SESSION['ESTUDANTE']['listarVagas']); exit();

    echo '<script type="text/javascript">
    window.location = "'. Validacao::getBase() . 'estudante/estudanteVagas/'.'"
    </script>';

  //  @header('location:' . Validacao::getBase() . 'estudante/estudanteVagas/');

}