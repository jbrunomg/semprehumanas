<?php
require_once '../loader.php';
@session_start();
require_once '../database/DB.php';
$db = new DB();
if (isset($_SESSION['ESTUDANTE']['ID'])) {

    $id = $_SESSION['ESTUDANTE']['ID'];
    $rua    = str_replace(',','',str_replace(' ', '+', strtolower($_POST['estudante_saluallogra'])));
    $bairro = str_replace(' ', '+', strtolower(($_POST['estudante_salualbairr'])));
    $endereco = $rua.','.$bairro.','.'pernambuco,brasil';

    $logLat = geolocationEstudante($endereco);	

    if(!$logLat){
        $logLat['latitude']   = ''; 
        $logLat['longitude']  = '';
    }	

    //Trata aqui algum dado do array de dados para que vá para o array dadosIserir corretamente
    $arquivo = false;

    if ($_FILES['arquivo']['name'] != '') {

        $filepath = $_FILES['arquivo']['tmp_name'];
        $fileSize = filesize($filepath);
        $fileinfo = finfo_open(FILEINFO_MIME_TYPE);
        $filetype = finfo_file($fileinfo, $filepath);

        $uploadOk = 1;

        // Check file size
        if ($_FILES["arquivo"]["size"][0] > 500000) {
            $uploadOk = 0;
            echo '<script type="text/javascript">
            window.location = "'. Validacao::getBase() . 'estudante/estudantePerfil/?erro'.'"
             </script>';
        }

        $imageFileType = strtolower(pathinfo(basename($_FILES["arquivo"]["name"]), PATHINFO_EXTENSION));

        // Allow certain file formats
        if($imageFileType != "doc" and $imageFileType != "pdf") {
           $uploadOk = 0;
           echo '<script type="text/javascript">
            window.location = "'. Validacao::getBase() . 'estudante/estudantePerfil/?erro'.'"
             </script>';
        }

        $Nomeimagem = date('YmdHis').'.'.$imageFileType;
        $tamanho = $_FILES["arquivo"]["size"];
        $tipo    = $imageFileType;
        $data    = date('Y-m-d H:i:s');

        $diretorio = '../estudantes/assets/files/curriculos/'; //define o caminho do diretório
        $target_file = $diretorio . $Nomeimagem;

        // echo "<pre>";
        // print_r($target_file);
        // echo "</pre>"; exit();

        if ($uploadOk != 0) {
            if (move_uploaded_file($_FILES["arquivo"]["tmp_name"], $target_file)) {
                $arquivo = 'estudantes/assets/files/curriculos/'. $Nomeimagem;
            } else {
                echo '<script type="text/javascript">
                window.location = "'. Validacao::getBase() . 'estudante/estudantePerfil/?erro'.'"
                </script>';
            }
        }
    }

    $talualniver = empty($nome) ? '0000-00-00' : date('Y-m-d',strtotime(str_replace('/', '-',$_POST['estudante_talualniver'])));
    
    $dadosAlterar = array(

        'salualnome'        => $_POST['estudante_nome'],
        'salualcpf'         => preg_replace('/[^0-9]/', '', $_POST['estudante_cpf']),
        'salualrg'          => $_POST['estudante_rg'],							    
        'salualrg_org_expd' => $_POST['estudante_rg_org_expd'],
        'talualniver'       => $talualniver,
        'ealualsexo'        => $_POST['estudante_ealualsexo'],
        'ialualcurso'       => $_POST['estudante_ialualcurso'],
        'ealualhabil'       => $_POST['estudante_ealualhabil'],
        'salualestci'       => $_POST['estudante_salualestci'], 
        'salualnatur'       => $_POST['estudante_salualnatur'],

        // Filiação
        'salualpai'        => $_POST['estudante_salualpai'],
        'salualpprof'      => $_POST['estudante_salualpprof'],
        'salualmae'        => $_POST['estudante_salualmae'],
        'salualmprof'      => $_POST['estudante_salualmprof'],

        // Logradouro 
        'saluallogra'      => $_POST['estudante_saluallogra'],
        'salualnumer'      => $_POST['estudante_salualnumer'],
        'salualcompl'      => $_POST['estudante_salualcompl'],
        'salualcidad'      => $_POST['estudante_salualcidad'],
        'salualbairr'      => str_replace(' ', '+', strtolower($_POST['estudante_salualbairr'])),
        'salualestad'      => $_POST['estudante_salualestad'],
        'salualcep'        => $_POST['estudante_salualcep'],
        
        // Contato
        'salualtel01'      => $_POST['estudante_salualtel01'],
        'salualtel02'      => $_POST['estudante_salualtel02'],
        'salualtel03'      => $_POST['estudante_salualtel03'],
        'salualemail'      => $_POST['estudante_salualemail'],

        // Acadêmico
        'ialualcurso'       => $_POST['estudante_ialualcurso'],
        // 'salualmatricula'   => $_POST['estudante_salualmatricula'], Não existe no banco de dados essa coluna
        'ialualcurso_nivel' => $_POST['estudante_ialualcurso_nivel'],							    
        'ialualinsen'       => $_POST['estudante_ialualinsen'],							    
        'salualperio'       => $_POST['estudante_salualperio'],
        'salualturno'       => $_POST['estudante_salualturno'],
        'salualtermino'     => $_POST['estudante_salualtermino'],
        'salualdispo'       => $_POST['estudante_salualdispo'],


        // Informática
        'ealualwindo'      => $_POST['estudante_ealualwindo'] == 'on' ?  '1' : '0',
        'ealualword'       => $_POST['estudante_ealualword']  == 'on' ?  '1' : '0',
        'ealualexel'       => $_POST['estudante_ealualexel']  == 'on' ?  '1' : '0',
        'ealualpower'      => $_POST['estudante_ealualpower']  == 'on' ?  '1' : '0',
        'ealuallinux'      => $_POST['estudante_ealuallinux']  == 'on' ?  '1' : '0',
        'ealualinter'      => $_POST['estudante_ealualinter']  == 'on' ?  '1' : '0',
        'ealualingle'      => $_POST['estudante_ealualingle']  == 'on' ?  '1' : '0',
        'ealualespan'      => $_POST['estudante_ealualespan']  == 'on' ?  '1' : '0',
        'ealualfranc'      => $_POST['estudante_ealualfranc']  == 'on' ?  '1' : '0',
        // 'ealualalema'      => $_POST['estudante_ealualalema']  == 'on' ?  '1' : '0', Não existe no banco de dados essa coluna
        'salualoutro'      => $_POST['estudante_salualoutro']  == 'on' ?  '1' : '0',

        // Outros Dados

        'saluallogin'      => preg_replace('/[^0-9]/', '', $_POST['estudante_cpf']),
        'salualsenha'      => md5(preg_replace('/[^0-9]/', '', $_POST['estudante_cpf'])),
        'salualobser'      => $_POST['estudante_salualobser'],
        'salualexper'      => $_POST['estudante_salualexper'],
        'date_operacao'    => date('Y-m-d H:i:s'), // Data sempre quando Atualizar cadastro	
        'latitude'         => $logLat['latitude'],
        'longitude'        => $logLat['longitude'],
        'estudante_visivel'   => 1
    );

    if($arquivo){

        $dadosArquivos = array(
            'url'         	   => $arquivo,
            'data_anexo'  	   => $data,
            'tamanho'     	   => $tamanho,
            'tipo'        	   => $tipo,
        );

        $dadosAlterar = array_merge($dadosAlterar, $dadosArquivos);

    }

    $set = [];
    foreach($dadosAlterar as $k => $v) {
      $set[] = "$k='$v'";
    }

    $db->str = "UPDATE tbalualunos SET ".implode(', ', $set)." WHERE palualcodig = '$id' ";
    $db->query("$db->str")->fetchAll();

    if ($db->link->affected_rows > 0) {
        echo '<script type="text/javascript">
        window.location = "'. Validacao::getBase() . 'estudante/home/?success'.'"
         </script>';
        //@header('location:' . Validacao::getBase() . 'estudante/home/?success');
    } else {
        echo '<script type="text/javascript">
        window.location = "'. Validacao::getBase() . 'estudante/estudantePerfil/?erro'.'"
         </script>';
       // @header('location:' . Validacao::getBase() . 'estudante/estudantePerfil/?erro');
    }
    

}