<?php
//die('AQUI');
require_once './loader.php'; 
@session_start();
if ($_SESSION['LOGADOEMPRESA'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'empresa/empresaLogin/');
    exit;
}
?>
<?php 
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl()."/thumb.php?w=200&src=images/".$sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';
?>

<style>
  h1 {
    font-family: arial, sans-serif;
    font-size: 15pt;

  }

  hr {
    border-color: orangered;
  }

  select.input-lg {
    height: 46px;
    line-height: 20px;
}
.input-lg {
    height: 0px;
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
}
</style>

<body class="js">

	<!-- Preloader -->
	 <!-- <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div> -->
	<!-- End Preloader -->
	
<?php require_once './menu.php'; ?>
      
<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($sobre->modulo3_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($sobre->modulo3_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
					<ul>
						<li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
						<li class="active"><a href="empresas/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->   
    
<section id="estudante" class="features section" style="padding-top: 70px;">

	<form method="post" action="empresa/perfilCadastro/">
		<div class="container">
			<H1>Edição de Empresa</H1>
			<hr>
			<div class="container">

				<?php if (isset($_GET['erro'])): ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h6><i class="icon fa fa-check">  Aconteceu um erro contate o Suporte!</i></h6>                 
					</div>
				<?php endif; ?>

				<br>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputEmail1">CNPJ:</label>
							<input type="text" class="form-control" id="empresa_sempemcnpj" name="empresa_sempemcnpj" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemcnpj ?>" placeholder="">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputPassword1">Razão Social:</label>
							<input type="text" class="form-control" id="empresa_sempemrazao" name="empresa_sempemrazao" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemrazao ?>" placeholder="">
						</div>
					</div>
				</div>
			</div>

			<br>

			<h1>Dados Logradouro</h1>

			<hr>

			<div class="container">
				<div class="row">
					<div class="col-lg-4">
					
						<div class="form-group">
						<label for="exampleInputEmail1">CEP:</label>
						<input type="text" class="form-control" id="empresa_sempemcep" name="empresa_sempemcep" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemcep ?>" placeholder="">
						</div>
					
					</div>
					<div class="col-lg-4">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Endereço:</label>
						<input type="text" class="form-control" id="empresa_sempemlogra" name="empresa_sempemlogra" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemlogra ?>" placeholder="">
						</div>
					
					</div>
					<div class="col-lg-4">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Nº:</label>
						<input type="text" class="form-control" id="empresa_sempemnumer" name="empresa_sempemnumer" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemnumer ?>" placeholder="">
						</div>
					
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-lg-6">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Complemento:</label>
						<input type="text" class="form-control" id="empresa_sempemcompl" name="empresa_sempemcompl" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemcompl ?>" placeholder="">
						</div>
						<div class="form-group">
						<label for="exampleInputEmail1">UF:</label>
						<input type="text" class="form-control" id="empresa_sempemuf" name="empresa_sempemuf" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemuf ?>" placeholder="">
						</div>
					
					</div>
					<div class="col-lg-6">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Bairro:</label>
						<input type="text" class="form-control" id="empresa_sempembairr" name="empresa_sempembairr" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempembairr ?>" placeholder="">
						</div>
						<div class="form-group">
						<label for="exampleInputEmail1">Cidade:</label>
						<input type="text" class="form-control" id="empresa_sempemcidad" name="empresa_sempemcidad" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemcidad ?>" placeholder="">
						</div>
					
					</div>
				</div>
			</div>

			<br>
			<h1>Dados Contato</h1>
			<hr>
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputEmail1">Telefone:</label>
							<input type="text" class="form-control" id="empresa_sempemtel01" name="empresa_sempemtel01" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemtel01 ?>" placeholder="">
							</div>
							<div class="form-group">
							<label for="exampleInputPassword1">Celular:</label>
							<input type="text" class="form-control" id="empresa_sempemtel02" name="empresa_sempemtel02" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemtel02 ?>" placeholder="">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputPassword1">E-Mail:</label>
							<input type="email" class="form-control" id="empresa_sempememail" name="empresa_sempememail" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempememail ?>" placeholder="">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Celular II:</label>
							<input type="text" class="form-control" id="empresa_sempemtel03" name="empresa_sempemtel03" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemtel03 ?>" placeholder="">
						</div>
					</div>
				</div>
			</div>

			<br>
			<h1>Outros</h1>
			<hr>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Ramo de Atividade:</label>
							<input type="text" class="form-control" id="empresa_sempemativi" name="empresa_sempemativi" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemativi ?>" placeholder="">
						</div>
					</div>
					<div class="col-lg-6">
				    	<div class="form-group">
							<label for="exampleInputPassword1">Responsavel:</label>
							<input type="text" class="form-control" id="empresa_sempemrespo" name="empresa_sempemrespo" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemrespo ?>" placeholder="">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Cargo do Responsável:</label>
							<input type="text" class="form-control" id="empresa_sempemcargo" name="empresa_sempemcargo" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemcargo ?>" placeholder="">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputPassword1">Representante:</label>
							<input type="text" class="form-control" id="empresa_sempemrepre" name="empresa_sempemrepre" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemrepre ?>" placeholder="">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Cargo do Representante:</label>
							<input type="text" class="form-control" id="empresa_sempemrecar" name="empresa_sempemrecar" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemrecar ?>" placeholder="">
						</div>
					</div>
				</div>
			</div>

			<br>

			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<button type="submit" class="btn btn-warning btn-lg">Salvar</button>
					</div>
				</div>
			</div>
    	</div>
	</form>



</section>


   
         



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>	
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
    <!-- Custom  -->
	<script type="text/javascript" src="js\jquery.maskedinput.min.js"></script>

	<script>
	$('li#services').addClass('current');
	</script>
	<script>

	$(function ($) {
		  $("#empresa_sempemtel01").mask("(99) 9999-9999?9");
		  $("#empresa_sempemtel02").mask("(99) 9999-9999?9");
		  $("#empresa_sempemtel03").mask("(99) 9999-9999?9");
		  $("#empresa_sempemcep").mask("99999-999");
		  $("#empresa_sempemcnpj").mask("99.999.999/9999-99");
	});


	$('#empresa_sempemcep').blur(function(){
 
		var cep = $('#empresa_sempemcep').val();
		cep = cep.replace('-','');

		if($.trim(cep) != ""){


		  $.getScript("https://www.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
			// o getScript dá um eval no script, então é só ler!
			//Se o resultado for igual a 1 
			  
			  if(resultadoCEP["resultado"]){
				  
				  if (resultadoCEP['resultado'] == '1' || resultadoCEP['resultado'] == '2'){
					
					  $("#empresa_sempemlogra").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
					  $("#empresa_sempembairr").val(unescape(resultadoCEP["bairro"]));
					  $("#empresa_sempemestad").val(unescape(resultadoCEP["uf"]));
					  $("#empresa_sempemcidad").val(unescape(resultadoCEP["cidade"]));

				  }else{ 

					  $("#empresa_sempemlogra").val('');
					  $("#empresa_sempembairr").val('');
					  $("#empresa_sempemestad").val('');
					  $("#empresa_sempemcidad").val('');

				  }
				
			  }
						  
		  });
		  
		}
	
	});

	
		

</script>
</body>
</html>          