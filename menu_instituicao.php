<?php
require_once './loader.php';

@session_start();
if ($_SESSION['LOGADOINSTITUICAO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'inst/instituicaoLogin/');
    exit;
}
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl()."/thumb.php?w=200&src=images/".$sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';
?>
<body class="js">

	<!-- Preloader -->
	 <!-- <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div> -->
	<!-- End Preloader -->
	
<?php require_once './menu.php'; ?>
      
<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($sobre->modulo3_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($sobre->modulo3_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
					<ul>
						<li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
						<li class="active"><a href="instituicoes/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->   
    
	<section id="estudante" class="features section" style="padding-top: 70px;">


        <div class="container">
            <div class="row">
            	<a href="#">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                    <div class="section-title">
                        <h2>Nossos serviços, facilite seu dia a dia</h2>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p> -->
                    </div>
                </div>
                </a>
            </div>

			<?php if (isset($_GET['success'])): ?>
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<h6><i class="icon fa fa-check"> Alteração realizado com sucesso!</i></h6>                 
				</div>
			<?php endif; ?>
			
            <div class="row">
            	
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
					<a href="inst/carregarVagas/">
                    <div class="single-features"> 
                        <i class="fa fa-calculator"></i>
						<h4>Vagas</h4>
						<p>Existem <strong><?php echo $_SESSION['INSTITUICAO']['totalvagas'][0]->qtd_vagas ?> </strong> disponíveis! Entre em contato com a nessa equipe.</p>
                    </div>
					</a>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
					<a href="inst/carregarPerfil/">
                    <div class="single-features">
                        <i class="fa fa-user"></i>
						<h4>Perfil</h4>
						<p>Lembre-se de manter seu cadastro atualizado!</p>
                    </div>
					</a>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
					<a href="inst/proposta/">
                    <div class="single-features">
                        <i class="fa fa-file-word-o"></i>
						<h4>Proposta Trabalho</h4>
						<p>Nossa proposta que temos a oferecer!</p>
                    </div>
					</a>
                </div>
            
		 
            </div>
        </div>
        
    </section>   
         



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>	
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
      <!-- Custom  -->
    
  <script>
   $('li#services').addClass('current');
  </script> 
</body>
</html>          