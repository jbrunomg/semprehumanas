<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once "includes/vendor/autoload.php";



global $mail;

$mail = new PHPMailer(true);

try{

	$mail->isSMTP();
	$mail->SMTPAuth = true;
	$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
	$mail->WordWrap = 80;
	$mail->IsHTML(true);

}catch(Exception $e){
	throw new Exception('Error');
}


