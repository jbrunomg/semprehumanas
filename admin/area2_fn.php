<?php
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}


function incluir() {
    $area2_nome = $_POST['area2_nome'];
    $cad = new Area2();
    $cad->area2_nome = addslashes($area2_nome);
    $cad->incluir();
    Filter:: redirect("categoria/video/?success");
}

function atualizar() {
    $area2_id = intval($_POST['area2_id']);
    $area2_nome = addslashes($_POST['area2_nome']);
    $a = new Area2();
    $a->area2_nome = $area2_nome;
    $a->area2_id = $area2_id;
    $a->atualizar();
    Filter :: redirect("categoria/video/?success");
}

function remover() {
    if (isset($_REQUEST['id'])) {
        $id = intval($_REQUEST['id']);
        $remover = new Area2();
        $remover->area2_id = $id;
        $remover->deletar();
        Filter :: redirect("categoria/video/?success");
    }
}

function Json() {
    if (isset($_REQUEST['area2_id'])) {
        $area2_id = intval($_REQUEST['area2_id']);
        $j = new Area2();
        $j->area2_id = $area2_id;
        echo $j->getJason();
    }
}

function posicao() {
    $posicao = new area2();
    $posicao->updatePos();
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}

