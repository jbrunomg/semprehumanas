<?php
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}

function atualizar() {
    $a = new Modulo14();
    $a->modulo14_icon1 = addslashes($_POST['modulo14_icon1']);
    $a->modulo14_text1 = addslashes($_POST['modulo14_text1']);
    $a->modulo14_descricao1 = addslashes($_POST['modulo14_descricao1']);
    $a->modulo14_icon2 = addslashes($_POST['modulo14_icon2']);
    $a->modulo14_text2 = addslashes($_POST['modulo14_text2']);
    $a->modulo14_descricao2 = addslashes($_POST['modulo14_descricao2']);
    $a->modulo14_icon3 = addslashes($_POST['modulo14_icon3']);
    $a->modulo14_text3 = addslashes($_POST['modulo14_text3']);
    $a->modulo14_descricao3 = addslashes($_POST['modulo14_descricao3']);
    $a->modulo14_icon4 = addslashes($_POST['modulo14_icon4']);
    $a->modulo14_text4 = addslashes($_POST['modulo14_text4']);
    $a->modulo14_descricao4 = addslashes($_POST['modulo14_descricao4']);
    $a->modulo14_status = $_POST['modulo14_status'];
    $a->modulo14_id = $_POST['modulo14_id'];
    if (isset($_FILES['modulo14_imagem']['name']) && !empty($_FILES['modulo14_imagem']['name'])) {
        $a->removerArquivo();
        $a->enviar();
    }
    $a->atualizar();
    Filter :: redirect("frontend/?success");
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}

