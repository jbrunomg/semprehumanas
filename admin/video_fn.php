<?php
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}

function incluir() {
    $video_nome = addslashes($_POST['video_nome']);
    $video_area2 = $_POST['video_area2'];
    $video_url = $_POST['video_url'];
    $video_imagem = "https://i.ytimg.com/vi/". $video_url."/hqdefault.jpg?custom=true&w=400&h=250&jpg";

    $video = new Video();
    $video->video_nome = $video_nome;
    $video->video_area2 = $video_area2;
    $video->video_url = $video_url;
    $video->video_imagem = $video_imagem;
      
    $video->incluir();

    $id = $video->db->getId();
    Filter :: redirect("video/?success");
}

function atualizar() {
    $a = new Video();
    $a->video_nome = addslashes($_POST['video_nome']);
    $a->video_area2 = $_POST['video_area2'];
    $a->video_url = $_POST['video_url'];
    $a->video_id = $_POST['video_id'];
    $a->video_imagem = "https://i.ytimg.com/vi/". $_POST['video_url']."/hqdefault.jpg?custom=true&w=400&h=250&jpg";
    
    $a->atualizar();
    Filter :: redirect("video/?success");
}

function remover() {
    if (isset($_REQUEST['id'])) {
        $id = intval($_REQUEST['id']);
        $r = new Video();
        $r->video_id = $id;
        $r->remover();
        Filter :: redirect("video/?success");
    }
}

function Json() {
    if (isset($_REQUEST['video_id'])) {
        $video_id = intval($_REQUEST['video_id']);
        $j = new Video();
        $j->video_id = $video_id;
        echo $j->JSON();
    }
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}