<?php
/*
 * @author phpstaff.com.br
 */
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}

function atualizar() {
    $a = new Modulo15();
    $a->modulo15_nome = addslashes($_POST['modulo15_nome']);
    $a->modulo15_subtitulo = addslashes($_POST['modulo15_subtitulo']);
    $a->modulo15_button = addslashes($_POST['modulo15_button']);
    $a->modulo15_status = intval($_POST['modulo15_status']);
    $a->modulo15_envios = intval($_POST['modulo15_envios']);
    $a->modulo15_id = intval($_POST['modulo15_id']);
    if (isset($_FILES['modulo15_imagem']['name']) && !empty($_FILES['modulo15_imagem']['name'])) {
        $a->removerArquivo();
        $a->enviar();
    }
    $a->atualizar();
    Filter :: redirect("frontend/?success");
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}

