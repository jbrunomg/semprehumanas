<?php

if(isset($_SESSION['tinymce_toggle_view'])){
	$view = $_SESSION['tinymce_toggle_view'];
}else{
	$view = 'grid';
}

$current_folder_content = scandirSorted($current_folder);

if(count($current_folder_content) > 0 AND CanAcessLibrary()){
	$html = '';
	foreach($current_folder_content as $c){
		if($view == 'list'){
			if($c['is_file'] == false){
			 if ($c['name'] == "portfolio" || $c['name'] == "blog" || $c['name'] == "partner" || $c['name'] == "slider" || $c['name'] == "team" || $c['name'] == "usuario" || $c['name'] == "video") {
			     $botoes = '';
			 } else {
			     $botoes = '<a href="" class="transparent change-folder" title="Alterar Nome" rel="'. $c['name'] .'"><i class="icon-pencil"></i></a>&nbsp;&nbsp;
					<a href="" class="transparent delete-folder" rel="'. urlencode($c['path']).'" title="Apagar"><i class="icon-trash"></i></a>';
			 }
				$html .= '<tr>
				<td>
				<i class="icon-folder-open"></i>&nbsp;
				<a class="lib-folder" href="" rel="'. urlencode($c['path']).'" title="'. $c['name'] .'">
					'. TrimText($c['name'], 50) .'
				</a>
				</td>
				<td width="20%">
				'. $c['i'] .' Items
				</td>
				<td width="10%">
					'.$botoes.'
				</td>
				</tr>';
			}else{
				$html .= '<tr>
				<td>
				<i class="icon-picture"></i>&nbsp;
				<a href="" class="img-thumbs" rel="' .$c['path'] . '" title="'. $c['name'] .'">
					'. TrimText($c['name'], 50) .'
				</a>
				</td>
				<td width="20%">
				'. formatSizeUnits($c['s']) .'
				</td>
				<td width="10%">
					<a href="" class="transparent change-file" title="Alterar Nome" rel="'. $c['name'] .'"><i class="icon-pencil"></i></a>&nbsp;&nbsp;
					<a href="" class="transparent delete-file" rel="'. urlencode($c['p']).'" title="Apagar"><i class="icon-trash"></i></a>
				</td>
				</tr>';
			}
		
		}else{
			if($c['is_file'] == false){
			 if ($c['name'] == "portfolio" || $c['name'] == "blog" || $c['name'] == "partner" || $c['name'] == "slider" || $c['name'] == "team" || $c['name'] == "usuario" || $c['name'] == "video") {
			     $botoes = '<span style="font-size: 12px">'.TrimText($c['name'], 14).'</span>';
			 } else {
			     $botoes = '
                 <a href="" class="pull-left transparent change-folder" title="Alterar Nome" rel="'. $c['name'] .'"><i class="icon-pencil"></i></a>
                 &nbsp;<span style="font-size: 12px">'.TrimText($c['name'], 14).'</span>
                 <a href="" class="pull-right transparent delete-folder" rel="'. urlencode($c['path']).'" title="Apagar"><i class="icon-trash"></i></a>';
			 }
				$html .= '<div class="item" style="width: 142px">
			<a class="lib-folder" href="" rel="'. urlencode($c['path']).'" title="'. $c['name'] .'">
			<img src="bootstrap/img/130x90.png" class="img-polaroid" width="130" height="90">
			</a>
			<div>
			'.$botoes.'
			<div class="clearfix"></div>
			</div>
			</div>';
			}else{
				$html .= '<div class="item"  style="width: 142px">
			<a href="" class="img-thumbs" rel="' .$c['path'] . '" title="'. $c['name'] .'">
			<img src="timthumb.php?src=' . $c['path'] . '&w=130&h=90" class="img-polaroid" width="130" height="90">
			</a>
			<div>
			<a href="" class="pull-left transparent change-file" title="Alterar Nome" rel="'. $c['name'] .'"><i class="icon-pencil"></i></a>
            &nbsp;<span style="font-size: 12px">'.TrimText($c['name'], 14).'</span>
			<a href="" class="pull-right transparent delete-file" rel="'. urlencode($c['p']).'" title="Apagar"><i class="icon-trash"></i></a>
			<div class="clearfix"></div>
			</div><br /><br />
			</div>';
			}
		}
	}
	if($html != ''){
		if($view == 'list'){
			$html = '<br/><table class="table">' . $html . '</table>';
		}
		
		$output["html"] = $html;
	}else{
		$output["html"] = '<center>Nenhuma imagem na pasta.</center>';
	}
}else{
	$output["html"] = '<center>Nenhuma imagem na pasta.</center>';
}
