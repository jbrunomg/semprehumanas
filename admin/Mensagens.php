<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once 'Controle.php';
require_once "../model/Smtpr.php";


class Mensagens extends Controle {



    public $db;

    public $mensagem_id;

    public $mensagem_nome;

    public $mensagem_texto;

    public $mensagem_data;



    public function __construct() {

        parent::__construct();

        $this->data = date('Y-m-d');

        $this->data_envio = date('Y-m-d H:i:s');

    }

    

    public function incluir() {

        $this->insert("mensagens", "mensagem_nome, mensagem_texto, mensagem_data", "'$this->mensagem_nome','$this->mensagem_texto','$this->data'");

    }

    

    public function atualizar() {

        $query = "mensagem_nome  = '$this->mensagem_nome', mensagem_texto = '$this->mensagem_texto', mensagem_data = '$this->mensagem_data'";

      

        $this->update("mensagens", "$query", "mensagem_id = '$this->mensagem_id'");

    }



    public function getMensagem() {

        $this->select("mensagens", "", "*", "", " WHERE mensagem_id = $this->mensagem_id", "");

    }



    public function getMensagens() {

        $this->select("mensagens", "", "*", "", "order by mensagem_data DESC", "");

    }

    

    public function Enviar() {

        $query = "mensagem_envio = '$this->data_envio'";

        $this->update("mensagens", "$query", "mensagem_id = '$this->mensagem_id'");

        

        global $mail;

        global $msg;

        global $cadastros;

        global $smtp;

        global $envios;

        global $qtd;

        

        $msg = new Mensagens();

        $msg->mensagem_id = $this->mensagem_id;

        $msg->getMensagem();

        

        $envios = new Modulo15();

        $envios->getModulo15();

        $qtd = 3600/$envios->modulo15_envios;

        

        $cadastros = new Cadastros();

        $cadastros->getCadastros();

        

        

        require_once '../includes/vendor/autoload.php';

        if (isset($cadastros->db->data [0])):           

            foreach ($cadastros->db->data as $cad):

                $mail = new PHPMailer(true);
                $smtp = new Smtpr();

                $smtp->getSmtp();

                try{
                    $mail->isSMTP();                                          
                    $mail->SMTPAuth   = true;                               
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
                    $mail->Port = $smtp->smtp_port;
                    $mail->Host = $smtp->smtp_host;

                    $mail->Username = $smtp->smtp_username;
                    $mail->Password = $smtp->smtp_password;
                    $mail->setFrom($smtp->smtp_username, $smtp->smtp_fromname);
                    $mail->Subject = utf8_decode($msg->mensagem_nome);
                    $mail->setFrom($smtp->smtp_username, $smtp->smtp_fromname);

                    $html = utf8_decode($msg->mensagem_texto);

                    if($smtp->smtp_bcc != "" || $smtp->smtp_bcc != null)
                        $mail->addBCC($smtp->smtp_bcc);

                    $mail->addAddress($cad->cadastro_email, utf8_decode($cad->cadastro_nome));
                    $mail->addReplyTo($smtp->smtp_username,utf8_decode($smtp->smtp_fromname));

                    $mail->isHTML(true);   

                    $mail->Body = $html;

                    $mail->Send();

                    sleep($qtd);

                } catch(Exception $e){

                }


                $mail->clearAddresses();
                $mail->clearAttachments();
         endforeach;
        endif; 
           
    }

    public function remover() {

        $this->delete("mensagens", "mensagem_id = $this->mensagem_id");

    }





    public function Contar($id) {

        $this->getCount("mensagens", "");

    }

    

    public function JSON() {

        $this->getJSON("mensagens", "mensagem_id = '$this->mensagem_id'");

    }



}