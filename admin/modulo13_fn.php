<?php
/*
 * @author phpstaff.com.br
 */
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}

function atualizar() {
    $a = new Modulo13();
    $a->modulo13_nome = addslashes($_POST['modulo13_nome']);
    $a->modulo13_descricao = addslashes($_POST['modulo13_descricao']);
    $a->modulo13_status = intval($_POST['modulo13_status']);
    $a->modulo13_id = intval($_POST['modulo13_id']);
    $a->atualizar();
    Filter :: redirect("frontend/?success");
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}

