<?php
require_once '../loader.php';
@session_start();
if (!isset($_SESSION['LOGADO']) || $_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}
$site = new Site();
$site->getMeta();

$area3 = new Area3();
$area3->db = new DB;
$area3->getCatSub();

$paginas_id = intval($_GET['id']);
$pages = new Paginas();
$pages->paginas_id = $paginas_id;
$pages->GetMyPaginas();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- START @HEAD -->
    <head>
        <?php require_once './base.php'; ?>
        <!-- START @META SECTION -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title><?= $site->site_meta_titulo ?></title>
        <!--/ END META SECTION -->

        <!-- START @FAVICONS -->
        <link href="./assets/img/ico/<?= $modulo_aparencia->modulo_aparencia_favicon ?>" rel="shortcut icon" sizes="144x144">
        <!--/ END FAVICONS -->

        <!-- START @FONT STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <!--/ END FONT STYLES -->

        <!-- START @GLOBAL MANDATORY STYLES -->
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- START @PAGE LEVEL STYLES -->
        <link href="./assets/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="./assets/css/animate.min.css" rel="stylesheet">
        <link href="./assets/css/bootstrap-tagsinput.css" rel="stylesheet">
        <link href="./assets/css/jasny-bootstrap-fileinput.min.css" rel="stylesheet">
        <link href="./assets/css/chosen.min.css" rel="stylesheet">
        <!--/ END PAGE LEVEL STYLES -->

        <!-- START @THEME STYLES -->
        <link href="./assets/css/reset.css" rel="stylesheet">
        <link href="./assets/css/layout.css" rel="stylesheet">
        <link href="./assets/css/components.css" rel="stylesheet">
        <link href="./assets/css/plugins.css" rel="stylesheet">
        <link href="./assets/css/themes/default.theme.css" rel="stylesheet" id="theme">
        <link href="./assets/css/custom.css" rel="stylesheet">
        <link href="./assets/css/summernote.css" rel="stylesheet">
        <link href="./assets/css/datepicker.css" rel="stylesheet">
        <link href="./assets/css/dropzone.css" rel="stylesheet">
        <link href="./assets/css/jquery-ui.min.css" rel="stylesheet">
        <!--/ END THEME STYLES -->

        <!-- START @IE SUPPORT -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="./assets/js/html5shiv.min.js"></script>
        <script src="./assets/js/respond.min.js"></script>
        <![endif]-->
        <!--/ END IE SUPPORT -->
    </head>
    <!--/ END HEAD -->

    <body>
<script type="text/javascript" src="assets/tiny_mce/tinymce.js" id="richedit"></script>
<script type="text/javascript">
 tinymce.init({
  selector: "textarea",
  language: "pt_BR",
  selector: 'textarea',
  browser_spellcheck: true,
  contextmenu: false,
  convert_urls:false,
  height: 600,
  theme: 'modern',
  
  images_upload_base_path: '<?= str_replace("/admin","",Validacao::getBaseUrl()); ?>/images/paginas/',
  plugins: [
    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak imagetools",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
  ],

  toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
  toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
  toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | visualchars visualblocks nonbreaking template pagebreak restoredraft",

  menubar: false,
  toolbar_items_size: 'small',

  style_formats: [{
    title: 'Texto negrito',
    inline: 'b'
  }, {
    title: 'Texto vermelho',
    inline: 'span',
    styles: {
      color: '#ff0000'
    }
  }, {
    title: 'Cabeçalho vernelho',
    block: 'h1',
    styles: {
      color: '#ff0000'
    }
  }, {
    title: 'Examplo 1',
    inline: 'span',
    classes: 'example1'
  }, {
    title: 'Examplo 2',
    inline: 'span',
    classes: 'example2'
  }, {
    title: 'Estilo de tabelas'
  }, {
    title: 'Tabela linha 1',
    selector: 'tr',
    classes: 'tablerow1'
  }],

  templates: [{
    title: 'Testar template 1',
    content: 'Teste 1'
  }, {
    title: 'Testar template 2',
    content: 'Teste 2'
  }],
  content_css: [
   
    '//www.tinymce.com/css/codepen.min.css'
  ]
});
</script>
        <!--[if lt IE 9]>
        <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @WRAPPER -->
        <section id="wrapper" class="page-sound">
            <!-- START @HEADER -->
            <?php require_once './navegacao.php'; ?>
            <!--/ END HEADER -->



            <!-- /#sidebar-left -->
            <?php require_once './menu.php'; ?>
            <!--/ END SIDEBAR LEFT -->

            <!-- START @PAGE CONTENT -->
            <section id="page-content">

                <!-- Start page header -->
                <div class="header-content">
                    <h2><i class="fa fa-file-text-o"></i>Páginas</h2>
                    <div class="breadcrumb-wrapper hidden-xs">
                        <span class="label">Você está em :</span>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="home/">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Páginas</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                        </ol>
                    </div><!-- /.breadcrumb-wrapper -->
                </div><!-- /.header-content -->
                <!--/ End page header -->

                <!-- Start body content -->
                <div class="body-content animated fadeIn">

                    <div class="row">
                        <div class="col-md-12">

                            <!-- Start input fields - basic form -->
                            <div class="panel rounded shadow">
                                <div class="panel-sub-heading">
                                    <div class="callout callout-info" style="padding-top: 19px;"><p><strong>Editar</strong></p></div>
                                </div><!-- /.panel-subheading -->
                                <div class="clearfix"></div>
                                <div class="panel-body no-padding">
                                    <form  enctype="multipart/form-data" method="post" action="paginas_fn.php?acao=atualizar">
                                        <div class="form-body">
                                            <div class="form-group ">
                                                <label for="paginas_area3" class="control-label">Categoria</label>
                                                <select data-placeholder="Obrigatório selecionar a área" id="paginas_area3" name="paginas_area3" class="form-control input-sm mb-15" required>
                                                    <option value="">Selecione a Área</option>
                                                    <?php if (isset($area3->db->data[0])): ?>
                                                        <?php foreach ($area3->db->data as $cat): ?>
                                                            <option value="<?= $cat->catid ?>"><?php if ($cat->categoria_parent) { echo stripslashes($cat->categoria_parent)." > "; } ?><?= stripslashes($cat->categoria) ?></option>
                                                    <?php endforeach; ?>
                                                    <?php endif; ?>   
                                                </select>
                                            </div><!-- /.form-group -->
                                            

                                            <div class="form-group">
                                                <label class="control-label">Título</label>
                                                <input class="form-control rounded" type="text" id="paginas_nome"  name="paginas_nome" required value="<?= stripslashes($pages->paginas_nome )?>">
                                            </div><!-- /.form-group -->
                                            
                                            
                                            <div class="row">
                                               <div class="form-group">
                                                 <div class="row-fluid">
                                                   <div class="col-md-10 pull-right">
                                                     <label class="control-label">Imagem</label>
                                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                          <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                             <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="paginas_imagem" name="paginas_imagem"></span>
                                                                <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                               </div>
                                                            </div>
                                                           <div class="col-md-2">
                                                               <?php if (!empty($pages->paginas_imagem)): ?>
                                                                   <img src="../thumb.php?w=200&h=140&zc=0&src=images/paginas/<?= $pages->paginas_imagem ?>" class="img-thumbnail" />
                                                               <?php else: ?>
                                                                   <img src="../thumb.php?w=200&h=140&zc=0&src=images/nopic.jpg" class="img-thumbnail" />
                                                               <?php endif; ?>
                                                        </div>
                                                     </div>
                                                   </div>
                                               </div>
                                               
                                             <div style="margin-bottom: 20px;"></div>  

                                            <div class="form-group">
                                                <label class="control-label">Descrição:</label>
                                                <textarea class="form-control" id="paginas_descricao" name="paginas_descricao" rows="5"><?= stripslashes($pages->paginas_descricao) ?></textarea>
                                                <input type="hidden" id="paginas_id" name="paginas_id" value="<?= $pages->paginas_id ?>" >
                                            </div><!-- /.form-group -->

                                            <div class="form-footer">
                                                <div class="text-center">
                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div><!-- /.form-footer -->
                                        </div><!-- /.panel-body -->
                                    </form>
                                    <div class="container">
                                    <div id="uploadFotos">
                                    
                                        <form id="form-dropzone" class="dropzone" action="uploads.php?paginas_id=<?= $paginas_id ?>" enctype="multipart/form-data" method="post">
                                            <div class="fallback">
                                                <div class="dz-preview dz-file-preview">
                                                    <div class="dz-details">
                                                        <div class="dz-filename"><span data-dz-name></span></div>
                                                        <div class="dz-size" data-dz-size></div>
                                                        <img data-dz-thumbnail />
                                                    </div>
                                                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                                                    <div class="dz-success-mark"><span>✔</span></div>
                                                    <div class="dz-error-mark"><span>✘</span></div>
                                                    <div class="dz-error-message"><span data-dz-errormessage></span></div>
                                                </div>
                                            </div>
                                            <div id="msg-drop"></div>
                                        </form>
                                    </div>
                                        <form method="post" id="form-remove" action="paginas_fn.php?acao=removerFoto&paginas_id=<?= $paginas_id ?>">
                                            <?php
                                            $fotos = new Fotos();
                                            $fotos->fotos_paginas = $paginas_id;
                                            $fotos->getFotosAll();
                                            ?>
                                            <?php $flag = ($fotos->db->data >= 1) ? 'show' : 'hide'; ?>

                                            <div id="galeria" class="col-sm-12 col-md-12" style="border:0px solid red;">
                                                <?php if (isset($fotos->db->data[0])) : ?>
                                                    <?php foreach ($fotos->db->data as $f) : ?>
                                                        <div class="col-md-2  col-sm-3 col-xs-6" id="li_<?= $f->fotos_id ?>">
                                                            <input class="hide" type="checkbox" name="fotos[]" value="<?= $f->fotos_id ?>" id="check_<?= $f->fotos_id ?>"  />
                                                            <img src="../thumb.php?w=120&h=100&zc=1&src=images/paginas/<?= $f->fotos_url ?>" class="thumbnail  tip click-remove" id="<?= $f->fotos_id ?>"
                                                                 style="cursor: pointer;width: 150px; height: 100px; margin-top:10px; margin-bottom: 20px; " title="marcar para remover" >
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>

                                            <div class="row col-xs-12 div-btn-remove <?= $flag ?>">
                                                <p class="text-center">
                                                    <button type="button" class="btn btn-danger  btn-sel-tudo">
                                                        <i class="fa fa-check-circle-o"></i>
                                                        Selecionar Tudo
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-sel-nada hide">
                                                        <i class="fa fa-undo"></i>
                                                        Desmarcar Tudo
                                                    </button>
                                                    <button type="button" class="btn btn-danger" id="btn-submit-remove">
                                                        <i class="fa fa-trash-o"></i>
                                                        Remover  Selecionadas
                                                    </button>
                                                </p>
                                            </div>                                       
                                        </form>
                                    </div>

                               
                                </div><!-- /.panel -->
                            </div><!-- /.col-md-6 -->
                        </div><!-- /.row -->
                    </div><!-- /.body-content -->
                    <!--/ End body content -->
            </section><!-- /#page-content -->
        </section><!-- /#wrapper -->
        <!--/ END WRAPPER -->

        <!-- START @BACK TOP -->
        <div id="back-top" class="animated pulse circle">
            <i class="fa fa-angle-up"></i>
        </div><!-- /#back-top -->
        <!--/ END BACK TOP -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- START @CORE PLUGINS -->
        <script src="./assets/js/jquery.min.js"></script>
        <script src="./assets/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="./assets/js/handlebars.js"></script>
        <script src="./assets/js/typeahead.bundle.min.js"></script>
        <script src="./assets/js/jquery.nicescroll.min.js"></script>
        <script src="./assets/js/index.js"></script>
        <script src="./assets/js/jquery.easing.1.3.min.js"></script>
        <script src="./assets/ionsound/ion.sound.min.js"></script>
        <script src="./assets/js/bootbox.js"></script>
        <!--/ END CORE PLUGINS -->

        <!-- START @PAGE LEVEL PLUGINS -->
        <script src="./assets/js/bootstrap-tagsinput.min.js"></script>
        <script src="./assets/js/jasny-bootstrap.fileinput.min.js"></script>
        <script src="./assets/js/holder.js"></script>
        <script src="./assets/js/bootstrap-maxlength.min.js"></script>
        <script src="./assets/js/jquery.autosize.min.js"></script>
        <script src="./assets/js/chosen.jquery.min.js"></script>
        <!--/ END PAGE LEVEL PLUGINS -->

        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="./assets/js/apps.js"></script>
        <script src="./assets/js/dark.form.js"></script>
        <script src="./assets/js/dropzone.js"></script>
        <script src="./assets/js/jquery-ui.min.js"></script>
         <script src="./assets/js/bootstrap-datepicker.js"></script>
        <script src="./assets/js/bootstrap-datepicker.pt-BR.js"></script>

        <!--/ END PAGE LEVEL SCRIPTS -->
        <!--/ END JAVASCRIPT SECTION -->

    </body>
    <script>
        $('.paginas').addClass('active');
        $('#paginas_area3').val('<?= $pages->paginas_area3 ?>');
        $('.paginaseditar').addClass('active');

    </script>
    <script>
        $(function () {
            reloadActions();
            
        });
        function reloadActions() {
            //var sortedIDs = $( ".selector" ).sortable( "toArray" );        
            $("#galeria").sortable({cursor: "move",
                stop: function (event, ui) {
                    var ids = $("#galeria").sortable("toArray");
                    var url = 'fotos_fn.php?acao=posicao';
                    $.post(url, {item: ids}, function (data) {
                        console.log(data);
                    });
                }
            });
        }

        $(function () {
            Dropzone.autoDiscover = false;
            $(".dropzone").dropzone({
                url: "uploads.php?paginas_id=<?= $paginas_id ?>",
                accept: function (file, done) {
                    done();

                },
                maxFilesize: 2000, // MB
                complete: function (file) {
                    //console.log($('#form-dropzone').html())
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                        $('.dz-preview').each(function () {
                            $(this).fadeOut(1000);
                        });

                        $('#msg-drop').html('<center> <h1><i class="fa fa-3x fa-thumbs-up"></i>  <br /> Upload Concluído</h1></center>');
                        setTimeout(function () {
                            $('#msg-drop').html('<center> <h1><i class="fa fa-3x fa-cloud-upload"></i>  <br /> Enviar Imagens</h1></center>');
                            $('.div-btn-remove').removeClass('hide').show();
                            $("#form-dropzone").hide();
                             window.location.reload();
                        }, 100);
                    }
                    var f = $.parseJSON(file.xhr.response);
                    var div = '';
                    div += '    <div class="col-md-2"> ';
                    div += '        <input class="hide" type="checkbox" name="fotos[]" value="' + f.fotos_id + '" id="check_' + f.fotos_id + '"  />';
                    div += '        <img src="../images/paginas/' + f.fotos_url + '" class="thumbnail tip click-remove" id="' + f.fotos_id + '" ';
                    div += '         style="cursor: pointer;width: 150px; height: 100px; margin-top:10px; margin-bottom: 20px; " title="marcar para remover" >';
                    div += '    </div>';
                    $('#galeria').append(div);
                    reloadActions();

                    $('#' + f.fotos_id).on('click', function () {
                        var id = f.fotos_id;
                        $('#check_' + id).trigger('click');
                        $('#' + id).toggleClass('btn-danger');

                        if ($('input:checked').length >= 1) {
                            $('#btn-submit-remove').show();
                        } else {
                            $('#btn-submit-remove').hide();
                        }
                    });

                },
                sending: function (file, xhr, formData) {
                    formData.append("paginas_id", "<?= $paginas_id ?>");
                    if (xhr.readyState === 4) {
                        //console.log(xhr.response)
                    }
                },
                totaluploadprogress: function () {
                    

                }
            });
        });
        $('#btn-submit-remove').hide();

        $('#btn-submit-remove').on('click', function (event) {
            if ($('input:checked').length >= 1) {
               
                $('#form-remove').submit();
                
                
               /* $("form#form-remove").on('submit',function(event){
                    if ($('input:checked').length >= 1) {
               event.preventDefault();
               

             
               
               var formData = new FormData($(this)[0]);
                 $.ajax({
                    url: 'paginas_fn.php?acao=removerFoto&paginas_id=<?= $paginas_id ?>',
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) { 
                  $("#galeria").load(location.href + " #galeria");
                  $("#form-dropzone").fadeOut();
                  $("#form-dropzone").fadeIn();
                  reloadActions();
                 } 
               
                            
              }); 
              }
               return false;
			}); */      
    
            }
        });

        $('.click-remove').on('click', function () {
            var id = $(this).attr('id');
            $('#check_' + id).trigger('click');
            $('#' + id).toggleClass('btn-danger');

            if ($('input:checked').length >= 1) {
                $('#btn-submit-remove').show();
            } else {
                $('#btn-submit-remove').hide();
            }
        });

        $('.btn-sel-tudo').on('click', function () {
            $('.click-remove').each(function () {
                var id = $(this).attr('id');
                $('#' + id).addClass('btn-danger');
                $('#check_' + id).attr('checked', 'checked');
            });
            $('.btn-sel-tudo').hide();
            $('.btn-sel-nada').removeClass('hide').show();

            if ($('input:checked').length >= 1) {
                $('#btn-submit-remove').show();
            } else {
                $('#btn-submit-remove').hide();
            }
        });

        $('.btn-sel-nada').on('click', function () {
            $('.click-remove').each(function () {
                var id = $(this).attr('id');
                $('#' + id).removeClass('btn-danger');
                $('#check_' + id).removeAttr('checked');
            });
            $('.btn-sel-tudo').show();
            $('.btn-sel-nada').hide();

            if ($('input:checked').length >= 1) {
                $('#btn-submit-remove').show();
            } else {
                $('#btn-submit-remove').hide();
            }
        });

    </script>
    <!--/ END BODY -->

</html>