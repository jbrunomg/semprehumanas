<?php
require_once '../loader.php';
@session_start();
if (!isset($_SESSION['LOGADO']) || $_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}
$site = new Site();
$site->getMeta();

$mensagem_id = intval($_GET['id']);
$editar = new Mensagens();
$editar->mensagem_id = $mensagem_id;
$editar->getMensagem();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- START @HEAD -->
    <head>
        <!-- START @META SECTION -->
        <?php require_once './base.php'; ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title><?= $site->site_meta_titulo ?></title>
        <!--/ END META SECTION -->

        <!-- START @FAVICONS -->
        <link href="./assets/img/ico/<?= $modulo_aparencia->modulo_aparencia_favicon ?>" rel="shortcut icon" sizes="144x144">
        <!--/ END FAVICONS -->

        <!-- START @FONT STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'>
        <!--/ END FONT STYLES -->

        <!-- START @GLOBAL MANDATORY STYLES -->
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- START @PAGE LEVEL STYLES -->
        <link href="./assets/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="./assets/css/animate.min.css" rel="stylesheet">
        <link href="./assets/css/bootstrap-tagsinput.css" rel="stylesheet">
        <link href="./assets/css/jasny-bootstrap-fileinput.min.css" rel="stylesheet">
        <link href="./assets/css/chosen.min.css" rel="stylesheet">
        <link href="./assets/css/bootstrap-switch.min.css" rel="stylesheet">
        <!--/ END PAGE LEVEL STYLES -->

        <!-- START @THEME STYLES -->
        <link href="./assets/css/reset.css" rel="stylesheet">
        <link href="./assets/css/layout.css" rel="stylesheet">
        <link href="./assets/css/components.css" rel="stylesheet">
        <link href="./assets/css/plugins.css" rel="stylesheet">
        <link href="./assets/css/themes/default.theme.css" rel="stylesheet" id="theme">
        <link href="./assets/css/custom.css" rel="stylesheet">
        <link href="./assets/css/datepicker.css" rel="stylesheet">
        <link href="./assets/css/social.css" rel="stylesheet">
        <link href="./assets/css/jquery.rtnotify.css" rel="stylesheet">
        <link href="./assets/css/noty_theme_default.css" rel="stylesheet">
  
        <!--/ END THEME STYLES -->

        <!-- START @IE SUPPORT -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="./assets/js/html5shiv.min.js"></script>
        <script src="./assets/js/respond.min.js"></script>
        <![endif]-->
        <!--/ END IE SUPPORT -->
       
    </head>
    <!--/ END HEAD -->

    <body>

        <!--[if lt IE 9]>
        <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
<script type="text/javascript" src="assets/tiny_mce/tinymce.js" id="richedit"></script>
<script type="text/javascript">
 tinymce.init({
  selector: "textarea",
  language: "pt_BR",
  selector: 'textarea',
  browser_spellcheck: true,
  contextmenu: false,
  external_plugins: {"nanospell": "<?= Validacao::getBaseUrl(); ?>/assets/tiny_mce/plugins/nanospell/plugin.js"},
  nanospell_server: "php",
  nanospell_dictionary: "pt_br",
  nanospell_autostart: true,
  nanospell_ignore_words_with_numerals: false,
  nanospell_ignore_block_caps: false,
  nanospell_compact_menu: true,
  height: 380,
  theme: 'modern',
  cleanup : true,
  
  images_upload_base_path: '<?=Validacao::getBase()?>images/newsletter/',
  plugins: [
    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
  ],

  toolbar1: "cut copy paste | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | searchreplace nanospell",
  toolbar2: "bullist numlist | outdent indent blockquote | undo redo | link unlink image code preview | forecolor backcolor | table hr fullscreen | ltr rtl",

  menubar: false,
  toolbar_items_size: 'small',

 
  content_css: [
   
    '//www.tinymce.com/css/codepen.min.css'
  ]
});
</script>

        <section id="wrapper" class="page-sound">
            <?php require_once './navegacao.php'; ?>
            <?php require_once './menu.php'; ?>
            <section id="page-content">
                <div class="header-content">
                    <h2><i class="fa fa-comments"></i>  <span>Editar Mensagem  </span></h2>
                    <div class="breadcrumb-wrapper hidden-xs">
                        <span class="label">Você está em :</span>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="home/">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                             <li>
                                <a href="newsletter/">Gerenciar Mensagens</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Mensagem</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="body-content animated fadeIn">
                     <div class="row">
                        <div class="col-md-12">

                            <!-- Start input fields - basic form -->
                            <div class="panel rounded shadow">
                                <div class="panel-sub-heading">
                                    <div class="callout callout-info" style="padding-top: 19px;"><p><strong>Editar Mensagem</strong></p></div>
                                </div><!-- /.panel-subheading -->
                                <div class="clearfix"></div>
                                <div class="panel-body no-padding">
                                    <form  enctype="multipart/form-data" method="post" action="mensagem_fn.php?acao=atualizar">
                                        <div class="form-body">

                                            <div class="form-group">
                                                <label class="control-label">Título</label>
                                                <input class="form-control rounded" type="text" id="mensagem_nome"  name="mensagem_nome" required value="<?= stripslashes($editar->mensagem_nome) ?>">
                                            </div><!-- /.form-group -->

                                <div class="form-group">
                                <label for="mensagem_texto">Texto:</label>
                                 <textarea class="form-control rounded" id="mensagem_texto" name="mensagem_texto" required><?= stripslashes($editar->mensagem_texto) ?></textarea>
        
                            </div>
                            
                             <div class="form-group">
                                <label for="mensagem_data">Data:</label>
                                <input type="text" class="form-control" id="mensagem_data" name="mensagem_data" value="<?= date('d/m/Y', strtotime($editar->mensagem_data)); ?>" style="text-align: left" required />             
                            </div>
                            
                            <div class="form-group">
                                <label for="mensagem_envio">Envio:</label>
                                <?php if($editar->mensagem_envio != "0000-00-00 00:00:00") {?>
                                <?= date('d/m/Y H:i', strtotime($editar->mensagem_envio)); ?>   
                                <?php } else {?>
                                sem envios ainda  
                                <?php } ?>         
                            </div>
                            <input type="hidden" id="mensagem_id"  name="mensagem_id" value="<?= $editar->mensagem_id ?>">
                                            </div><!-- /.form-group -->

                                            <div class="form-footer">
                                                <div class="pull-right">
                                                    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Atualizar</button>
                                                    <button type="button" class="btn btn-danger" id="btn-cancel" data-url='./newsletter/' style="margin-left: 6px"><i class="fa fa-remove"></i> Cancelar</button>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div><!-- /.form-footer -->
                                        </div><!-- /.panel-body -->
                                    </form>
                                </div><!-- /.panel -->
                            </div><!-- /.col-md-6 -->
                        </div><!-- /.row -->
                    </div><!-- /.body-content -->
                    <!--/ End body content -->
            </section><!-- /#page-content -->
        </section><!-- /#wrapper -->
        <!--/ END WRAPPER -->
        
        
        <!--***************MODAL CANCEL****************-->
        <div class="modal fade" id="MODALCANCEL" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="text-center text-danger">Atenção!</h4>
                        <p class="text-center text-danger">
                            Todas as alterações na mensagem serão perdidas.<br />
                            Deseja realmente executar este procedimento?
                        </p>
                        <p class="text-center">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="btn-confirm-cancel"><i class="fa fa-check"></i> Sim</button>
                            <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-remove"></i> Não</button>

                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!--***************MODAL CANCEL*****************-->


        <!-- START @BACK TOP -->
        <div id="back-top" class="animated pulse circle">
            <i class="fa fa-angle-up"></i>
        </div><!-- /#back-top -->
        <!--/ END BACK TOP -->

        <!-- START @CORE PLUGINS -->
        <script src="./assets/js/jquery.min.js"></script>
        <script src="./assets/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="./assets/js/handlebars.js"></script>
        <script src="./assets/js/typeahead.bundle.min.js"></script>
        <script src="./assets/js/jquery.nicescroll.min.js"></script>
        <script src="./assets/js/index.js"></script>
        <script src="./assets/js/jquery.easing.1.3.min.js"></script>
        <script src="./assets/ionsound/ion.sound.min.js"></script>
        <script src="./assets/js/bootbox.js"></script>
        <!--/ END CORE PLUGINS -->

        <!-- START @PAGE LEVEL PLUGINS -->
        <script src="./assets/js/bootstrap-tagsinput.min.js"></script>
        <script src="./assets/js/jasny-bootstrap.fileinput.min.js"></script>
        <script src="./assets/js/holder.js"></script>
        <script src="./assets/js/bootstrap-maxlength.min.js"></script>
        <script src="./assets/js/jquery.autosize.min.js"></script>
        <script src="./assets/js/chosen.jquery.min.js"></script>
        <script src="./assets/js/jquery.rtnotify.js"></script>
        <!--/ END PAGE LEVEL PLUGINS -->

        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="./assets/js/apps.js"></script>
        <script src="./assets/js/bootstrap-switch.min.js"></script>
        <script src="./assets/js/bootstrap-datepicker.js"></script>
        <script src="./assets/js/bootstrap-datepicker.pt-BR.js"></script>
      
        <script>
            $('.newsletter').addClass('active');

            $(".sound").on("click", function () {
                ion.sound.play("button_push.mp3");
            });
            
            $('#btn-cancel').on('click', function () {
                var url = $(this).attr('data-url');
                $('#MODALCANCEL').modal('show');
                $('#btn-confirm-cancel').on('click', function () {
                    window.location = url;
                });
             });
          
          $('#mensagem_data').datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR"
        });  
        </script>
        
    </body>
</html>