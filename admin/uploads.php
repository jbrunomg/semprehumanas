<?php

require_once '../loader.php';
$fotos = new Fotos();

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $fotos_url = md5(uniqid(time())) . '.jpg';
    if (move_uploaded_file($_FILES['file']['tmp_name'], "../images/paginas/" . $fotos_url)) {
        $fotos_paginas = $_REQUEST['paginas_id'];
        $fotos->fotos_url = $fotos_url;
        $fotos->fotos_paginas = "$fotos_paginas";
        $fotos_id = $fotos->incluir();
        echo json_encode(array('fotos_url' => $fotos_url, 'fotos_id' => $fotos_id));
    }
    exit;
}

