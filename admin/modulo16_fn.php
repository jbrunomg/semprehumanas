<?php
/*
 * @author phpstaff.com.br
 */
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}

function atualizar() {
    $a = new Modulo16();
    $a->modulo16_nome = addslashes($_POST['modulo16_nome']);
    $a->modulo16_userid = addslashes($_POST['modulo16_userid']);
    $a->modulo16_status = intval($_POST['modulo16_status']);
    $a->modulo16_id = intval($_POST['modulo16_id']);
    $a->atualizar();
    Filter :: redirect("frontend/?success");
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}

