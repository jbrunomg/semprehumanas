<?php
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}


function incluir() {
    $area3_nome = $_POST['area3_nome'];
    $area3_parent = intval($_POST['area3_parent']);
    if ($area3_parent == '0') {
    $area3_level = '1';    
    } else {
    $area3_level = '0';     
    }
    $cad = new Area3();
    $cad->area3_nome = addslashes($area3_nome);
    $cad->area3_parent = intval($area3_parent);
    $cad->area3_level = intval($area3_level);
    $cad->incluir();
    Filter:: redirect("categoria/paginas/?success");
}

function atualizar() {
    $area3_id = intval($_POST['area3_id']);
    $area3_nome = addslashes($_POST['area3_nome']);
    $area3_parent = intval($_POST['area3_parent']);
    if ($area3_parent == '0') {
    $area3_level = '1';    
    } else {
    $area3_level = '0';     
    }
    $a = new Area3();
    $a->area3_nome = $area3_nome;
    $a->area3_parent = $area3_parent;
    $a->area3_level = $area3_level;
    $a->area3_id = $area3_id;
    $a->atualizar();
    Filter :: redirect("categoria/paginas/?success");
}

function remover() {
    if (isset($_REQUEST['id'])) {
        $id = intval($_REQUEST['id']);
        $remover = new Area3();
        $remover->area3_id = $id;
        $remover->deletar();
        Filter :: redirect("categoria/paginas/?success");
    }
}

function Json() {
    if (isset($_REQUEST['area3_id'])) {
        $area3_id = intval($_REQUEST['area3_id']);
        $j = new Area3();
        $j->area3_id = $area3_id;
        echo $j->getJason();
    }
}

function posicao() {
    $posicao = new area3();
    $posicao->updatePos();
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}

