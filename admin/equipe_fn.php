<?php
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}

function incluir() {
    $equipe_nome = addslashes($_POST['equipe_nome']);
    $equipe_subtitulo = addslashes($_POST['equipe_subtitulo']);
    $equipe_link1 = addslashes($_POST['equipe_link1']);
    $equipe_link2 = addslashes($_POST['equipe_link2']);
    $equipe_link3 = addslashes($_POST['equipe_link3']);
    $equipe_link4 = addslashes($_POST['equipe_link4']);
    $equipe_descricao = addslashes($_POST['equipe_descricao']);

    $equipe = new Equipe();
    $equipe->equipe_nome = $equipe_nome;
    $equipe->equipe_subtitulo = $equipe_subtitulo;
    $equipe->equipe_link1 = $equipe_link1;
    $equipe->equipe_link2 = $equipe_link2;
    $equipe->equipe_link3 = $equipe_link3;
    $equipe->equipe_link4 = $equipe_link4;
    $equipe->equipe_descricao = $equipe_descricao;
    if (isset($_FILES['equipe_imagem']['name']) && !empty($_FILES['equipe_imagem']['name'])) {
        $equipe->enviar();
    }
    $equipe->incluir();
    Filter :: redirect("equipe/");
}

function atualizar() {
    $atualizar = new Equipe();
    $atualizar->equipe_nome = addslashes($_POST['equipe_nome']);
    $atualizar->equipe_subtitulo = addslashes($_POST['equipe_subtitulo']);
    $atualizar->equipe_link1 = addslashes($_POST['equipe_link1']);
    $atualizar->equipe_link2 = addslashes($_POST['equipe_link2']);
    $atualizar->equipe_link3 = addslashes($_POST['equipe_link3']);
    $atualizar->equipe_link4 = addslashes($_POST['equipe_link4']);
    $atualizar->equipe_descricao = addslashes($_POST['equipe_descricao']);
    $atualizar->equipe_id  =  addslashes($_POST['equipe_id']);
    if (isset($_FILES['equipe_imagem']['name']) && !empty($_FILES['equipe_imagem']['name'])) {
        $atualizar->removerArquivo();
        $atualizar->enviar();
    }
    $atualizar->atualizar();
    Filter :: redirect("equipe/?success");
}

function remover() {
    if (isset($_REQUEST['id'])) {
        $id = intval($_REQUEST['id']);
        $remover = new Equipe();
        $remover->equipe_id = "$id";
        $remover->removerArquivo();
        $remover->remover();
        Filter :: redirect("equipe/?success");
    }
}

function Json() {
    if (isset($_REQUEST['equipe_id'])) {
        $equipe_id = intval($_REQUEST['equipe_id']);
        $j = new Equipe();
        $j->equipe_id =  $equipe_id;
        echo $j->JSON();
    }
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}

