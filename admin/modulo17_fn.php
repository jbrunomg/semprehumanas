<?php
/*
 * @author phpstaff.com.br
 */
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}

function atualizar() {
    $a = new Modulo17();
    $a->modulo17_nome = addslashes($_POST['modulo17_nome']);
    $a->modulo17_subtitulo = addslashes($_POST['modulo17_subtitulo']);
    $a->modulo17_button = addslashes($_POST['modulo17_button']);
    $a->modulo17_status = intval($_POST['modulo17_status']);
    $a->modulo17_paginacao = intval($_POST['modulo17_paginacao']);
    $a->modulo17_comment = intval($_POST['modulo17_comment']);
    $a->modulo17_id = intval($_POST['modulo17_id']);
    if (isset($_FILES['modulo17_imagem']['name']) && !empty($_FILES['modulo17_imagem']['name'])) {
        $a->removerArquivo();
        $a->enviar();
    }
    $a->atualizar();
    Filter :: redirect("frontend/?success");
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}

