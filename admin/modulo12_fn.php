<?php
/*
 * @author phpstaff.com.br
 */
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}

function atualizar() {
    $a = new Modulo12();
    $a->modulo12_nome = addslashes($_POST['modulo12_nome']);
    $a->modulo12_descricao = addslashes($_POST['modulo12_descricao']);
    $a->modulo12_status = intval($_POST['modulo12_status']);
    $a->modulo12_id = intval($_POST['modulo12_id']);
     if (isset($_FILES['modulo12_imagem']['name']) && !empty($_FILES['modulo12_imagem']['name'])) {
        $a->removerArquivo();
        $a->enviar();
    }
    $a->atualizar();
    Filter :: redirect("frontend/?success");
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}

