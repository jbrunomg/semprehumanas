<?php

require_once '../loader.php';
$foto = new Foto();

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $foto_url = md5(uniqid(time())) . '.jpg';
    if (move_uploaded_file($_FILES['file']['tmp_name'], "../images/video/" . $foto_url)) {
        $foto_video = $_REQUEST['video_id'];
        $foto->foto_url = $foto_url;
        $foto->foto_video = "$foto_video";
        $foto_id = $foto->incluir();
        echo json_encode(array('foto_url' => $foto_url, 'foto_id' => $foto_id));
    }
    exit;
}

