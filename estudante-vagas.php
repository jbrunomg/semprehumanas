<?php
//die('AQUI');
require_once './loader.php';
@session_start();
if ($_SESSION['LOGADOESTUDANTE'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'estudante/estudanteLogin/');
    exit;
}
?>
<?php
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl() . "/thumb.php?w=200&src=images/" . $sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';
?>
<style>

h1 {
    font-family: arial, sans-serif;
    font-size: 15pt;

  }

  hr {
    border-color: orangered;
  }
</style>

<body class="js">

	<!-- Preloader -->
	<!-- <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
		<div class="loader-inner ball-scale-ripple-multiple vh-center">
			<div></div>
			<div></div>
			<div></div>
		</div>
	</div> -->
	<!-- End Preloader -->

	<?php require_once './menu.php'; ?>

	<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if (!empty($sobre->modulo3_imagem)) {
										echo "style='background: url(thumb.php?w=1280&zc=0&src=images/" . stripslashes($sobre->modulo3_imagem) . ");'";
									} ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
					<ul>
						<li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
						<li class="active"><a href="estudantes/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->

	<section  id="contact" class="features section" style="padding-top: 70px;">


		<div class="container">
			
			<?php if (isset($_GET['erro'])): ?>
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h6><i class="icon fa fa-check">  Aconteceu um erro contate o Suporte!</i></h6>                 
			</div>
			<?php endif; ?>
	  
			<div class="row">
				<div class="col-lg-6">
					<h1>Foram encontrada(as) <?php echo $_SESSION['ESTUDANTE']['totalvagas'][0]->qtd_vagas ?>(vagas).</h1>
					<hr>

					<?php foreach ($_SESSION['ESTUDANTE']['listarVagas'] as $key) { ?>

						<div class="panel panel-default">
							<div class="panel-body">
								<h1><?php echo $key->sestvacurso ?></h1>
								<hr>
								<ul class="fa-ul">
									<li><i class="fa-li fa fa-check-circle"style="color: orangered;"></i>Carga Horária: <?php echo $key->sestvacarho ?></li>
									<li><i class="fa-li fa fa-check-circle"style="color: orangered;"></i>Local: <?php echo $key->lugar ?></li>
									<li><i class="fa-li fa fa-check-circle"style="color: orangered;"></i>Benefícios: <?php echo $key->beneficios ?></li>
									<li><i class="fa-li fa fa-check-circle"style="color: orangered;"></i>Atividades: <?php echo $key->sestvaativi ?></li>
									<li><i class="fa-li fa fa-check-circle"style="color: orangered;"></i>Observação: <?php echo $key->observacaovaga ?></li>
									<li><i class="fa-li fa fa-check-circle"style="color: orangered;"></i>Código da Vaga: <?php echo $key->pestvacodig ?></li>
								</ul>

								<br>
								<div class="form-group">
									<a  href="estudante/vagasCadastro/?id=<?php echo $key->pestvacodig ?>"><button type="submit" class="button primary"><i class="fa fa-send"></i> CANDIDATAR-SE</button></a>
								</div>
							</div>
						</div>

					<?php } ?>

				</div>
				<div class="col-lg-6">
					<h1>Informação</h1>
					<hr>

					
				    <?php
				      $servico = new Servico();
				      $servico->getServicos();     
				    ?> 
				    <!-- Início Serviço --> 
				    <?php foreach ($servico->db->data as $s): ?>
				    <div class="col-md-12 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
				      <a href="<?= stripslashes($s->servico_link) ?>">
				        <div class="single-features">
				          <i class="<?= stripslashes($s->servico_icon) ?>"></i>
				            <h4><?= stripslashes($s->servico_nome) ?></h4>
				              <p><?= stripslashes($s->servico_descricao) ?></p>
				        </div>
				        </br>
				<!--    <div class="service-content">             
				              <p>Cadastre aqui o seu currículo e tenha acesso as oportunidades de estágio.</p>
				            </div> -->
				      </a>
				    </div>

				    </br>
				    </br>

				    <?php endforeach; ?> 


				</div>
			</div>
		</div>







	</section>







	<?php require_once './footer.php'; ?>
	<!-- Jquery -->
	<script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
	<script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>
	<!-- Scrool Up -->
	<script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
	<!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
	<script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
	<!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>
	<!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
	<!-- Custom  -->

	<script>
		$('li#services').addClass('current');
	</script>
</body>

</html>