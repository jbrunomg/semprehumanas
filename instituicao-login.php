<?php
@session_start();
if (isset($_GET['deslogar'])) {
    $_SESSION['LOGADOINSTITUICAO'] = FALSE;
    session_destroy();
    session_start();
}
require_once './loader.php'; ?>
<?php
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl() . "/thumb.php?w=200&src=images/" . $sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';
?>

<style>
	h1 {
		font-family: arial, sans-serif;
		font-size: 15pt;

	}

	hr {
		border-color: orangered;
	}

	.v {
		color: orangered;
	}
</style>

<body class="js">

	<!-- Preloader -->
	<!-- <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
		<div class="loader-inner ball-scale-ripple-multiple vh-center">
			<div></div>
			<div></div>
			<div></div>
		</div>
	</div> -->
	<!-- End Preloader -->

	<?php require_once './menu.php'; ?>

	<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if (!empty($sobre->modulo3_imagem)) {
										echo "style='background: url(thumb.php?w=1280&zc=0&src=images/" . stripslashes($sobre->modulo3_imagem) . ");'";
									} ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
					<ul>
						<li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
						<li class="active"><a href="instituicao/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->

	<section id="contact" class="features section" style="padding-top: 70px;">

		<div class="container">
			
		<iframe  width="100%"   style="border:none; height:100vh;" src="https://iframe.semprehumana.com.br/instituicoes/processarlogin"></iframe>
<!-- 
            <?php if (isset($_GET['erro'])): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h6><i class="icon fa fa-check">  Login ou senha estão incorretos!</i></h6>                 
                </div>
            <?php endif; ?>

			<?php if (isset($_GET['success'])): ?>
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<h6><i class="icon fa fa-check">Cadastro efetuado com sucesso, faça o login com o seu CNPJ!</i></h6>                 
				</div>
			<?php endif; ?>

			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-body">
							<h1>JÁ SOU CADASTRADO</h1>
							<hr>
							<form method="post" action="inst/login_verificacao/">
								<div class="form-group">
									<input type="text" class="form-control" id="instituicao_login" name="instituicao_login" placeholder="Cnpj" required>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" id="instituicao_senha" name="instituicao_senha" placeholder="Senha" required>
								</div>
								<div class="form-group">
									<button type="submit" class="button primary"><i class="fa fa-send"></i>ACESSAR</button>
								</div>
								<a href="#" data-toggle="modal" data-target="#myModal">Esqueceu a senha?</a>
							</form>
						</div>
					</div>
				</div>
				
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-body">
							<h1>AINDA NÃO SOU CADASTRADO</h1>
							<hr>
							<p>Mantenha-se atualizado. Receba em seu e-mail informaçãoes sobre o que está acontecendo no Sistema SEMPRE HUMANA.</p>
							<br>
							<div class="form-group">
							    <a href="inst/cadastro/"><button type="submit" class="button primary"><i class="fa fa-send"></i>CADASTRE-SE</button></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> -->

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Recupere sua senha de forma simples e rápida e volte a acessar nosso site!</h4>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-lg-6">
								<p>Prezado sua senha é seu CNPJ, caso esteja com dificuldade de acesso entre em contato com o Suporte!</p>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					</div>
				</div>

			</div>
		</div>

	</section>


	<?php require_once './footer.php'; ?>
	<!-- Jquery -->
	<script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
	<script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>
	<!-- Scrool Up -->
	<script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
	<!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
	<script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
	<!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>
	<!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
	<!-- Custom  -->

	<script>
		$('li#services').addClass('current');
	</script>
    
</body>

</html>