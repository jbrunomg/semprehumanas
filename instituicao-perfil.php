<?php
//die('AQUI');
require_once './loader.php'; 
@session_start();
if ($_SESSION['LOGADOINSTITUICAO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'inst/instituicaoLogin/');
    exit;
}
?>
<?php 
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl()."/thumb.php?w=200&src=images/".$sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';
?>

<style>
  h1 {
    font-family: arial, sans-serif;
    font-size: 15pt;

  }

  hr {
    border-color: orangered;
  }

  select.input-lg {
    height: 46px;
    line-height: 20px;
}
.input-lg {
    height: 0px;
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
}
</style>

<body class="js">

	<!-- Preloader -->
	 <!-- <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div> -->
	<!-- End Preloader -->
	
<?php require_once './menu.php'; ?>
      
<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($sobre->modulo3_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($sobre->modulo3_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
					<ul>
						<li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
						<li class="active"><a href="instituicoes/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->   
    
<section id="estudante" class="features section" style="padding-top: 70px;">

	<form method="post" action="inst/perfilCadastro/">
		<div class="container">
			<H1>Edição de Instituições</H1>
			<hr>
			<div class="container">

				<?php if (isset($_GET['erro'])): ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h6><i class="icon fa fa-check">  Aconteceu um erro contate o Suporte!</i></h6>                 
					</div>
				<?php endif; ?>

				<br>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputEmail1">CNPJ:</label>
							<input type="text" class="form-control" id="instituicao_sensencnpj" name="instituicao_sensencnpj" value="<?php echo $_SESSION['INSTITUICAO']['DADOS'][0]->sensencnpj ?>" placeholder="">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputPassword1">Nome Instituição:</label>
							<input type="text" class="form-control" id="instituicao_sensennome" name="instituicao_sensennome" value="<?php echo $_SESSION['INSTITUICAO']['DADOS'][0]->sensennome ?>" placeholder="">
						</div>
					</div>
				</div>
			</div>

			<br>

			<h1>Dados Logradouro</h1>

			<hr>

			<div class="container">
				<div class="row">
					<div class="col-lg-4">
					
						<div class="form-group">
						<label for="exampleInputEmail1">CEP:</label>
						<input type="text" class="form-control" id="instituicao_sensencep" name="instituicao_sensencep" value="<?php echo $_SESSION['INSTITUICAO']['DADOS'][0]->sensencep ?>" placeholder="">
						</div>
					
					</div>
					<div class="col-lg-4">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Endereço:</label>
						<input type="text" class="form-control" id="instituicao_sensenlogra" name="instituicao_sensenlogra" value="<?php echo $_SESSION['INSTITUICAO']['DADOS'][0]->sensenlogra ?>" placeholder="">
						</div>
					
					</div>
					<div class="col-lg-4">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Nº:</label>
						<input type="text" class="form-control" id="instituicao_sensennumer" name="instituicao_sensennumer" value="<?php echo $_SESSION['INSTITUICAO']['DADOS'][0]->sensennumer ?>" placeholder="">
						</div>
					
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-lg-6">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Complemento:</label>
						<input type="text" class="form-control" id="instituicao_sensencompl" name="instituicao_sensencompl" value="<?php echo $_SESSION['INSTITUICAO']['DADOS'][0]->sensencompl ?>" placeholder="">
						</div>
						<div class="form-group">
						<label for="exampleInputEmail1">UF:</label>
						<input type="text" class="form-control" id="instituicao_sensenuf" name="instituicao_sensenuf" value="<?php echo $_SESSION['INSTITUICAO']['DADOS'][0]->sensenuf ?>" placeholder="">
						</div>
					
					</div>
					<div class="col-lg-6">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Bairro:</label>
						<input type="text" class="form-control" id="instituicao_sensenbairr" name="instituicao_sensenbairr" value="<?php echo $_SESSION['INSTITUICAO']['DADOS'][0]->sensenbairr ?>" placeholder="">
						</div>
						<div class="form-group">
						<label for="exampleInputEmail1">Cidade:</label>
						<input type="text" class="form-control" id="instituicao_sensencidad" name="instituicao_sensencidad" value="<?php echo $_SESSION['INSTITUICAO']['DADOS'][0]->sensencidad ?>" placeholder="">
						</div>
					
					</div>
				</div>
			</div>

			<br>
			<h1>Dados Contato</h1>
			<hr>
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputEmail1">Telefone:</label>
							<input type="text" class="form-control" id="instituicao_sensentel01" name="instituicao_sensentel01" value="<?php echo $_SESSION['INSTITUICAO']['DADOS'][0]->sensentel01 ?>" placeholder="">
						</div>
					</div>
					<div class="col-lg-6">
                        <div class="form-group">
							<label for="exampleInputPassword1">Representante:</label>
							<input type="text" class="form-control" id="instituicao_sensenrepre" name="instituicao_sensenrepre" value="<?php echo $_SESSION['INSTITUICAO']['DADOS'][0]->sensenrepre ?>" placeholder="">
						</div>
					</div> 
					<div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputPassword1">Cargo do Representante:</label>
							<input type="text" class="form-control" id="instituicao_sensencargo" name="instituicao_sensencargo" value="<?php echo $_SESSION['INSTITUICAO']['DADOS'][0]->sensencargo ?>" placeholder="">
						</div>
					</div>
				</div>
			</div>

			<br>

			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<button type="submit" class="btn btn-warning btn-lg">Salvar</button>
					</div>
				</div>
			</div>
    	</div>
	</form>



</section>


   
         



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>	
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
    <!-- Custom  -->
	<script type="text/javascript" src="js\jquery.maskedinput.min.js"></script>

	<script>
	$('li#services').addClass('current');
	</script>
	<script>

	$(function ($) {
		  $("#instituicao_sensentel01").mask("(99) 9999-9999?9");
		  $("#instituicao_sensentel02").mask("(99) 9999-9999?9");
		  $("#instituicao_sensentel03").mask("(99) 9999-9999?9");
		  $("#instituicao_sensencep").mask("99999-999");
		  $("#instituicao_sensencnpj").mask("99.999.999/9999-99");
	});


	$('#instituicao_sensencep').blur(function(){
 
		var cep = $('#instituicao_sensencep').val();
		cep = cep.replace('-','');

		if($.trim(cep) != ""){


		  $.getScript("https://www.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
			// o getScript dá um eval no script, então é só ler!
			//Se o resultado for igual a 1 
			  
			  if(resultadoCEP["resultado"]){
				  
				  if (resultadoCEP['resultado'] == '1' || resultadoCEP['resultado'] == '2'){
					
					  $("#instituicao_sensenlogra").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
					  $("#instituicao_sensenbairr").val(unescape(resultadoCEP["bairro"]));
					  $("#instituicao_sensenestad").val(unescape(resultadoCEP["uf"]));
					  $("#instituicao_sensencidad").val(unescape(resultadoCEP["cidade"]));

				  }else{ 

					  $("#instituicao_sensenlogra").val('');
					  $("#instituicao_sensenbairr").val('');
					  $("#instituicao_sensenestad").val('');
					  $("#instituicao_sensencidad").val('');

				  }
				
			  }
						  
		  });
		  
		}
	
	});

	
		

</script>
</body>
</html>          