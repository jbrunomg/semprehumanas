<?php
require_once './loader.php'; 
@session_start();
if ($_SESSION['LOGADOESTUDANTE'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'estudante/estudanteLogin/');
    exit;
}

?>
<?php
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl() . "/thumb.php?w=200&src=images/" . $sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';
?>

<style>
	h1 {
		font-family: arial, sans-serif;
		font-size: 15pt;

	}

	hr {
		border-color: orangered;
	}

	.v{
		color: orangered;
	}

</style>

<body class="js">

	<!-- Preloader -->
	<!-- <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
		<div class="loader-inner ball-scale-ripple-multiple vh-center">
			<div></div>
			<div></div>
			<div></div>
		</div>
	</div> -->
	<!-- End Preloader -->

	<?php require_once './menu.php'; ?>

	<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if (!empty($sobre->modulo3_imagem)) {
										echo "style='background: url(thumb.php?w=1280&zc=0&src=images/" . stripslashes($sobre->modulo3_imagem) . ");'";
									} ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
					<ul>
						<li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
						<li class="active"><a href="estudantes/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->


	<section id="contact" class="features section" style="padding-top: 70px;">


		<div class="container">

			<h1>Informe o Início, o Fim, O Valor de Bolsa e clique em Calcular Recesso</h1>
			<hr>
			<div class="row">
				<div class="col-lg-6">
					<form method="post" action="estudante/calculoResult/">
						<div class="form-group">
							<input type="date" class="form-control" id="dataInicio" name="dataInicio" placeholder="Início do Estágio" required>
						</div>
						<div class="form-group">
							<input type="date" class="form-control" id="dataFim" name="dataFim" placeholder="Fim do estágio" required>
						</div>
						<div class="form-group">
							<input type="number" min="0.00" max="10000.00" step="0.01" class="form-control" id="valor" name="valor" placeholder="Valor da Bolsa-Auxílio" required>
						</div>
						<div class="form-group">
						    <button type="submit" class="button primary"><i class="fa fa-send"></i>CALCULAR</button>
						</div>
					</form>
					
				</div>
			</div>
		</div></br>
            
		<?php if (isset($_GET['calcular'])): ?>
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-body">
								<h1>Resultado - Recesso Previsto</h1>
								<hr>
								<ul class="fa-ul">
									<li><i class="fa-li fa fa-check-circle" style="color: orangered;"></i>Lembramos que PREFERENCIALMENTE o Recesso deve ser gozado durante as férias escolares.
										Conforme cita a Lei do Estágio 11.788 (25/09/2008), Art.13,Capítulo IV: "É assegurado ao estagiário,
										sempre que o estágio tenha duração igual ou superior a 01 (um) ano, período de recesso de 30 (trinta) dias,
										a ser gozado preferencialmente durante suas férias escolares". Em caso de Rescisão do Contrato de estágio antes do término previsto,
										o Recesso deve ser pago em dinheiro conforme a quantidade de dias estagiados.
									</li></br>
									<li><i class="fa-li fa fa-check-circle" style="color: orangered;"></i>RECESSO EM DIAS</li>
									<li>Total de Dias Estagiados: <?php echo $_SESSION['ESTUDANTE']['calculoRecesso']['dias']?></li>
									<li>Quantidade de Dias para gozo do Recesso: <?php echo $_SESSION['ESTUDANTE']['calculoRecesso']['recesso']?></li></br>
									<li><i class="fa-li fa fa-check-circle" style="color: orangered;"></i>RECESSO EM VALOR</li>
									<li>Valor de Bolsa-Auxílio por dia (R$): <?php echo $_SESSION['ESTUDANTE']['calculoRecesso']['bolsaDias']?></li>
									<li>Valor de Recesso Previsto (R$): <?php echo $_SESSION['ESTUDANTE']['calculoRecesso']['calculoRecesso']?></li></br>	
									<li><i class="fa-li fa fa-check-circle" style="color: orangered;"></i>A fórmula utilizada para chegar valor do Recesso Remunerado previsto é a seguinte: divida o valor de Bolsa por 30 e multiplique pela quantidade de dias estagiados. </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<P>Para dúvidas sobre o Recesso Remunerado,<a class="v" href="contato/">Clique aqui!</a></P>
				</div>
			</div>
		</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<P>Para fazer o download do Termo de Concessão do Recesso Remunerado,<a class="v" onclick="location.href='estudantes/assets/files/TERMO_DE_CONCESSAO_DE_RECESSO_DE_ESTAGIO.doc';" target="_blank"> clique aqui !</a> </P>
				</div>
			</div>
		</div>
		</div>
		</div>


	</section>








	<?php require_once './footer.php'; ?>
	<!-- Jquery -->
	<script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
	<script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>
	<!-- Scrool Up -->
	<script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
	<!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
	<script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
	<!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>
	<!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
	<!-- Custom  -->

	<script>
		$('li#services').addClass('current');
	</script>


</body>

</html>