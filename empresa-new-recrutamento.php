<?php
//die('AQUI');
require_once './loader.php'; 
@session_start();
if ($_SESSION['LOGADOEMPRESA'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'empresa/empresaLogin/');
    exit;
}
//echo'<pre>'; var_dump($_SESSION); exit();
?>


<?php 
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl()."/thumb.php?w=200&src=images/".$sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';
?>

<style>
  h1 {
    font-family: arial, sans-serif;
    font-size: 15pt;

  }

  hr {
    border-color: orangered;
  }

  select.input-lg {
    height: 46px;
    line-height: 20px;
}
.input-lg {
    height: 0px;
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
}
</style>

<body class="js">

	<!-- Preloader -->
	 <!-- <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div> -->
	<!-- End Preloader -->
	
<?php require_once './menu.php'; ?>
      
<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($sobre->modulo3_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($sobre->modulo3_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
					<ul>
						<li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
						<li class="active"><a href="empresas/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->   
    
<section id="estudante" class="features section" style="padding-top: 70px;">

	<form method="post" action="empresa/recrutCadastro/">
		<div class="container">
			<H1>Empresa</H1>
			<hr>
			<div class="container">

				<?php if (isset($_GET['erro'])): ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h6><i class="icon fa fa-check">  Aconteceu um erro contate o Suporte!</i></h6>                 
					</div>
				<?php endif; ?>

				<br>
			</div>

			<div class="container">
				<div class="row">
                    <div class="col-lg-6">
						<div class="form-group">
							<label for="exampleInputPassword1">Empresa</label>
							<input type="text" class="form-control" id="empresa_sempemrazao" name="empresa_sempemrazao" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemrazao ?>" disabled>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
                            <label for="input">Usar endereço da empresa?</label>
                            <select id="usar" class="form-control">
                                <option value="sim">Sim</option>
                                <option value="nao">Não</option>
                            </select>
						</div>
					</div>
				</div>
			</div>
            <div class="container">
				<div class="row">
					<div class="col-lg-4">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Endereço</label>
						<input type="text" class="form-control" id="empresa_sempemlogra" name="empresa_sempemlogra" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemlogra ?>" placeholder="">
						</div>
					
					</div>
					<div class="col-lg-1">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Nº</label>
						<input type="text" class="form-control" id="empresa_sempemnumer" name="empresa_sempemnumer" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemnumer ?>" placeholder="">
						</div>
					
					</div>
                    <div class="col-lg-3">

                        <div class="form-group">
                        <label for="exampleInputEmail1">Bairro</label>
                        <input type="text" class="form-control" id="empresa_sempembairr" name="empresa_sempembairr" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempembairr ?>" placeholder="">
                        </div>
                        
                    </div>
                    <div class="col-lg-3">
                    
                        <div class="form-group">
                        <label for="exampleInputEmail1">Cidade</label>
                        <input type="text" class="form-control" id="empresa_sempemcidad" name="empresa_sempemcidad" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemcidad ?>" placeholder="">
                        </div>
                    
                    </div>
                    <div class="col-lg-1">

                        <div class="form-group">
                        <label for="exampleInputEmail1">UF</label>
                        <input type="text" class="form-control" id="empresa_sempemuf" name="empresa_sempemuf" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemuf ?>" placeholder="">
                        </div>
                    
                    </div>
				</div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                    
                        <div class="form-group">
                        <label for="exampleInputEmail1">CEP</label>
                        <input type="text" class="form-control" id="empresa_sempemcep" name="empresa_sempemcep" value="<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemcep ?>" placeholder="">
                        </div>
                    
                    </div>

                   
                </div>
            </div>

			<br>
            <h1>Dados da vaga</h1>

            <hr>

            <div class="container">
                <div class="row">

                    <div class="col-lg-5">                        
                        <div class="form-group">
                        <label for="input">Tipo recrutamento</label>
                        <select id="recrut_tipo" name="recrut_tipo" class="form-control" required>
                            <option value="">Selecione</option>
                            <option value="estagio">Estágio</option>
                            <option value="emprego">Emprego</option>
                        </select>
                        </div>                    
                    </div>         


                    <div class="col-lg-3">                    
                        <div class="form-group">
                        <label for="input">Quantidade de vaga(s)</label>
                        <input type="text" class="form-control" id="recrut_qtd_vaga" name="recrut_qtd_vaga" value="">
                        </div>                    
                    </div>

                    <div class="col-lg-4">
                        
                        <div class="form-group">
                        <label for="input">Período/Estágio</label>
                        <select id="periodo" name="periodo" class="form-control">
                            <option value="">Selecione</option>
                            <option value="Manha">Manhã</option>
                            <option value="Tarde">Tarde</option>
                            <option value="Noite">Noite</option>
                            <option value="Manha/Tarde">Manhã/Tarde</option>
                            <option value="Manha/Noite">Manhã/Noite</option>
                            <option value="Tarde/Noite">Tarde/Noite</option>
                            <option value="Integral">Integral</option>
                            <option value="EAD">EAD</option>
                        </select>
                        </div>
                    
                    </div>
    
                </div>
            </div>


            <br>
            <h1>Cursos/Nível</h1>
            <hr>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="panel panel-body border-top-teal text-center">
                            <h5 class="no-margin text-semibold"><strong>Curso 01</strong></h5>
                            <select id="curso" name="curso" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($_SESSION['EMPRESA']['CURSOS'] as $valor) { ?>
                                    <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>
                           
                            <h5 class="no-margin text-semibold"><strong>Nível</strong></h5>            
                            <select id="nivel" name="nivel" class="form-control">
                                <option value="">Selecione</option>
                                <option value="Medio">Médio</option>
                                <option value="Tecnico">Técnico</option>                  
                                <option value="Superior">Superior</option>
                                <option value="Pos">Pós Graduação</option>                   
                            </select>

                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Curso 02</strong></h5>
                            <select id="curso2" name="curso2" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($_SESSION['EMPRESA']['CURSOS'] as $valor) { ?>
                                    <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>

                            <h5 class="no-margin text-semibold"><strong>Nível</strong></h5>                                     
                            <select id="nivel2" name="nivel2" class="form-control">
                                <option value="">Selecione</option>
                                <option value="Medio">Médio</option>
                                <option value="Tecnico">Técnico</option>                  
                                <option value="Superior">Superior</option>
                                <option value="Pos">Pós Graduação</option>                   
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Curso 03</strong></h5>
                            <select id="curso3" name="curso3" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($_SESSION['EMPRESA']['CURSOS'] as $valor) { ?>
                                    <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>

                            <h5 class="no-margin text-semibold"><strong>Nível</strong></h5>
                            <select id="nivel3" name="nivel3" class="form-control">
                                <option value="">Selecione</option>
                                <option value="Medio">Médio</option>
                                <option value="Tecnico">Técnico</option>                  
                                <option value="Superior">Superior</option>
                                <option value="Pos">Pós Graduação</option>                   
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Curso 04</strong></h5>
                            <select id="curso4" name="curso4" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($_SESSION['EMPRESA']['CURSOS'] as $valor) { ?>
                                    <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>

                            <h5 class="no-margin text-semibold"><strong>Nível</strong></h5>
                            <select id="nivel4" name="nivel4" class="form-control">
                                <option value="">Selecione</option>
                                <option value="Medio">Médio</option>
                                <option value="Tecnico">Técnico</option>                  
                                <option value="Superior">Superior</option>
                                <option value="Pos">Pós Graduação</option>                   
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">                    
                        <div class="form-group">
                        <label for="input">Inicio (Estágio)</label>
                        <input type="text" class="form-control" id="inicio" name="inicio" placeholder="Ex: 00/00/0000">
                        </div>                    
                    </div> 

                    <div class="col-lg-4">                        
                        <div class="form-group">
                        <label for="input">Requisitos exigidos pela empresa</label>
                        <input type="text" class="form-control" name="requisitosExigidos" id="requisitosExigidos" placeholder="Ex: conhecimento em informatica">
                        </div>                    
                    </div>

                    <div class="col-lg-4">                        
                        <div class="form-group">
                        <label for="input">Área de atuação / Atividades previstas</label>
                        <input type="text" class="form-control" id="atv-previstas" name="atv-previstas" placeholder="Ex: Financeiro / contas a pagar">
                        </div>                    
                    </div>           

                 

                    <div class="col-lg-4">                    
                        <div class="form-group">
                        <label for="input">Horário</label>
                        <input type="text" class="form-control" id="horario" name="horario" placeholder="Ex: 13hs as 18hs">
                        </div>                    
                    </div> 

                    <div class="col-lg-4">                        
                        <div class="form-group">
                        <label for="input">Gênero</label>
                        <select id="sexo" name="sexo" class="form-control">
                            <option value="Masculino">Masculino</option>
                            <option value="Feminino">Feminino</option>
                            <option value="Ambos">Indiferentes</option>
                        </select>
                        </div>                    
                    </div>

                    <div class="col-lg-4">                    
                        <div class="form-group">
                        <label for="input">Duração (Estágio)</label>
                        <input type="text" class="form-control" id="duracao" name="duracao" placeholder="Ex: 06 Meses">
                        </div>                    
                    </div>

                </div>
            </div>

            <br>
			<h1>Benefícios</h1>

			<hr>

			<div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Bolsa</strong></h5>
                          <label class="checkbox-inline">
                            <input type="checkbox" class="styled" name="bolsa" value="1">  
                            Checked
                          </label>
                          <input type="text"  class="form-control input-xs" name="valor_auxilio" id="valor_auxilio">
                        </div>
                    </div>
               
                    <div class="col-lg-3">
                        <div class="panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Vale transporte</strong></h5>
                          <label class="checkbox-inline">
                            <input type="checkbox" class="styled" name="vale" value="1">   
                            Checked
                          </label>
                          <input type="text"  class="form-control input-xs" name="valor_transporte" id="valor_transporte">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Refeição</strong></h5>
                          <label class="checkbox-inline">
                            <input type="checkbox" class="styled" name="refeicao" value="1">                 
                            Checked
                          </label>
                          <input type="text"  class="form-control input-xs" name="valor_refei" id="valor_refei">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Outros</strong></h5>
                          <label class="checkbox-inline">
                            <input type="checkbox" class="styled" name="outros" value="1">                 
                            Checked
                          </label>
                          <input type="text"  class="form-control input-xs" name="valor_outro" id="valor_outro">
                        </div>
                    </div>

                </div>
			</div>

            <div class="container">
				<div class="row">
<!--                     <div class="col-lg-5">
                        
                        <div class="form-group">
                        <label for="input">Atividades previstas</label>
                        <input type="text" class="form-control" id="atv-previstas" name="atv-previstas">
                        </div>
                    
                    </div>
                    <div class="col-lg-4">
                        
                        <div class="form-group">
                        <label for="input">Responsável</label>
                        <input type="text" class="form-control" name="responsavel" id="responsavel">
                        </div>
                    
                    </div>
                    <div class="col-lg-3">
                        
                        <div class="form-group">
                        <label for="input">Horário para entrevista</label>
                        <input type="text" class="form-control" name="h-entrevista" id="h-entrevista">
                        </div>
                    
                    </div> -->
				</div>
			</div>

            <div class="container">
				<div class="row">
       

  <!--                   <div class="col-lg-5">
                        
                        <div class="form-group">
                        <label for="input">Autorizo divulgação de vaga em site e redes sociais</label>
                        <select id="indicado" name="indicado" class="form-control">
                            <option value="0">Não | (Iremos, divulgar vaga no site)</option>
                            <option value="1">Sim | (Sem vaga no site)</option>
                        </select>
                        </div>
                    
                    </div>
 -->

 

                    <div class="col-lg-5">
                        <div class="panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold">&nbsp;</h5>
                          <label class="checkbox-inline">
                            <input type="checkbox" class="styled" name="divulgacao" value="1">                 
                            <strong>Autorizo divulgação de vaga em site e redes sociais.</strong>
                          </label>                          
                        </div>
                    </div>


              
				</div>
			</div><br>
<!--             <div class="container">
				<div class="row">
                    <div class="col-lg-4">
                        
                        <div class="form-group">
                        <label for="input">Quatidade Vaga</label>
                        <input type="text" class="form-control" name="recrut_qtd_vaga" id="recrut_qtd_vaga">
                        </div>
                    
                    </div>
                  <div class="col-lg-4">
                        
                        <div class="form-group">
                        <label for="input">Requisitos exigidos pela empresa</label>
                        <input type="text" class="form-control" name="requisitosExigidos" id="requisitosExigidos">
                        </div>
                    
                    </div> 
                    <div class="col-lg-4">
                        
                        <div class="form-group">
                        <label for="input">Divulgação</label>
                        <input type="text" class="form-control" name="divulgacao" id="divulgacao">
                        </div>
                    
                    </div>
				</div>
			</div>
 -->
			<br>

			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<button type="submit" class="btn btn-warning btn-lg">Salvar</button>
					</div>
				</div>
			</div>
    	</div>
	</form>



</section>


   
         



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>	
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
    <!-- Custom  -->
	<script type="text/javascript" src="js\jquery.maskedinput.min.js"></script>

	<script>
	$('li#services').addClass('current');
	</script>
	<script>

    $('#usar').change(function() {
        preencherDadosEmpresa();
    });

    function preencherDadosEmpresa() {

        if ($('#usar').val() == 'sim') {
            $('#empresa_sempemlogra').val('<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemlogra ?>');
            $('#empresa_sempemnumer').val('<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemnumer ?>');
            $('#empresa_sempembairr').val('<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempembairr ?>');
            $('#empresa_sempemcep').val('<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemcep ?>');
            $('#empresa_sempemcidad').val('<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemcidad ?>');
            $('#empresa_sempemuf').val('<?php echo $_SESSION['EMPRESA']['DADOS'][0]->sempemuf ?>');
        } else {
            $('#empresa_sempemlogra').val('');
            $('#empresa_sempemnumer').val('');
            $('#empresa_sempembairr').val('');
            $('#empresa_sempemcep').val('');
            $('#empresa_sempemcidad').val('');
            $('#empresa_sempemuf').val('');
        }
    }

	$('#empresa_sempemcep').blur(function(){
 
		var cep = $('#empresa_sempemcep').val();
		cep = cep.replace('-','');

		if($.trim(cep) != ""){


		  $.getScript("https://www.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
			// o getScript dá um eval no script, então é só ler!
			//Se o resultado for igual a 1 
			  
			  if(resultadoCEP["resultado"]){
				  
				  if (resultadoCEP['resultado'] == '1' || resultadoCEP['resultado'] == '2'){
					
					  $("#empresa_sempemlogra").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
					  $("#empresa_sempembairr").val(unescape(resultadoCEP["bairro"]));
					  $("#empresa_sempemuf").val(unescape(resultadoCEP["uf"]));
					  $("#empresa_sempemcidad").val(unescape(resultadoCEP["cidade"]));

				  }else{ 

					  $("#empresa_sempemlogra").val('');
					  $("#empresa_sempembairr").val('');
					  $("#empresa_sempemuf").val('');
					  $("#empresa_sempemcidad").val('');

				  }
				
			  }
						  
		  });
		  
		}
	
	});

	
		

</script>
</body>
</html>          