<?php 
$pagina->getPaginasLateral(); 
                
?>  
  <?php if (isset($pagina->db->data[0])) : ?>

<div class="single-sidebar latest">
 <h2>Últimos Posts</h2>
      <?php foreach ($pagina->db->data as $p): ?>
		<!-- Single Post -->
	<div class="single-post">
		<div class="post-img">
			<img src="thumb.php?w=60&amp;h=60&amp;zc=1&amp;src=images/blog/<?=$p->pagina_imagem?>" alt="<?=stripslashes($p->pagina_nome)?>">
		</div>
	<div class="post-info">
		<h4><a href="post/<?= Filter::slug2($p->pagina_nome) ?>/<?= $p->pagina_id ?>/"><?= Validacao::cut(stripslashes($p->pagina_nome), 61, '...') ?> </a></h4>
		<p><?=$p->pagina_data?></p>
	</div>
  </div>
<!--/ End Single Post -->
  <?php endforeach; ?>
</div> 
  <?php endif; ?>  