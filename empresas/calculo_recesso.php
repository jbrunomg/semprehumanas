<?php
require_once '../loader.php';
@session_start();
if (isset($_SESSION['EMPRESA']['ID'])) {

    $data1 = new DateTime( str_replace('/','-',$_POST['dataInicio']) );
    $data2 = new DateTime( str_replace('/','-',$_POST['dataFim']) );
    $valor = $_POST['valor'];

    $intervalo = $data1->diff( $data2 );

    $data['dias']  = $intervalo->days;
    $data['recesso'] = round((($intervalo->days * 30) / 360), 0);

    $data['meses'] = round(($intervalo->days / 30), 2);

    $data['bolsaDias']  = round(($valor / 30),2);
    $data['calculoRecesso'] = round(($data['recesso'] * ($valor / 30)),2);

    $_SESSION['EMPRESA']['calculoRecesso'] = $data;

    echo '<script type="text/javascript">
    window.location = "'. Validacao::getBase() . 'empresa/empresaCalculo/?calcular'.'"
     </script>';

    //@header('location:' . Validacao::getBase() . 'empresa/empresaCalculo/?calcular');

}