<?php
require_once '../loader.php';
@session_start();
require_once '../database/DB.php';
$db = new DB();
if (isset($_SESSION['EMPRESA']['ID'])) {

    $idEmpresa = $_SESSION['EMPRESA']['ID'];
    $idAluno = $_POST['estudante_palualcodig'];

    if(!$idAluno){
      $rua    = str_replace(',','',str_replace(' ', '+', strtolower($_POST['estudante_saluallogra'])));
      $bairro = str_replace(' ', '+', strtolower(($_POST['estudante_salualbairr'])));
      $endereco = $rua.','.$bairro.','.'pernambuco,brasil';
      $cpf = preg_replace('/[^0-9]/', '', $_POST['estudante_cpf']);
      $talualniver = empty($nome) ? '0000-00-00' : date('Y-m-d',strtotime(str_replace('/', '-',$_POST['estudante_talualniver'])));

      $logLat = geolocationEstudante($endereco);	

      if(!$logLat){
          $logLat['latitude']   = ''; 
          $logLat['longitude']  = '';
      }	

      $dadosInserirEstudante = array(

            'salualnome'        => $_POST['estudante_nome'],
            'salualcpf'         => $cpf,
            'salualrg'          => $_POST['estudante_rg'],							    
            'salualrg_org_expd' => $_POST['estudante_rg_org_expd'],
            'talualniver'       => $talualniver,
            'ialualcurso'       => $_POST['estudante_ialualcurso'],
            'salualestci'       => $_POST['estudante_salualestci'], 

            // Filiação
            'salualpai'        => $_POST['estudante_salualpai'],
            'salualmae'        => $_POST['estudante_salualmae'],

            // Logradouro 
            'saluallogra'      => $_POST['estudante_saluallogra'],
            'salualnumer'      => $_POST['estudante_salualnumer'],
            'salualcidad'      => $_POST['estudante_salualcidad'],
            'salualbairr'      => str_replace(' ', '+', strtolower($_POST['estudante_salualbairr'])),
            'salualestad'      => $_POST['estudante_salualestad'],
            'salualcep'        => $_POST['estudante_salualcep'],
            
            // Contato
            'salualtel01'      => $_POST['estudante_salualtel01'],
            'salualemail'      => $_POST['estudante_salualemail'],

            // Acadêmico
            'ialualcurso'       => $_POST['estudante_ialualcurso'],
            'salualmatricula'   => $_POST['estudante_salualmatricula'],					    
            'ialualinsen'       => $_POST['estudante_ialualinsen'],							    
            'salualperio'       => $_POST['estudante_salualperio'],
            'salualtermino'     => $_POST['estudante_salualtermino'],

            // Outros Dados
            'saluallogin'      => preg_replace('/[^0-9]/', '', $_POST['estudante_cpf']),
            'salualsenha'      => md5(preg_replace('/[^0-9]/', '', $_POST['estudante_cpf'])),
            'date_operacao'    => date('Y-m-d H:i:s'), // Data sempre quando Atualizar cadastro	
            'latitude'         => $logLat['latitude'],
            'longitude'        => $logLat['longitude'],
            'estudante_visivel'   => 1
      );

      $db->str = "SELECT * FROM tbalualunos WHERE salualcpf = '$cpf' ";
      $db->query("$db->str")->fetchAll();
      if ($db->link->affected_rows == 1) {
          echo '<script type="text/javascript">
          window.location = "'. Validacao::getBase() . 'empresa/newestagio/?errocpf'.'"
           </script>';
      }

      $set = [];
      $setValue = [];
      foreach($dadosInserirEstudante as $k => $v) {
        $set[] = "$k";
        $setValue[] = "'$v'";
      }
  
      $db->str = "INSERT INTO tbalualunos (".implode(', ', $set).") VALUES (".implode(', ', $setValue).")";
      $db->query("$db->str")->fetchAll();
  
      if ($db->link->affected_rows != 1) {
          echo '<script type="text/javascript">
          window.location = "'. Validacao::getBase() . 'empresa/newestagio/?erro'.'"
          </script>';
      }

      $idAluno = $db->link->insert_id;

    }

    $dadosInserir = array  (

        'iempemcodig'  => $idEmpresa,
        'ialualcodig'  => $idAluno, 
        'iestvacodig'  => 0,
        'contrestempr_solicitacao_site' => 0,
        'contrestempr_atividade' => $_POST['contrestempr_sertor'].' / '.$_POST['contrestempr_atividade'],
        'contrestempr_estagioobg' => $_POST['contrestempr_estagioobg'],
        'contrestempr_cargahoraria' => $_POST['contrestempr_cargahoraria'],
        'contrestempr_horario' => date('Y-m-d',strtotime(str_replace('/', '-',$_POST['contrestempr_horario']))),
        'contrestempr_bolsa_Auxilio' => $_POST['valor_auxilio'],
        'contrestempr_beneficios' => $_POST['valor_transporte'].' - '.$_POST['valor_refei'].' - '.$_POST['valor_outro'],
        'contrestempr_duracao_estagio'   => $_POST['contrestempr_duracao_estagio'],
        'contrestempr_data_ini'   => date('Y-m-d',strtotime(str_replace('/', '-',$_POST['contrestempr_data_ini']))),
        'contrestempr_data_term'   => date('Y-m-d',strtotime(str_replace('/', '-',$_POST['contrestempr_data_term']))),
        'date_cadastro'  => date('Y-m-d'),
    );

    $set = [];
    $setValue = [];
    foreach($dadosInserir as $k => $v) {
      $set[] = "$k";
      $setValue[] = "'$v'";
    }

    $db->str = "INSERT INTO tbcontrestempr (".implode(', ', $set).") VALUES (".implode(', ', $setValue).")";
    $db->query("$db->str")->fetchAll();

    if ($db->link->affected_rows == 1) {
        echo '<script type="text/javascript">
        window.location = "'. Validacao::getBase() . 'empresa/home/?successRecrut'.'"
          </script>';
    } else {
        echo '<script type="text/javascript">
        window.location = "'. Validacao::getBase() . 'empresa/newestagio/?erro'.'"
          </script>';
    }

}