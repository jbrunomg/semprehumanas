<?php
require_once '../loader.php';
@session_start();
require_once '../database/DB.php';
$db = new DB();

    $Cnpj =  str_replace("-","",str_replace("/","",str_replace(".","",$_POST['empresa_sempemcnpj'])));
    $rua    = str_replace(',','',str_replace(' ', '+', strtolower($_POST['empresa_sempemlogra'])));
    $bairro = str_replace(' ', '+', strtolower(($_POST['empresa_sempembairr'])));
    $endereco = $rua.','.$bairro.','.'pernambuco,brasil';

    $logLat = geolocationEstudante($endereco);	

    if(!$logLat){
        $logLat['latitude']   = ''; 
        $logLat['longitude']  = '';
    }	

    $dadosInserir = array  (
        'sempemcnpj' => $Cnpj,
        'sempemrazao' =>  $_POST['empresa_sempemrazao'],
        'sempemfanta' =>  $_POST['empresa_sempemrazao'],
        'sempemlogra' =>  $_POST['empresa_sempemlogra'],
        'sempemnumer' => $_POST['empresa_sempemnumer'],
        'sempemcompl' => $_POST['empresa_sempemcompl'],
        'sempembairr' => $_POST['empresa_sempembairr'],
        'sempemcidad' => $_POST['empresa_sempemcidad'],
        'sempemuf' => $_POST['empresa_sempemuf'],
        'sempemcep' => $_POST['empresa_sempemcep'],
        'sempemrespo' => $_POST['empresa_sempemrespo'],
        'sempemcargo' => $_POST['empresa_sempemcargo'],
        'sempemtel01' => $_POST['empresa_sempemtel01'],
        'sempemtel02' => $_POST['empresa_sempemtel02'],
        'sempemtel03' => $_POST['empresa_sempemtel03'],
        'sempememail' => $_POST['empresa_sempememail'],
        'sempemrepre' => $_POST['empresa_sempemrepre'],
        'sempemrecar' => $_POST['empresa_sempemrecar'],
        'sempemativi' => $_POST['empresa_sempemativi'],
        'sempemie' => '',
        'date_cadastro' => date('Y-m-d H:i:s'), // Data do 1º dia de cadastro (Não Editar)
        'date_operacao' => date('Y-m-d H:i:s'), // Data da Atualização

        'latitude'   => $logLat['latitude'],
        'longitude'  => $logLat['longitude']
      );
    

    $Cnpj = $Cnpj;
    $db->str = "SELECT * FROM tbempempres WHERE sempemcnpj = '$Cnpj' ";
    $db->query("$db->str")->fetchAll();
    if ($db->link->affected_rows == 1) {
        echo '<script type="text/javascript">
        window.location = "'. Validacao::getBase() . 'empresa/cadastro/?errocpf'.'"
         </script>';
        //@header('location:' . Validacao::getBase() . 'empresa/cadastro/?errocpf');
    }

    if ($Cnpj == NULL or $Cnpj == ''){
        echo '<script type="text/javascript">
        window.location = "'. Validacao::getBase() . 'empresa/cadastro/?errocpfvazio'.'"
         </script>';
        //@header('location:' . Validacao::getBase() . 'empresa/cadastro/?errocpfvazio'); 
    }

   // $email = enviarEmail($_POST['empresa_sempemrazao'], $_POST['empresa_sempemtel01']);

    $set = [];
    $setValue = [];
    foreach($dadosInserir as $k => $v) {
      $set[] = "$k";
      $setValue[] = "'$v'";
    }

    $db->str = "INSERT INTO tbempempres (".implode(', ', $set).") VALUES (".implode(', ', $setValue).")";
    $db->query("$db->str")->fetchAll();

    if ($db->link->affected_rows == 1) {
        echo '<script type="text/javascript">
        window.location = "'. Validacao::getBase() . 'empresa/empresaLogin/?success'.'"
         </script>';
        //@header('location:' . Validacao::getBase() . 'empresa/empresaLogin/?success');
    } else {
        echo '<script type="text/javascript">
        window.location = "'. Validacao::getBase() . 'empresa/cadastro/?erro'.'"
         </script>';
        //@header('location:' . Validacao::getBase() . 'empresa/cadastro/?erro');
    }
    