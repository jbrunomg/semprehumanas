<?php
require_once '../loader.php';
@session_start();
require_once '../database/DB.php';
$db = new DB();
if (isset($_SESSION['EMPRESA']['ID'])) {

    $dadosAlterar = array  (
        
        'recrut_end_esta' => $_POST['recrut_end_esta'].' Nº'.$_POST['empresa_sempemnumer'],            
        'recrut_bairro'   => $_POST['recrut_bairro'],  
        'recrut_cidade'   => $_POST['recrut_cidade'],
        'recrut_uf'       => $_POST['recrut_uf'],          
        'recrut_cep'      => $_POST['recrut_cep'],
        
        'recrut_inicio'   => $_POST['recrut_inicio'],
        'recrut_duracao'  => $_POST['recrut_duracao'],
        'recrut_horario'  => $_POST['recrut_horario'],
        'recrut_bolsa_auxilio' => $_POST['recrut_bolsa_auxilio'] == '1' ?  '1' : '0',
        'recrut_valor_auxilio' => $_POST['recrut_valor_auxilio'],
        'recrut_transporte'    => $_POST['recrut_transporte'] == '1' ?  '1' : '0',
        'recrut_refei'     => $_POST['recrut_refei'] == '1' ?  '1' : '0',
        'recrut_outro'     =>$_POST['recrut_outro'] == '1' ?  '1' : '0',
        'recrut_atividade' => $_POST['atv-previstas'],
        'recrut_respo'     => $_POST['responsavel'],
        'recrut_hs_entrev' => $_POST['h-entrevista'],

        'recrut_curso'   => $_POST['curso'],            
        'recrut_nivel'   => $_POST['nivel'],
        'recrut_curso2'  => $_POST['curso2'],            
        'recrut_nivel2'  => $_POST['nivel2'],
        'recrut_curso3'  => $_POST['curso3'],            
        'recrut_nivel3'  => $_POST['nivel3'],
        'recrut_curso4'  => $_POST['curso4'],            
        'recrut_nivel4'  => $_POST['nivel4'],

        'recrut_periodo' => $_POST['periodo'],
        'recrut_sexo'    => $_POST['sexo'],
        'recrut_indicado'  => $_POST['indicado'],            
        'recrut_requisito' => $_POST['requisitosExigidos'],
        'recrut_qtd_vaga' => $_POST['recrut_qtd_vaga'],
        'recrut_divul'     => $_POST['divulgacao']
      );

      $idEdicao = $_POST['idEdicao'];

      $set = [];
      foreach($dadosAlterar as $k => $v) {
        $set[] = "$k='$v'";
      }

      $db->str = "UPDATE tbrecrutamento SET ".implode(', ', $set)." WHERE recrut_id = '$idEdicao' ";
      $db->query("$db->str")->fetchAll();


      if ($db->link->affected_rows > 0) {
        echo '<script type="text/javascript">
        window.location = "'. Validacao::getBase() . 'empresa/home/?success'.'"
         </script>';
      } else {
            echo '<script type="text/javascript">
            window.location = "'. Validacao::getBase() . 'empresa/recrutPerfil/?erro'.'"
            </script>';
      }

}