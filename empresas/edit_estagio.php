<?php
require_once '../loader.php';
@session_start();
require_once '../database/DB.php';
$db = new DB();
if (isset($_SESSION['EMPRESA']['ID'])) {

    $dadosAlterar = array  (

          'iestvacodig'  => 0,
          'contrestempr_solicitacao_site' => 0,
          'contrestempr_atividade' => $_POST['contrestempr_sertor'].' / '.$_POST['contrestempr_atividade'],
          'contrestempr_estagioobg' => $_POST['contrestempr_estagioobg'],
          'contrestempr_cargahoraria' => $_POST['contrestempr_cargahoraria'],
          'contrestempr_horario' => date('Y-m-d',strtotime(str_replace('/', '-',$_POST['contrestempr_horario']))),
          'contrestempr_bolsa_Auxilio' => $_POST['valor_auxilio'],
          'contrestempr_beneficios' => $_POST['valor_transporte'].' - '.$_POST['valor_refei'].' - '.$_POST['valor_outro'],
          'contrestempr_duracao_estagio'   => $_POST['contrestempr_duracao_estagio'],
          'contrestempr_data_ini'   => date('Y-m-d',strtotime(str_replace('/', '-',$_POST['contrestempr_data_ini']))),
          'contrestempr_data_term'   => date('Y-m-d',strtotime(str_replace('/', '-',$_POST['contrestempr_data_term'])))
      );

      $idEdicao = $_POST['idEdicao'];

      $set = [];
      foreach($dadosAlterar as $k => $v) {
        $set[] = "$k='$v'";
      }

      $db->str = "UPDATE tbcontrestempr SET ".implode(', ', $set)." WHERE contrestempr_id = '$idEdicao' ";
      $db->query("$db->str")->fetchAll();


      if ($db->link->affected_rows > 0) {
        echo '<script type="text/javascript">
        window.location = "'. Validacao::getBase() . 'empresa/home/?success'.'"
         </script>';
      } else {
            echo '<script type="text/javascript">
            window.location = "'. Validacao::getBase() . 'empresa/estPerfil/?erro'.'"
            </script>';
      }

}