<?php
require_once '../loader.php';
@session_start();
require_once '../database/DB.php';
$db = new DB();
if (isset($_SESSION['EMPRESA']['ID'])) {

    $id = $_SESSION['EMPRESA']['ID'];

    $dadosInserir = array  (

        'recrut_empr_id'  => $id,
        'recrut_end_esta' => $_POST['empresa_sempemlogra'].' Nº'.$_POST['empresa_sempemnumer'],            
        'recrut_bairro'   => $_POST['empresa_sempembairr'],  
        'recrut_cidade'   => $_POST['empresa_sempemcidad'],
        'recrut_uf'       => $_POST['empresa_sempemuf'],          
        'recrut_cep'      => $_POST['empresa_sempemcep'],

        'recrut_tipo'      => $_POST['recrut_tipo'],
        'recrut_qtd_vaga'  => $_POST['recrut_qtd_vaga'],
        'recrut_periodo'   => $_POST['periodo'],

        'recrut_curso'   => $_POST['curso'],            
        'recrut_nivel'   => $_POST['nivel'],
        'recrut_curso2'  => $_POST['curso2'],            
        'recrut_nivel2'  => $_POST['nivel2'],
        'recrut_curso3'  => $_POST['curso3'],            
        'recrut_nivel3'  => $_POST['nivel3'],
        'recrut_curso4'  => $_POST['curso4'],            
        'recrut_nivel4'  => $_POST['nivel4'],


        'recrut_inicio'    => $_POST['inicio'],
        'recrut_requisito' => $_POST['requisitosExigidos'],
        'recrut_atividade' => $_POST['atv-previstas'],
        'recrut_horario'  => $_POST['horario'],
        'recrut_sexo'     => $_POST['sexo'],
        'recrut_duracao'  => $_POST['duracao'],

      
        'recrut_bolsa_auxilio' => $_POST['bolsa'] == '1' ?  '1' : '0',
        'recrut_valor_auxilio' => $_POST['valor_auxilio'],
        'recrut_transporte'    => $_POST['vale'] == '1' ?  '1' : '0',
        'recrut_valor_transporte' => $_POST['valor_transporte'],
        'recrut_refei'         => $_POST['refeicao'] == '1' ?  '1' : '0',
        'recrut_valor_refei'   => $_POST['valor_refei'],
        'recrut_outro'         => $_POST['outros'] == '1' ?  '1' : '0',
        'recrut_valor_outro'   => $_POST['valor_outro'],


        
        'recrut_respo'     => '',  // $_POST['responsavel'],
        'recrut_hs_entrev' => '',  // $_POST['h-entrevista'],    
        'recrut_indicado'  => '0', // $_POST['indicado'], 
        'recrut_solicitacao_site'  => '0',            
        
        'recrut_divul'     => $_POST['divulgacao'] == '1' ?  '1' : '0',
        'recrut_status'    => 'ABERTO',  
        'recrut_data_cadastro' =>  date("Y-m-d H:i:s", time())

      );

      $set = [];
      $setValue = [];
      foreach($dadosInserir as $k => $v) {
        $set[] = "$k";
        $setValue[] = "'$v'";
      }
  
      $db->str = "INSERT INTO tbrecrutamento (".implode(', ', $set).") VALUES (".implode(', ', $setValue).")";

      //var_dump($db->str);die();
      //echo $db->str;die();

      $db->query("$db->str")->fetchAll();
  
      if ($db->link->affected_rows == 1) {

        $recrutId = $db->link->insert_id;

        if ($_POST['indicado'] == '0') {
            
            $db->vaga = "INSERT INTO `tbestvagas` (`sestvaempresa`,`sestvaativi`,`sestvacarho`,`sestvacidad`,`sestvabairr`, `sestvauf`, `sestvasexo`, `eestvabolsa`, `eestva_valorauxilio`, `eestvavale`, `eestvarefei`, `eestvaoutros`,
                    vaga_curso, vaga_nivel,
                    vaga_curso2, vaga_nivel2,
                    vaga_curso3, vaga_nivel3,
                    vaga_curso4, vaga_nivel4,
                    recrutamento_id, testvaexpir )		
            SELECT `recrut_empr_id`,`recrut_atividade`,`recrut_horario`,tb_cidades.`id` ,`recrut_bairro`, `recrut_uf` ,`recrut_sexo`, `recrut_bolsa_auxilio`, `recrut_valor_auxilio`, `recrut_transporte`, `recrut_refei`, `recrut_outro`, 
                    recrut_curso, recrut_nivel,
                    recrut_curso2, recrut_nivel2,
                    recrut_curso3, recrut_nivel3,
                    recrut_curso4, recrut_nivel4,
                    {$recrutId}, DATE_ADD(CURDATE(), INTERVAL 15 DAY)    
            FROM `tbrecrutamento`
            LEFT JOIN `tb_cidades` ON tbrecrutamento.`recrut_cidade` = tb_cidades.`nome` AND tbrecrutamento.`recrut_uf` = tb_cidades.`uf`
            WHERE `recrut_id` = ".$recrutId ;
            $db->query("$db->vaga")->fetchAll();
            $vagaId = $db->link->insert_id;

            $db->sql = "UPDATE tbrecrutamento SET vaga_id = {$vagaId} WHERE `recrut_id` = {$recrutId} ";
            $db->query("$db->sql")->fetchAll();

        }

          echo '<script type="text/javascript">
          window.location = "'. Validacao::getBase() . 'empresa/home/?successRecrut'.'"
           </script>';
      } else {
          echo '<script type="text/javascript">
          window.location = "'. Validacao::getBase() . 'empresa/newrecrutamento/?erro'.'"
           </script>';
      }
    

}