<?php
//die('AQUI');
require_once './loader.php'; 
@session_start();
if ($_SESSION['LOGADOEMPRESA'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'empresa/empresaLogin/');
    exit;
}

$pos = strpos($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_end_esta, 'Nº');

if ($pos === false) {
   $end = $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_end_esta;
   $num = '';
}else{
    $result = explode("Nº", $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_end_esta);
    $end = $result[0];
    $num = $result[1]; 
}
?>

<?php 
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl()."/thumb.php?w=200&src=images/".$sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';
?>

<style>
  h1 {
    font-family: arial, sans-serif;
    font-size: 15pt;

  }

  hr {
    border-color: orangered;
  }

  select.input-lg {
    height: 46px;
    line-height: 20px;
}
.input-lg {
    height: 0px;
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
}
</style>

<body class="js">

	<!-- Preloader -->
	 <!-- <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div> -->
	<!-- End Preloader -->
	
<?php require_once './menu.php'; ?>
      
<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($sobre->modulo3_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($sobre->modulo3_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
					<ul>
						<li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
						<li class="active"><a href="empresas/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->   
    
<section id="estudante" class="features section" style="padding-top: 70px;">

	<form method="post" action="empresa/recrutEditExe/">
		<div class="container">
			<H1>Editar Vaga</H1>
			<hr>
			<div class="container">

				<?php if (isset($_GET['erro'])): ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h6><i class="icon fa fa-check">  Aconteceu um erro contate o Suporte!</i></h6>                 
					</div>
				<?php endif; ?>

				<br>
			</div>

			<div class="container">
				<div class="row">
                    <div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputPassword1">Empresa</label>
                            <input  type="hidden" name="idEdicao" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_id; ?>" />
                            <input  type="hidden" name="vaga_id" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->vaga_id; ?>" />
							<input type="text" class="form-control" id="empresa_sempemrazao" name="empresa_sempemrazao" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->sempemrazao ?>" disabled>
						</div>
					</div>
				</div>
			</div>
            <div class="container">
				<div class="row">
					<div class="col-lg-4">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Endereço</label>
						<input type="text" class="form-control" id="recrut_end_esta" name="recrut_end_esta" value="<?php echo $end ?>" placeholder="">
						</div>
					
					</div>
					<div class="col-lg-1">
					
						<div class="form-group">
						<label for="exampleInputEmail1">Nº</label>
						<input type="text" class="form-control" id="empresa_sempemnumer" name="empresa_sempemnumer" value="<?php echo $num ?>" placeholder="">
						</div>
					
					</div>
                    <div class="col-lg-3">

                        <div class="form-group">
                        <label for="exampleInputEmail1">Bairro</label>
                        <input type="text" class="form-control" id="recrut_bairro" name="recrut_bairro" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_bairro ?>" placeholder="">
                        </div>
                        
                    </div>
                    <div class="col-lg-3">
                    
                        <div class="form-group">
                        <label for="exampleInputEmail1">Cidade</label>
                        <input type="text" class="form-control" id="recrut_cidade" name="recrut_cidade" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_cidade ?>" placeholder="">
                        </div>
                    
                    </div>
                    <div class="col-lg-1">

                        <div class="form-group">
                        <label for="exampleInputEmail1">UF</label>
                        <input type="text" class="form-control" id="recrut_uf" name="recrut_uf" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_uf ?>" placeholder="">
                        </div>
                    
                    </div>
				</div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                    
                        <div class="form-group">
                        <label for="exampleInputEmail1">CEP</label>
                        <input type="text" class="form-control" id="recrut_cep" name="recrut_cep" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_cep ?>" placeholder="">
                        </div>
                    
                    </div>
                    
                </div>
            </div>

            <br>

            <h1>Dados da vaga</h1>

            <hr>

            <div class="container">
                <div class="row">




                     <div class="col-lg-5">                    
                        <div class="form-group">
                        <label for="input">Tipo recrutamento</label>
                        <select id="recrut_tipo" name="recrut_tipo" class="form-control">
                            <option value="" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_tipo == ''){ echo "SELECTED";}?>>Selecione</option>
                            <option value="estagio" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_tipo == 'estagio'){ echo "SELECTED";}?>>Estágio</option>
                            <option value="emprego" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_tipo == 'emprego'){ echo "SELECTED";}?>>Emprego</option>                         
                        </select>
                        
                        </div>                    
                    </div>

                    <div class="col-lg-3">                    
                        <div class="form-group">
                        <label for="input">Quantidade de vaga(s)</label>
                        <input type="text" class="form-control" id="recrut_qtd_vaga" name="recrut_qtd_vaga" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_qtd_vaga ?>">
                        </div>                    
                    </div>

                    <div class="col-lg-4">                        
                        <div class="form-group">
                        <label for="input">Período/Estágio</label>
                        <select id="periodo" name="periodo" class="form-control">
                            <option value="" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_periodo == ''){ echo "SELECTED";}?>>Selecione</option>
                            <option value="Manha" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_periodo == 'Manha'){ echo "SELECTED";}?>>Manhã</option>
                            <option value="Tarde" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_periodo == 'Tarde'){ echo "SELECTED";}?>>Tarde</option>
                            <option value="Noite" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_periodo == 'Noite'){ echo "SELECTED";}?>>Noite</option>
                            <option value="Manha/Tarde" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_periodo == 'Manha/Tarde'){ echo "SELECTED";}?>>Manhã/Tarde</option>
                            <option value="Manha/Noite" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_periodo == 'Manha/Noite'){ echo "SELECTED";}?>>Manhã/Noite</option>
                            <option value="Tarde/Noite" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_periodo == 'Tarde/Noite'){ echo "SELECTED";}?>>Tarde/Noite</option>
                            <option value="Integral" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_periodo == 'Integral'){ echo "SELECTED";}?>>Integral</option>
                            <option value="Integral" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_periodo == 'EAD'){ echo "SELECTED";}?>>EAD</option>
                        </select>
                        </div>                    
                    </div>
         

                </div>
            </div>      

			<br>
			<h1>Cursos/Nível</h1>
			<hr>
			<div class="container">
				<div class="row">
                    <div class="col-lg-3">
                        <div class="panel panel-body border-top-teal text-center">
                            <h5 class="no-margin text-semibold"><strong>Curso 01</strong></h5>
                            <select id="curso" name="curso" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($_SESSION['EMPRESA']['CURSOS'] as $valor) { ?>
                                    <?php $selected = ($valor->pcurcucodig == $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_curso)?'SELECTED': ''; ?>
                                    <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>
                           
                            <h5 class="no-margin text-semibold"><strong>Nível</strong></h5>            
                            <select id="nivel" name="nivel" class="form-control">
                                <option value="" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel == ''){ echo "SELECTED";}?>>Selecione</option>
                                <option value="Medio" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel == 'Medio'){ echo "SELECTED";}?>>Médio</option>
                                <option value="Tecnico" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel == 'Tecnico'){ echo "SELECTED";}?>>Técnico</option>
                                <option value="Superior" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel == 'Superior'){ echo "SELECTED";}?>>Superior</option>
                                <option value="Pos" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel == 'Pos'){ echo "SELECTED";}?>>Pós Graduação</option>              
                            </select>

                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Curso 02</strong></h5>
                            <select id="curso2" name="curso2" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($_SESSION['EMPRESA']['CURSOS'] as $valor) { ?>
                                    <?php $selected = ($valor->pcurcucodig == $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_curso2)?'SELECTED': ''; ?>
                                    <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>

                            <h5 class="no-margin text-semibold"><strong>Nível</strong></h5>                                     
                            <select id="nivel2" name="nivel2" class="form-control">
                                <option value="" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel2 == ''){ echo "SELECTED";}?>>Selecione</option>
                                <option value="Medio" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel2 == 'Medio'){ echo "SELECTED";}?>>Médio</option>
                                <option value="Tecnico" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel2 == 'Tecnico'){ echo "SELECTED";}?>>Técnico</option>
                                <option value="Superior" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel2 == 'Superior'){ echo "SELECTED";}?>>Superior</option>
                                <option value="Pos" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel2 == 'Pos'){ echo "SELECTED";}?>>Pós Graduação</option>              
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Curso 03</strong></h5>
                            <select id="curso3" name="curso3" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($_SESSION['EMPRESA']['CURSOS'] as $valor) { ?>
                                    <?php $selected = ($valor->pcurcucodig == $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_curso3)?'SELECTED': ''; ?>
                                    <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>

                            <h5 class="no-margin text-semibold"><strong>Nível</strong></h5>
                            <select id="nivel3" name="nivel3" class="form-control">
                                <option value="" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel3 == ''){ echo "SELECTED";}?>>Selecione</option>
                                <option value="Medio" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel3 == 'Medio'){ echo "SELECTED";}?>>Médio</option>
                                <option value="Tecnico" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel3 == 'Tecnico'){ echo "SELECTED";}?>>Técnico</option>
                                <option value="Superior" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel3 == 'Superior'){ echo "SELECTED";}?>>Superior</option>
                                <option value="Pos" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel3 == 'Pos'){ echo "SELECTED";}?>>Pós Graduação</option>              
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Curso 04</strong></h5>
                            <select id="curso4" name="curso4" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($_SESSION['EMPRESA']['CURSOS'] as $valor) { ?>
                                    <?php $selected = ($valor->pcurcucodig == $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_curso4)?'SELECTED': ''; ?>
                                    <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>

                            <h5 class="no-margin text-semibold"><strong>Nível</strong></h5>
                            <select id="nivel4" name="nivel4" class="form-control">
                                <option value="" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel4 == ''){ echo "SELECTED";}?>>Selecione</option>
                                <option value="Medio" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel4 == 'Medio'){ echo "SELECTED";}?>>Médio</option>
                                <option value="Tecnico" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel4 == 'Tecnico'){ echo "SELECTED";}?>>Técnico</option>
                                <option value="Superior" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel4 == 'Superior'){ echo "SELECTED";}?>>Superior</option>
                                <option value="Pos" <?php if($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_nivel4 == 'Pos'){ echo "SELECTED";}?>>Pós Graduação</option>              
                            </select>
                        </div>
                    </div>


                    <div class="col-lg-4">                    
                        <div class="form-group">
                        <label for="input">Inicio (Estágio)</label>
                        <input type="text" class="form-control" id="recrut_inicio" name="recrut_inicio" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_inicio ?>">
                        </div>                    
                    </div>

                    <div class="col-lg-4">
                        
                        <div class="form-group">
                        <label for="input">Requisitos exigidos pela empresa</label>
                        <input type="text" class="form-control" name="requisitosExigidos" id="requisitosExigidos" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_requisito ?>">
                        </div>
                    
                    </div>

                    <div class="col-lg-4">
                        
                        <div class="form-group">
                        <label for="input">Área de atuação / Atividades previstas</label>
                        <input type="text" class="form-control" id="atv-previstas" name="atv-previstas" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_atividade ?>">
                        </div>
                    
                    </div>
        
                    <div class="col-lg-4">                    
                        <div class="form-group">
                        <label for="input">Horário</label>
                        <input type="text" class="form-control" id="recrut_horario" name="recrut_horario" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_horario ?>">
                        </div>                    
                    </div>

                    <div class="col-lg-4">                        
                        <div class="form-group">
                        <label for="input">Gênero</label>
                        <select id="sexo" name="sexo" class="form-control">
                            <option value="">Selecione</option>
                            <option value="Masculino" <?php echo ($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_sexo == 'Masculino')?'selected':''; ?>>Masculino</option>
                            <option value="Feminino" <?php echo ($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_sexo == 'Feminino')?'selected':''; ?>>Feminino</option>
                            <option value="Ambos" <?php echo ($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_sexo == 'Ambos')?'selected':''; ?>>Indiferente</option>
                        </select>
                        </div>                    
                    </div>

                    <div class="col-lg-4">                    
                        <div class="form-group">
                        <label for="input">Duração</label>
                        <input type="text" class="form-control" id="recrut_duracao" name="recrut_duracao" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_duracao ?>">
                        </div>                    
                    </div>


				</div>
			</div>


            <br>

            <h1>Benefícios</h1>

            <hr>

            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>Bolsa</strong></h5>
                          <label class="checkbox-inline">
                          <input class="styled" name="recrut_bolsa_auxilio" type="checkbox" value="1" <?php if( $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_bolsa_auxilio <> 0){ echo 'checked';}else{ echo '';} ?>>
                            Checked
                          </label>
                          <input type="text"  class="form-control input-xs" name="recrut_valor_auxilio" id="recrut_valor_auxilio"  value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_valor_auxilio ?>">
                        </div>
                    </div>
    
        
                  <div class="col-lg-3">
                    <div class="panel-body border-top-teal text-center">
                      <h5 class="no-margin text-semibold"><strong>Vale transporte</strong></h5>
                      <label class="checkbox-inline">
                      <input class="styled" name="recrut_transporte" type="checkbox" value="1" <?php if( $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_transporte <> 0){ echo 'checked';}else{ echo '';} ?>>
                        Checked
                      </label>
                       <input type="text"  class="form-control input-xs" name="valor_transporte" id="valor_transporte"  value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_valor_transporte ?>">
                    </div>
                  </div>

                  <div class="col-lg-3">
                    <div class="panel-body border-top-teal text-center">
                      <h5 class="no-margin text-semibold"><strong>Refeição</strong></h5>
                      <label class="checkbox-inline">
                      <input class="styled" name="recrut_refei" type="checkbox" value="1" <?php if( $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_refei <> 0){ echo 'checked';}else{ echo '';} ?>>
                        Checked
                      </label>
                       <input type="text"  class="form-control input-xs" name="valor_refei" id="valor_refei"  value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_valor_refei ?>">
                    </div>
                  </div>

                  <div class="col-lg-3">
                    <div class="panel-body border-top-teal text-center">
                      <h5 class="no-margin text-semibold"><strong>Outros</strong></h5>
                      <label class="checkbox-inline">
                      <input class="styled" name="recrut_outro" type="checkbox" value="1" <?php if( $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_outro <> 0){ echo 'checked';}else{ echo '';} ?>>
                        Checked
                      </label>
                       <input type="text"  class="form-control input-xs" name="valor_outro" id="valor_outro"  value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_valor_outro ?>">
                    </div>
                  </div>
                    
                </div>
            </div>


<!--             <div class="container">
				<div class="row">
             
                    <div class="col-lg-4">
                        
                        <div class="form-group">
                        <label for="input">Responsável</label>
                        <input type="text" class="form-control" name="responsavel" id="responsavel" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_respo ?>">
                        </div>
                    
                    </div>
                    <div class="col-lg-3">
                        
                        <div class="form-group">
                        <label for="input">Horário para entrevista</label>
                        <input type="text" class="form-control" name="h-entrevista" id="h-entrevista" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_hs_entrev ?>">
                        </div>
                    
                    </div>

                    <div class="col-lg-5">
                        
                        <div class="form-group">
                        <label for="input">Indicado pela empresa ?</label>
                        <select id="indicado" name="indicado" class="form-control">
                            <option value="0" <?php if ($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_indicado == '0') { echo 'SELECTED'; } ?>>Não | (Iremos, divulgar vaga no site)</option>
                            <option value="1" <?php if ($_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_indicado == '1') { echo 'SELECTED'; } ?>>Sim | (Sem vaga no site)</option>
                        </select>
                        </div>
                    
                    </div>

				</div>
			</div>
 -->
            <div class="container">
				<div class="row"> 
                    <div class="col-lg-5">
                        <div class="panel-body border-top-teal text-center">
                          <h5 class="no-margin text-semibold"><strong>&nbsp;</strong></h5>
                          <label class="checkbox-inline">
                          <input class="styled" name="divulgacao" type="checkbox" value="1" <?php if( $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_divul <> 0){ echo 'checked';}else{ echo '';} ?>>
                            Autorizo divulgação de vaga em site e redes sociais.
                          </label>                       
                        </div>
                    </div>       
				</div>
			</div>
            <br>


<!--        <div class="container">
				<div class="row">   
                    <div class="col-lg-4">                        
                        <div class="form-group">
                        <label for="input">Divulgação</label>
                        <input type="text" class="form-control" name="divulgacao" id="divulgacao" value="<?php echo $_SESSION['EMPRESA']['DADOSRECRUT'][0]->recrut_divul ?>">
                        </div>                    
                    </div>
				</div>
			</div>
			<br> -->

			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<button type="submit" class="btn btn-warning btn-lg">Editar</button>
					</div>
				</div>
			</div>
    	</div>
	</form>



</section>


   
         



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>	
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
    <!-- Custom  -->
	<script type="text/javascript" src="js\jquery.maskedinput.min.js"></script>

	<script>
	$('li#services').addClass('current');
	</script>
	<script>

	$('#recrut_cep').blur(function(){
 
		var cep = $('#recrut_cep').val();
		cep = cep.replace('-','');

		if($.trim(cep) != ""){


		  $.getScript("https://www.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
			// o getScript dá um eval no script, então é só ler!
			//Se o resultado for igual a 1 
			  
			  if(resultadoCEP["resultado"]){
				  
				  if (resultadoCEP['resultado'] == '1' || resultadoCEP['resultado'] == '2'){
					
					  $("#recrut_end_esta").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
					  $("#recrut_bairro").val(unescape(resultadoCEP["bairro"]));
					  $("#recrut_uf").val(unescape(resultadoCEP["uf"]));
					  $("#recrut_cidade").val(unescape(resultadoCEP["cidade"]));

				  }else{ 

					  $("#recrut_end_esta").val('');
					  $("#recrut_bairro").val('');
					  $("#recrut_uf").val('');
					  $("#recrut_cidade").val('');

				  }
				
			  }
						  
		  });
		  
		}
	
	});

	
		

</script>
</body>
</html>          