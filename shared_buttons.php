           <?php
           $shared_text = urlencode($titulo_pagina);
           ?>
           <div id="share-buttons">
             <span style="font-weight: 500;">Compartilhe:  </span>
             <!-- Facebook -->            
            <div id="fb-share-button-facebook">
            <a href="http://www.facebook.com/sharer.php?u=<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank">
            <i class="fa fa-facebook"></i>
            <span>Facebook</span>
            </a>
            </div>
            
            
             <!-- Google+ -->
            <div id="fb-share-button-google">
            <a href="https://plus.google.com/share?url=<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank">
            <i class="fa fa-google"></i>
            <span>Google+</span>
            </a>
            </div>
            
             <!-- Twitter -->
            <div id="fb-share-button-twitter">
            <a href="https://twitter.com/share?url=<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&amp;text=<?=stripslashes($shared_text)?>" target="_blank">
            <i class="fa fa-twitter"></i>
            <span>Twitter</span>
            </a>
            </div>
            
            
            <!-- Pinterest -->
            <div id="fb-share-button-pinterest">
            <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
            <i class="fa fa-pinterest"></i>
            <span>Pinterest</span>
            </a>
            </div>
            
            <!-- Print -->
            <div id="fb-share-button-print">
            <a href="javascript:;" onclick="window.print()">
            <i class="fa fa-print"></i>
            <span>Imprimir</span>
            </a>
            </div>
            
            </div>